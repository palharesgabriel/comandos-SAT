#QT += core concurrent
QT += network
QT += sql
QT += core
QT += serialport

CONFIG += c++11

TARGET = TesteAndroidQt
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

android{
    QT += androidextras
}

SOURCES += server_core/main.cpp \
    jsonparser.cpp \
    httpserver/httpconnection.cpp \
    httpserver/httpcookie.cpp \
    httpserver/httprequest.cpp \
    httpserver/httprequesthandler.cpp \
    httpserver/httpresponse.cpp \
    httpserver/httpserver.cpp \
    httpserver/httpsession.cpp \
    httpserver/httpsessionstore.cpp \
    httpserver/httpsettings.cpp \
    httpserver/httptcpserver.cpp \
    httpserver/shtmlcontroller.cpp \
    httpserver/staticfilecontroller.cpp \
    server_core/server.cpp \
    server_core/serviceshandler.cpp \
    server_core/logfile.cpp \
    handlers/FMHANDLER/fmhandler.cpp \
    handlers/MP4200/comandosimpressora.cpp \
    handlers/MP4200/mp4200handler.cpp \
    handlers/MP4200/socketimpressora.cpp \
    dbmanager.cpp \
    handlers/MP4200/bitmap.cpp \
    handlers/Generic/testehandler.cpp \
    handler_templates/generichandler.cpp \
    pdfmanager.cpp \
    handlers/SAT/sathandler.cpp \
    devices/udpbroadcastdeviceinfo.cpp \
    devices/serialdeviceinfo.cpp \
    discovery/udpdiscoveryinfo.cpp \
    discovery/udpdiscoveryagent.cpp \
    discovery/serialdiscoveryagent.cpp \
    handlers/SAT/comandossat.cpp \
    serialmanager.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    jsonparser.h \
    httpserver/hobrasofthttpd.h \
    httpserver/httpconnection.h \
    httpserver/httpcookie.h \
    httpserver/httprequest.h \
    httpserver/httprequesthandler.h \
    httpserver/httpresponse.h \
    httpserver/httpserver.h \
    httpserver/httpsession.h \
    httpserver/httpsessionstore.h \
    httpserver/httpsettings.h \
    httpserver/httptcpserver.h \
    httpserver/shtmlcontroller.h \
    httpserver/staticfilecontroller.h \
    httpserver/testsettings.h \
    handlers/FMHANDLER/fmhandler.h \
    server_core/logfile.h \
    server_core/server.h \
    server_core/serviceshandler.h \
    handlers/MP4200/comandosimpressora.h \
    handlers/MP4200/mp4200handler.h \
    handlers/MP4200/socketimpressora.h \
    handlers/MP4200/defines.h \
    dbmanager.h \
    handlers/MP4200/bitmap.h \
    handlers/Generic/testehandler.h \
    handler_templates/generichandler.h \
    pdfmanager.h \
    handlers/SAT/sathandler.h \
    devices/udpbroadcastdeviceinfo.h \
    devices/serialdeviceinfo.h \
    discovery/udpdiscoveryinfo.h \
    discovery/udpdiscoveryagent.h \
    discovery/serialdiscoveryagent.h \
    handlers/SAT/comandossat.h \
    serialmanager.h \
    handlers/SAT/defines.h

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \
    java/org/qtproject/example/BackgroundService.java \
    java/org/qtproject/example/InitialActivity.java \
    java/org/qtproject/example/QtBridge.java \
    java/org/qtproject/example/BroadcastRegisters.java \
    java/org/qtproject/example/Commands.java \
    java/org/qtproject/example/Actions.java \
    java/org/qtproject/example/Helper.java \
    android/res/xml/filepaths.xml \
    java/org/qtproject/example/bnf/BnfData.java \
    java/org/qtproject/example/bnf/BnfItem.java \
    java/org/qtproject/example/bnf/BnfPag.java \
    httpserver/hobrasofthttpd.pri \
    httpserver/hobrasofthttpd.pri \
    java/LogFile.java \
    java/org/qtproject/example/discovery/DiscoveryThread.java \
    java/org/qtproject/example/util/LogFile.java \
    java/org/qtproject/example/discovery/DiscoveryAgent.java \
    java/org/qtproject/example/discovery/DiscoveryInfo.java \
    java/org/qtproject/example/discovery/DiscoveryField.java \
    java/org/qtproject/example/PdfToBmpParser.java

RESOURCES += \
    config.qrc

android {
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
    ANDROID_JAVA_SOURCES.path = /src/org/qtproject/example
    ANDROID_JAVA_SOURCES.files += $$files($$PWD/java/org/qtproject/example/bnf/*.java)
    ANDROID_JAVA_SOURCES.files += $$files($$PWD/java/org/qtproject/example/discovery/*.java)
    ANDROID_JAVA_SOURCES.files += $$files($$PWD/java/org/qtproject/example/util/*.java)
    ANDROID_JAVA_SOURCES.files += $$files($$PWD/java/org/qtproject/example/*.java)
    INSTALLS += ANDROID_JAVA_SOURCES
}
