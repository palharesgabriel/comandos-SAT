#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QString>
#include <QSqlQuery>
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlError>
#include <QSqlRecord>
#include <QFile>
#include <QVector>

#include "devices/udpbroadcastdeviceinfo.h"


class DbManager
{
public:
    static bool init(void);

    bool removePerson(const QString& name);
    bool personExists(const QString& name);

    // Network Devices
    static UDPBroadcastDeviceInfo::NetworkDevice getNetworkDeviceByMac(const QString&);
    static bool addNetworkDevice(const QString&,const QString&, const QString&);
    static void printAllNetworkDevices(void);
    static QVector<UDPBroadcastDeviceInfo::NetworkDevice> getNetworkDevices(const QString&);
    static bool removeNetworkDevices(const QString& tipo);

    // Serial Devices
    QVector<UDPBroadcastDeviceInfo::NetworkDevice> getSerialDevices(const QString&);
    static bool addSerialDevice(const QString& tag, const QString& porta, const QString& serial);
    static void printAllSerialDevices(void);
    static bool removeSerialDevices(const QString& tipo);
private:
    QSqlDatabase m_db;

    static bool openConnection(QSqlDatabase*);

    DbManager(const QString& path);
    DbManager();

    static bool removeDevice(const QString& tablename, const QString& tipo);
};

#endif // DBMANAGER_H
