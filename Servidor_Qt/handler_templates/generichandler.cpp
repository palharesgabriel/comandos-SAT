#include "generichandler.h"

// Destructor
GenericHandler::~GenericHandler(){}
//---------------------------------------------------------------------------------------------------------------
// Constructor
// Inicia a variável interna jsonMap
GenericHandler::GenericHandler()
{
    qDebug() << "GENERIC";
}
//---------------------------------------------------------------------------------------------------------------
// Envia uma resposta pela rede de volta ao cliente
void GenericHandler::sendResponse(HobrasoftHttpd::HttpResponse *response, QByteArray responseText)
{
    response->write(responseText);
    response->flush();
}
//---------------------------------------------------------------------------------------------------------------
// Envia uma resposta pela rede de volta ao cliente
void GenericHandler::sendResponse(HobrasoftHttpd::HttpResponse *response, int responseStatus)
{
    response->write(QString::number(responseStatus).toUtf8());
    response->flush();
}
//---------------------------------------------------------------------------------------------------------------
// Retorna os valores das chaves de acordo com a lista passada
bool GenericHandler::getValuesFromJSON(const QString content, const QStringList fields)
{
    return JsonParser::parseBody(content, fields, &(this->jsonMap));
}
//---------------------------------------------------------------------------------------------------------------
// Retorna um Vector dos campos de um array json
QVector<QMap<QString,QVariant>> GenericHandler::getVectorFromJSONArray(const QString conteudo, const QString nome, const QStringList fields)
{
    return JsonParser::parseArray(conteudo, nome, fields);
}
//---------------------------------------------------------------------------------------------------------------
// Verifica se o campo existe no json
bool GenericHandler::JSONFieldExists(const QString field, const QString jsonText)
{
    return JsonParser::fieldExists(field, jsonText);
}
//---------------------------------------------------------------------------------------------------------------
// Desvolve um json array de descoberta
QString GenericHandler::jsonDiscoveryArray(QVector<QMap<QString,QVariant>> values)
{
    return JsonParser::jsonArrayResponse("devices", values);
}

