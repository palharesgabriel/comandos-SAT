#ifndef GENERIC_HANDLER_H
#define GENERIC_HANDLER_H

#include <QObject>
#include <QDebug>
#include <QString>

#include "httpserver/httpresponse.h"
#include "httpserver/httpconnection.h"
#include "httpserver/httprequesthandler.h"
#include "jsonparser.h"

#if defined(Q_OS_ANDROID)
#include <QAndroidJniObject>
#include <QtAndroid>
#include <jni.h>
#endif

// Template de classe para representar um dispositivo genérico
class GenericHandler
{
public:
    virtual ~GenericHandler();
    GenericHandler();
protected:
    QMap<QString, QVariant> jsonMap;
    int discoveryDelay = 4;

    // Communication
    void sendResponse(HobrasoftHttpd::HttpResponse *response, int responseStatus);
    void sendResponse(HobrasoftHttpd::HttpResponse *response, QByteArray responseText);

    // JSONParser wrappers
    bool getValuesFromJSON(const QString content, const QStringList fields);
    QVector<QMap<QString,QVariant>> getVectorFromJSONArray(const QString conteudo, const QString nome, const QStringList fields);
    bool JSONFieldExists(const QString field, const QString jsonText);
    QString jsonDiscoveryArray(QVector<QMap<QString,QVariant>>);
};
#endif // GENERIC_HANDLER_H






