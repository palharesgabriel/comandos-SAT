package org.qtproject.example.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.*;
import java.util.Date;
import android.content.Context;
import android.os.Environment;
import android.util.Log;


public class LogFile {
    public static void writeToLog(Context ctx, String s) {
        String format = new SimpleDateFormat("HH:mm:ss:SSS").format(new Date());

        File folder = new File(Environment.getExternalStorageDirectory() + File.separator + "Documents" +
                                             File.separator + "QtRest");

        String logname = new SimpleDateFormat("dd-MM-yyyy").format(new Date());

        File file = new File(folder, "Debug-" +logname + ".log");

        try {
            FileOutputStream fOut = new FileOutputStream(file, true);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(format + " " + s + " [ANDROID]\n");
            myOutWriter.close();
            fOut.flush();
            fOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
