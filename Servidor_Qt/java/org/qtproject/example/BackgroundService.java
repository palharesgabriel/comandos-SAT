package org.qtproject.example;

import android.app.Service;
import android.os.IBinder;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import org.qtproject.qt5.android.bindings.QtService;
import android.util.Log;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import android.widget.Toast;
import android.view.View;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import java.util.Set;
import java.util.HashSet;


public class BackgroundService extends QtService {

    BroadcastReceiver versionReceiver = BroadcastRegisters.versionReceiver;
    BroadcastReceiver baseReceiver = BroadcastRegisters.baseReceiver;
    BroadcastReceiver checkoutReceiver = BroadcastRegisters.checkoutReceiver;
    BroadcastReceiver lookupReceiver = BroadcastRegisters.lookupReceiver;

    // Helper para diminuir uma linha no bloco principal
    private static IntentFilter createFilter(String action){
        IntentFilter filter = new IntentFilter(action);
        filter.addAction(action);

        return filter;
    }

    // Registrar os BroadcastReceivers aqui
    private void registerReceivers(){
        IntentFilter baseFilter = createFilter(Actions.BASE_ACTIVITY);
        registerReceiver(baseReceiver, baseFilter);

        IntentFilter checkoutFilter = createFilter(Actions.CHECKOUT);
        registerReceiver(checkoutReceiver, checkoutFilter);

        IntentFilter lookupFilter = createFilter(Actions.INVOICE_LOOKUP);
        registerReceiver(lookupReceiver, lookupFilter);

        IntentFilter versionFilter = createFilter(Actions.VERSION);
        registerReceiver(versionReceiver, versionFilter);
    }

    // Desregistrar (?) aqui
    private void unregisterReceivers(){
        unregisterReceiver(baseReceiver);
        unregisterReceiver(checkoutReceiver);
        unregisterReceiver(lookupReceiver);
        unregisterReceiver(versionReceiver);
    }

   @Override
   public void onCreate() {
      super.onCreate();
      Log.i("Service", "Service created!");

      registerReceivers();
      Toast.makeText(this, "***SERVIDOR REST***", Toast.LENGTH_SHORT).show();
   }

   @Override
   public int onStartCommand(Intent intent, int flags, int startId) {
      int ret = super.onStartCommand(intent, flags, startId);
      Log.i("Service", "Service created!");

      return ret;
   }

   @Override
   public IBinder onBind(Intent intent) {
      return super.onBind(intent);
      //return mBinder;
   }

   @Override
   public boolean onUnbind(Intent intent) {
      return super.onUnbind(intent);
   }

   @Override
   public void onRebind(Intent intent) {
         super.onRebind(intent);
   }

   @Override
   public void onDestroy() {
      super.onDestroy();
      unregisterReceivers();
   }
}
