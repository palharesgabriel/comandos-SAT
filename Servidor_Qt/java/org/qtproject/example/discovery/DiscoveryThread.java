package org.qtproject.example.discovery;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.Arrays;
import java.util.Vector;

import org.qtproject.example.QtBridge;
import org.qtproject.example.discovery.DiscoveryInfo;

public class DiscoveryThread implements Runnable {
    MulticastSocket socket;
    WifiManager.MulticastLock multicastLock;
    DiscoveryInfo deviceToDiscover = null;
    String deviceList = "";
    int delay = 4;
    Context ctx;

    @Override
    public void run() {
        // Find the server using UDP broadcast

        try {
            // multicast lock
            WifiManager wifi = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
            multicastLock = wifi.createMulticastLock("multicastLock");
            multicastLock.setReferenceCounted(true);
            multicastLock.acquire();

            //Open a random port to send the package
            socket = new MulticastSocket(deviceToDiscover.originPort);

            //socket.joinGroup(InetAddress.getByName("192.168.0.131"));
            socket.setBroadcast(true);
            socket.setSoTimeout(delay*1000);

            byte[] sendData = "MP4200FIND".getBytes();

            //Try the 255.255.255.255 first
            try {
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByName("255.255.255.255"), 1460);
                socket.send(sendPacket);
                System.out.println(getClass().getName() + ">>> Request packet sent to: 255.255.255.255 (DEFAULT)");
            } catch (Exception ex) {
                Log.d("Erro", ex.toString());
            }

            System.out.println(getClass().getName() + ">>> Done looping over all network interfaces. Now waiting for a reply!");

            while(true){
                //Wait for a response
                byte[] recvBuf = new byte[15000];
                DatagramPacket receivePacket = new DatagramPacket(recvBuf, recvBuf.length);
                socket.receive(receivePacket);

                //We have a response
                System.out.println(getClass().getName() + ">>> Broadcast response from server: " + receivePacket.getAddress().getHostAddress());

                //Check if the message is correct
                String message = new String(receivePacket.getData()).trim();
                Log.d("Msg",message);

                String foundMessage = message.substring(0,deviceToDiscover.resultString.length());

                Log.d("FOUND", foundMessage);

                if (foundMessage.equals(deviceToDiscover.resultString)) {
                    //DO SOMETHING WITH THE SERVER'S IP (for example, store it in your controller)
                    //Controller_Base.setServerIp(receivePacket.getAddress());
                    String ipAddr = "";
                    StringBuilder sb = null;

                    Log.d("debug", deviceToDiscover.fields.get(0).name);

                    for(DiscoveryField field : deviceToDiscover.fields) {
                        Log.d("field name", field.name);
                        if(field.name.equals("IP")) {
                            byte[] ip  = Arrays.copyOfRange(receivePacket.getData(), field.begin, field.begin + 4);
                            InetAddress addr = InetAddress.getByAddress(ip);
                            ipAddr = addr.getCanonicalHostName();
                        }else{
                            byte[] mac = Arrays.copyOfRange(receivePacket.getData(), field.begin, field.begin + 6);
                            sb = new StringBuilder(18);

                            for (byte b : mac) {
                                if (sb.length() > 0)
                                    sb.append(':');
                                    sb.append(String.format("%x", b));
                            }
                        }
                    }

                    Log.d("MACFILTER", deviceToDiscover.MACFilter);

                    if(deviceToDiscover.MACFilter.equals("") || deviceToDiscover.MACFilter.equals(sb.toString())) {
                        deviceList += deviceToDiscover.tagName + "-" + ipAddr + "-" + sb.toString() + "|*|";
                        Log.d("DEV", deviceList);
                    }
                }
            }

        } catch (IOException ex) {
            Log.d("Erro",ex.toString());
            socket.close();
            multicastLock.release();
            QtBridge.descobrimentoConcluido(deviceList);
        }
    }

    public static DiscoveryThread getInstance() {
        return DiscoveryThreadHolder.INSTANCE;
    }

    private static class DiscoveryThreadHolder {
        private static final DiscoveryThread INSTANCE = new DiscoveryThread();
    }

    public void setContext(Context ctx) {
        this.ctx = ctx;
    }

    public void setDiscoveryInfo(DiscoveryInfo info) {
        this.deviceToDiscover = info;
    }

    public void setDelay(int delay)
    {
        this.delay = delay;
    }
}
