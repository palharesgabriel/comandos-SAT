package org.qtproject.example.discovery;

import android.content.Context;
import android.util.Log;

import org.qtproject.example.discovery.DiscoveryField;

public class DiscoveryAgent {

    private DiscoveryAgent(){}

    public static void UDPBroadcastDeviceDiscovery(Context ctx, String jsonText, int delay) {
        DiscoveryInfo d = new DiscoveryInfo(jsonText);

        DiscoveryThread t = DiscoveryThread.getInstance();
        t.setContext(ctx);
        t.setDiscoveryInfo(d);
        t.setDelay(delay);

        Thread discoveryThread = new Thread(t);
        discoveryThread.start();
    }
}
