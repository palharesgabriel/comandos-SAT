package org.qtproject.example.discovery;

public class DiscoveryField {
    String name;
    int begin;

    public DiscoveryField(){}

    public DiscoveryField(String name, int begin) {
        this.name = name;
        this.begin = begin;
    }
}

