package org.qtproject.example.discovery;

import android.util.Log;
import java.util.Vector;
import org.json.JSONObject;
import org.json.JSONArray;

public class DiscoveryInfo {
    private static final String TAG_NAME         = "tag";
    private static final String ORIGIN_PORT      = "originPort";
    private static final String DEST_PORT        = "destPort";
    private static final String DISCOVERY_STRING = "discoveryString";
    private static final String RESULT_STRING    = "resultString";
    private static final String MAC_FILTER       = "filtroMAC";

    private static final String FIELDS_OBJ       = "fields";
    private static final String FIELD_ID         = "name";
    private static final String INDEX_BEGIN      = "begin";

    String tagName;
    int originPort;
    int destPort;
    String discoveryString;
    String resultString;
    String MACFilter;
    Vector<DiscoveryField> fields;

    public DiscoveryInfo(){}

    public DiscoveryInfo(String jsonText) {
        try {
            JSONObject obj = new JSONObject(jsonText);

            this.tagName = obj.getString(TAG_NAME);
            this.originPort = obj.getInt(ORIGIN_PORT);
            this.destPort = obj.getInt(DEST_PORT);
            this.discoveryString = obj.getString(DISCOVERY_STRING);
            this.resultString = obj.getString(RESULT_STRING);
            this.MACFilter = obj.getString(MAC_FILTER);
            this.fields = new Vector<DiscoveryField>();

            JSONArray arr = obj.getJSONArray(FIELDS_OBJ);

            for (int i=0; i < arr.length(); i++) {
                try {
                    JSONObject entry = arr.getJSONObject(i);
                    String name = entry.getString(FIELD_ID);
                    int begin = entry.getInt(INDEX_BEGIN);

                    DiscoveryField f = new DiscoveryField(name,begin);
                    this.fields.add(f);
                } catch (Exception e) {
                    Log.d("Erro", e.getMessage());
                }
            }

        }catch(Exception e){
            Log.d("Erro", e.getMessage());
        }
    }
}
