package org.qtproject.example.bnf;

import org.qtproject.example.bnf.BnfPag;
import java.util.List;

public class BnfData {

    /**
     * The versao.
     */
    private String versao;

    /**
     * The v ecf serial.
     */
    private String vEcfSerial;

    /**
     * The c id_ a03.
     */
    private String cId_A03;

    /**
     * The versao_ a02.
     */
    private String versao_A02;

    //Identificação da NFe
    /**
     * The ver proc_ b27.
     */
    private String verProc_B27;

    /**
     * The c n f_ b03.
     */
    private int cNF_B03;

    /**
     * The nat op_ b04.
     */
    private String natOp_B04;

    /**
     * The ind pag_ b05.
     */
    private String indPag_B05;

    /**
     * The mod_ b06.
     */
    private int mod_B06;

    /**
     * The serie_ b07.
     */
    private String serie_B07;

    /**
     * The n n f_ b08.
     */
    private int nNF_B08;

    /**
     * The c u f_ b02.
     */
    private int cUF_B02;

    /**
     * The dh emi_ b09.
     */
    private String dhEmi_B09;

    /**
     * The tp n f_ b11.
     */
    private int tpNF_B11;

    /**
     * The id dest_ b11a.
     */
    private int idDest_B11a;

    /**
     * The c mun f g_ b12.
     */
    private String cMunFG_B12;

    /**
     * The tp imp_ b21.
     */
    private int tpImp_B21;

    /**
     * The tp emis_ b22.
     */
    private int tpEmis_B22;

    /**
     * The c d v_ b23.
     */
    private int cDV_B23;

    /**
     * The tp amb_ b24.
     */
    private int tpAmb_B24;

    /**
     * The fin n fe_ b25.
     */
    private int finNFe_B25;

    /**
     * The ind final_ b25a.
     */
    private int indFinal_B25a;

    /**
     * The ind pres_ b25b.
     */
    private int indPres_B25b;

    /**
     * The proc emi_ b26.
     */
    private int procEmi_B26;

    //Dados do Contribuinte
    /**
     * The c cr t_ c21.
     */
    private int cCRT_C21;

    /**
     * The c cnp j_ c02.
     */
    private String cCNPJ_C02;

    /**
     * The x nome_ c03.
     */
    private String xNome_C03;

    /**
     * The x fant_ c04.
     */
    private String xFant_C04;

    /**
     * The x lgr_ c06.
     */
    private String xLgr_C06;

    /**
     * The nro_ c07.
     */
    private String nro_C07;

    /**
     * The xCpl_C08;
     */
    private String xCpl_C08;

    /**
     * The x bairro_ c09.
     */
    private String xBairro_C09;

    /**
     * The c mun_ c10.
     */
    private String cMun_C10;

    /**
     * The x mun_ c11.
     */
    private String xMun_C11;

    /**
     * The x u f_ c12.
     */
    private String xUF_C12;

    /**
     * The c ce p_ c13.
     */
    private String cCEP_C13;

    /**
     * The c pais_ c14.
     */
    private String cPais_C14;

    /**
     * The x pais_ c15.
     */
    private String xPais_C15;

    /**
     * The fone_ c16.
     */
    private String fone_C16;

    /**
     * The c i e_ c17.
     */
    private String cIE_C17;

    /**
     * The c ies t_ c18.
     */
    private String cIEST_C18;

    //Dados Consumidor
    /**
     * The c cnp j_ e02.
     */
    private String cCNPJ_E02;

    /**
     * The c cp f_ e03.
     */
    private String cCPF_E03;

    /**
     * The id estrangeiro_ e03a.
     */
    private String idEstrangeiro_E03a;

    /**
     * The x nome_ e04.
     */
    private String xNome_E04;

    /**
     * The x lgr_ e06.
     */
    private String xLgr_E06;

    /**
     * The nro_ e07.
     */
    private String nro_E07;

    /**
     * The x cpl_ e08.
     */
    private String xCpl_E08;

    /**
     * The x bairro_ e09.
     */
    private String xBairro_E09;

    /**
     * The c mun_ e10.
     */
    private String cMun_E10;

    /**
     * The x mun_ e11.
     */
    private String xMun_E11;

    /**
     * The U f_ e12.
     */
    private String UF_E12;

    /**
     * The CE p_ e13.
     */
    private String CEP_E13;

    /**
     * The c pais_ e14.
     */
    private String cPais_E14;

    /**
     * The x pais_ e15.
     */
    private String xPais_E15;

    /**
     * The fone_ e16.
     */
    private String fone_E16;

    /**
     * The ind ie dest_ e16a.
     */
    private String indIEDest_E16a;

    /**
     * The I e_ e17.
     */
    private String IE_E17;

    /**
     * The ISU f_ e18.
     */
    private String ISUF_E18;

    /**
     * The email_ e19.
     */
    private String email_E19;

    /**
     * The emailDanfe.
     */
    private String emailDanfe;

    //Totais da NFCe
    /**
     * The v troco.
     */
    private double vTroco;

    /**
     * The v imposto dilma.
     */
    private double vTotTrib_W16a;

    /**
     * The v b c_ w03.
     */
    private double vBC_W03;

    /**
     * The v icm s_ w04.
     */
    private double vICMS_W04;

    /**
     * The v bcs t_ w05.
     */
    private double vBCST_W05;

    /**
     * The v s t_ w06.
     */
    private double vST_W06;

    /**
     * The v prod_ w07.
     */
    private double vProd_W07;

    /**
     * The v frete_ w08.
     */
    private double vFrete_W08;

    /**
     * The v seg_ w09.
     */
    private double vSeg_W09;

    /**
     * The v desc_ w10.
     */
    private double vDesc_W10;

    /**
     * The v i i_ w11.
     */
    private double vII_W11;

    /**
     * The v ip i_ w12.
     */
    private double vIPI_W12;

    /**
     * The v pi s_ w13.
     */
    private double vPIS_W13;

    /**
     * The v cofin s_ w14.
     */
    private double vCOFINS_W14;

    /**
     * The v outro_ w15.
     */
    private double vOutro_W15;

    /**
     * The v n f_ w16.
     */
    private double vNF_W16;

    /**
     * The mod frete_ x02.
     */
    private int modFrete_X02;

    /**
     * The tpSaidaDanfe.
     */
    private Integer tpSaidaDanfe;

    /**
     * The tpLayoutDanfe.
     */
    private Integer tpLayoutDanfe;

    /**
     * The vICMSDeson W04a
     */
    private double vICMSDeson_W04a;

    //Produtos da NFCe
    /**
     * The items.
     */
    private List<BnfItem> items;

    //Pagamentos da NFCe
    /**
     * The pagamentos.
     */
    private List<BnfPag> pagamentos;

    //Informações Complementares
    /**
     * The inf cpl_ z03.
     */
    private String infCpl_Z03;

    /**
     * Instantiates a new bnf data.
     */
    public BnfData() {
    }

    /**
     * Gets the x cpl_ e08.
     *
     * @return the x cpl_ e08
     */
    public String getxCpl_E08() {
        return xCpl_E08;
    }

    /**
     * Sets the x cpl_ e08.
     *
     * @param xCpl_E08 the new x cpl_ e08
     */
    public void setxCpl_E08(String xCpl_E08) {
        this.xCpl_E08 = xCpl_E08;
    }

    /**
     * Gets the cE p_ e13.
     *
     * @return the cE p_ e13
     */
    public String getCEP_E13() {
        return CEP_E13;
    }

    /**
     * Sets the cE p_ e13.
     *
     * @param cEP_E13 the new cE p_ e13
     */
    public void setCEP_E13(String cEP_E13) {
        CEP_E13 = cEP_E13;
    }

    /**
     * Gets the c pais_ e14.
     *
     * @return the c pais_ e14
     */
    public String getcPais_E14() {
        return cPais_E14;
    }

    /**
     * Sets the c pais_ e14.
     *
     * @param cPais_E14 the new c pais_ e14
     */
    public void setcPais_E14(String cPais_E14) {
        this.cPais_E14 = cPais_E14;
    }

    /**
     * Gets the x pais_ e15.
     *
     * @return the x pais_ e15
     */
    public String getxPais_E15() {
        return xPais_E15;
    }

    /**
     * Sets the x pais_ e15.
     *
     * @param xPais_E15 the new x pais_ e15
     */
    public void setxPais_E15(String xPais_E15) {
        this.xPais_E15 = xPais_E15;
    }

    /**
     * Gets the fone_ e16.
     *
     * @return the fone_ e16
     */
    public String getFone_E16() {
        return fone_E16;
    }

    /**
     * Sets the fone_ e16.
     *
     * @param fone_E16 the new fone_ e16
     */
    public void setFone_E16(String fone_E16) {
        this.fone_E16 = fone_E16;
    }

    /**
     * Gets the i e_ e17.
     *
     * @return the i e_ e17
     */
    public String getIE_E17() {
        return IE_E17;
    }

    /**
     * Sets the i e_ e17.
     *
     * @param iE_E17 the new i e_ e17
     */
    public void setIE_E17(String iE_E17) {
        IE_E17 = iE_E17;
    }

    /**
     * Gets the iSU f_ e18.
     *
     * @return the iSU f_ e18
     */
    public String getISUF_E18() {
        return ISUF_E18;
    }

    /**
     * Sets the iSU f_ e18.
     *
     * @param iSUF_E18 the new iSU f_ e18
     */
    public void setISUF_E18(String iSUF_E18) {
        ISUF_E18 = iSUF_E18;
    }

    /**
     * Gets the email_ e19.
     *
     * @return the email_ e19
     */
    public String getEmail_E19() {
        return email_E19;
    }

    /**
     * Sets the email_ e19.
     *
     * @param email_E19 the new email_ e19
     */
    public void setEmail_E19(String email_E19) {
        this.email_E19 = email_E19;
    }

    /**
     * Gets the emailDanfe.
     *
     * @return the emailDanfe
     */
    public String getEmailDanfe() {
        return emailDanfe;
    }

    /**
     * Sets the emailDanfe.
     *
     * @param emailDanfe the new emailDanfe
     */
    public void setEmailDanfe(String emailDanfe) {
        this.emailDanfe = emailDanfe;
    }

    /**
     * Adds the item.
     *
     * @param it the it
     */
    public void addItem(BnfItem it) {
        items.add(it);
    }

    /**
     * Gets the u f_ e12.
     *
     * @return the u f_ e12
     */
    public String getUF_E12() {
        return UF_E12;
    }

    /**
     * Sets the u f_ e12.
     *
     * @param uF_E12 the new u f_ e12
     */
    public void setUF_E12(String uF_E12) {
        UF_E12 = uF_E12;
    }

    /**
     * Gets the id estrangeiro_ e03a.
     *
     * @return the id estrangeiro_ e03a
     */
    public String getIdEstrangeiro_E03a() {
        return idEstrangeiro_E03a;
    }

    /**
     * Sets the id estrangeiro_ e03a.
     *
     * @param idEstrangeiro_E03a the new id estrangeiro_ e03a
     */
    public void setIdEstrangeiro_E03a(String idEstrangeiro_E03a) {
        this.idEstrangeiro_E03a = idEstrangeiro_E03a;
    }

    /**
     * Gets the x lgr_ e06.
     *
     * @return the x lgr_ e06
     */
    public String getxLgr_E06() {
        return xLgr_E06;
    }

    /**
     * Sets the x lgr_ e06.
     *
     * @param xLgr_E06 the new x lgr_ e06
     */
    public void setxLgr_E06(String xLgr_E06) {
        this.xLgr_E06 = xLgr_E06;
    }

    /**
     * Gets the nro_ e07.
     *
     * @return the nro_ e07
     */
    public String getNro_E07() {
        return nro_E07;
    }

    /**
     * Sets the nro_ e07.
     *
     * @param nro_E07 the new nro_ e07
     */
    public void setNro_E07(String nro_E07) {
        this.nro_E07 = nro_E07;
    }

    /**
     * Gets the x bairro_ e09.
     *
     * @return the x bairro_ e09
     */
    public String getxBairro_E09() {
        return xBairro_E09;
    }

    /**
     * Sets the x bairro_ e09.
     *
     * @param xBairro_E09 the new x bairro_ e09
     */
    public void setxBairro_E09(String xBairro_E09) {
        this.xBairro_E09 = xBairro_E09;
    }

    /**
     * Gets the c mun_ e10.
     *
     * @return the c mun_ e10
     */
    public String getcMun_E10() {
        return cMun_E10;
    }

    /**
     * Sets the c mun_ e10.
     *
     * @param cMun_E10 the new c mun_ e10
     */
    public void setcMun_E10(String cMun_E10) {
        this.cMun_E10 = cMun_E10;
    }

    /**
     * Gets the x mun_ e11.
     *
     * @return the x mun_ e11
     */
    public String getxMun_E11() {
        return xMun_E11;
    }

    /**
     * Sets the x mun_ e11.
     *
     * @param xMun_E11 the new x mun_ e11
     */
    public void setxMun_E11(String xMun_E11) {
        this.xMun_E11 = xMun_E11;
    }

    /**
     * Gets the ind ie dest_ e16a.
     *
     * @return the ind ie dest_ e16a
     */
    public String getIndIEDest_E16a() {
        return indIEDest_E16a;
    }

    /**
     * Sets the ind ie dest_ e16a.
     *
     * @param indIEDest_E16a the new ind ie dest_ e16a
     */
    public void setIndIEDest_E16a(String indIEDest_E16a) {
        this.indIEDest_E16a = indIEDest_E16a;
    }

    /**
     * Gets the inf cpl_ z03.
     *
     * @return the inf cpl_ z03
     */
    public String getInfCpl_Z03() {
        return infCpl_Z03;
    }

    /**
     * Sets the inf cpl_ z03.
     *
     * @param infCpl_Z03 the new inf cpl_ z03
     */
    public void setInfCpl_Z03(String infCpl_Z03) {
        this.infCpl_Z03 = infCpl_Z03;
    }

    /**
     * Gets the v troco.
     *
     * @return the v troco
     */
    public double getvTroco() {
        return vTroco;
    }

    /**
     * Sets the v troco.
     *
     * @param vTroco the new v troco
     */
    public void setvTroco(double vTroco) {
        this.vTroco = vTroco;
    }

    /**
     * Gets the v imposto dilma.
     *
     * @return the v imposto dilma
     */
    public double getvTotTrib_W16a() {
        return vTotTrib_W16a;
    }

    /**
     * Sets the v imposto dilma.
     *
     * @param vTotTrib_W16a the new v imposto dilma
     */
    public void setvTotTrib_W16a(double vTotTrib_W16a) {
        this.vTotTrib_W16a = vTotTrib_W16a;
    }

    /**
     * Gets the c cnp j_ e02.
     *
     * @return the c cnp j_ e02
     */
    public String getcCNPJ_E02() {
        return cCNPJ_E02;
    }

    /**
     * Sets the c cnp j_ e02.
     *
     * @param cCNPJ_E02 the new c cnp j_ e02
     */
    public void setcCNPJ_E02(String cCNPJ_E02) {
        this.cCNPJ_E02 = cCNPJ_E02;
    }

    /**
     * Gets the ind pag_ b05.
     *
     * @return the ind pag_ b05
     */
    public String getIndPag_B05() {
        return indPag_B05;
    }

    /**
     * Sets the ind pag_ b05.
     *
     * @param indPag_B05 the new ind pag_ b05
     */
    public void setIndPag_B05(String indPag_B05) {
        this.indPag_B05 = indPag_B05;
    }

    /**
     * Adds the pagto.
     *
     * @param pg the pg
     */
    public void addPagto(BnfPag pg) {
        pagamentos.add(pg);
    }

    /**
     * Gets the c id_ a03.
     *
     * @return the c id_ a03
     */
    public String getcId_A03() {
        return cId_A03;
    }

    /**
     * Sets the c id_ a03.
     *
     * @param cId_A03 the new c id_ a03
     */
    public void setcId_A03(String cId_A03) {
        this.cId_A03 = cId_A03;
    }

    /**
     * Gets the versao_ a02.
     *
     * @return the versao_ a02
     */
    public String getVersao_A02() {
        return versao_A02;
    }

    /**
     * Sets the versao_ a02.
     *
     * @param versao_A02 the new versao_ a02
     */
    public void setVersao_A02(String versao_A02) {
        this.versao_A02 = versao_A02;
    }

    /**
     * Gets the ver proc_ b27.
     *
     * @return the ver proc_ b27
     */
    public String getVerProc_B27() {
        return verProc_B27;
    }

    /**
     * Sets the ver proc_ b27.
     *
     * @param verProc_B27 the new ver proc_ b27
     */
    public void setVerProc_B27(String verProc_B27) {
        this.verProc_B27 = verProc_B27;
    }

    /**
     * Gets the c n f_ b03.
     *
     * @return the c n f_ b03
     */
    public int getcNF_B03() {
        return cNF_B03;
    }

    /**
     * Sets the c n f_ b03.
     *
     * @param cNF_B03 the new c n f_ b03
     */
    public void setcNF_B03(int cNF_B03) {
        this.cNF_B03 = cNF_B03;
    }

    /**
     * Gets the nat op_ b04.
     *
     * @return the nat op_ b04
     */
    public String getNatOp_B04() {
        return natOp_B04;
    }

    /**
     * Sets the nat op_ b04.
     *
     * @param natOp_B04 the new nat op_ b04
     */
    public void setNatOp_B04(String natOp_B04) {
        this.natOp_B04 = natOp_B04;
    }

    /**
     * Gets the mod_ b06.
     *
     * @return the mod_ b06
     */
    public int getMod_B06() {
        return mod_B06;
    }

    /**
     * Sets the mod_ b06.
     *
     * @param mod_B06 the new mod_ b06
     */
    public void setMod_B06(int mod_B06) {
        this.mod_B06 = mod_B06;
    }

    /**
     * Gets the serie_ b07.
     *
     * @return the serie_ b07
     */
    public String getSerie_B07() {
        return serie_B07;
    }

    /**
     * Sets the serie_ b07.
     *
     * @param serie_B07 the new serie_ b07
     */
    public void setSerie_B07(String serie_B07) {
        this.serie_B07 = serie_B07;
    }

    /**
     * Gets the n n f_ b08.
     *
     * @return the n n f_ b08
     */
    public int getnNF_B08() {
        return nNF_B08;
    }

    /**
     * Sets the n n f_ b08.
     *
     * @param nNF_B08 the new n n f_ b08
     */
    public void setnNF_B08(int nNF_B08) {
        this.nNF_B08 = nNF_B08;
    }

    /**
     * Gets the c u f_ b02.
     *
     * @return the c u f_ b02
     */
    public int getcUF_B02() {
        return cUF_B02;
    }

    /**
     * Sets the c u f_ b02.
     *
     * @param cUF_B02 the new c u f_ b02
     */
    public void setcUF_B02(int cUF_B02) {
        this.cUF_B02 = cUF_B02;
    }

    /**
     * Gets the dh emi_ b09.
     *
     * @return the dh emi_ b09
     */
    public String getDhEmi_B09() {
        return dhEmi_B09;
    }

    /**
     * Sets the dh emi_ b09.
     *
     * @param dhEmi_B09 the new dh emi_ b09
     */
    public void setDhEmi_B09(String dhEmi_B09) {
        this.dhEmi_B09 = dhEmi_B09;
    }

    /**
     * Gets the tp n f_ b11.
     *
     * @return the tp n f_ b11
     */
    public int getTpNF_B11() {
        return tpNF_B11;
    }

    /**
     * Sets the tp n f_ b11.
     *
     * @param tpNF_B11 the new tp n f_ b11
     */
    public void setTpNF_B11(int tpNF_B11) {
        this.tpNF_B11 = tpNF_B11;
    }

    /**
     * Gets the id dest_ b11a.
     *
     * @return the id dest_ b11a
     */
    public int getIdDest_B11a() {
        return idDest_B11a;
    }

    /**
     * Sets the id dest_ b11a.
     *
     * @param idDest_B11a the new id dest_ b11a
     */
    public void setIdDest_B11a(int idDest_B11a) {
        this.idDest_B11a = idDest_B11a;
    }

    /**
     * Gets the c mun f g_ b12.
     *
     * @return the c mun f g_ b12
     */
    public String getcMunFG_B12() {
        return cMunFG_B12;
    }

    /**
     * Sets the c mun f g_ b12.
     *
     * @param cMunFG_B12 the new c mun f g_ b12
     */
    public void setcMunFG_B12(String cMunFG_B12) {
        this.cMunFG_B12 = cMunFG_B12;
    }

    /**
     * Gets the tp imp_ b21.
     *
     * @return the tp imp_ b21
     */
    public int getTpImp_B21() {
        return tpImp_B21;
    }

    /**
     * Sets the tp imp_ b21.
     *
     * @param tpImp_B21 the new tp imp_ b21
     */
    public void setTpImp_B21(int tpImp_B21) {
        this.tpImp_B21 = tpImp_B21;
    }

    /**
     * Gets the tp emis_ b22.
     *
     * @return the tp emis_ b22
     */
    public int getTpEmis_B22() {
        return tpEmis_B22;
    }

    /**
     * Sets the tp emis_ b22.
     *
     * @param tpEmis_B22 the new tp emis_ b22
     */
    public void setTpEmis_B22(int tpEmis_B22) {
        this.tpEmis_B22 = tpEmis_B22;
    }

    /**
     * Gets the c d v_ b23.
     *
     * @return the c d v_ b23
     */
    public int getcDV_B23() {
        return cDV_B23;
    }

    /**
     * Sets the c d v_ b23.
     *
     * @param cDV_B23 the new c d v_ b23
     */
    public void setcDV_B23(int cDV_B23) {
        this.cDV_B23 = cDV_B23;
    }

    /**
     * Gets the tp amb_ b24.
     *
     * @return the tp amb_ b24
     */
    public int getTpAmb_B24() {
        return tpAmb_B24;
    }

    /**
     * Sets the tp amb_ b24.
     *
     * @param tpAmb_B24 the new tp amb_ b24
     */
    public void setTpAmb_B24(int tpAmb_B24) {
        this.tpAmb_B24 = tpAmb_B24;
    }

    /**
     * Gets the fin n fe_ b25.
     *
     * @return the fin n fe_ b25
     */
    public int getFinNFe_B25() {
        return finNFe_B25;
    }

    /**
     * Sets the fin n fe_ b25.
     *
     * @param finNFe_B25 the new fin n fe_ b25
     */
    public void setFinNFe_B25(int finNFe_B25) {
        this.finNFe_B25 = finNFe_B25;
    }

    /**
     * Gets the ind final_ b25a.
     *
     * @return the ind final_ b25a
     */
    public int getIndFinal_B25a() {
        return indFinal_B25a;
    }

    /**
     * Sets the ind final_ b25a.
     *
     * @param indFinal_B25a the new ind final_ b25a
     */
    public void setIndFinal_B25a(int indFinal_B25a) {
        this.indFinal_B25a = indFinal_B25a;
    }

    /**
     * Gets the ind pres_ b25b.
     *
     * @return the ind pres_ b25b
     */
    public int getIndPres_B25b() {
        return indPres_B25b;
    }

    /**
     * Sets the ind pres_ b25b.
     *
     * @param indPres_B25b the new ind pres_ b25b
     */
    public void setIndPres_B25b(int indPres_B25b) {
        this.indPres_B25b = indPres_B25b;
    }

    /**
     * Gets the proc emi_ b26.
     *
     * @return the proc emi_ b26
     */
    public int getProcEmi_B26() {
        return procEmi_B26;
    }

    /**
     * Sets the proc emi_ b26.
     *
     * @param procEmi_B26 the new proc emi_ b26
     */
    public void setProcEmi_B26(int procEmi_B26) {
        this.procEmi_B26 = procEmi_B26;
    }

    /**
     * Gets the c cr t_ c21.
     *
     * @return the c cr t_ c21
     */
    public int getcCRT_C21() {
        return cCRT_C21;
    }

    /**
     * Sets the c cr t_ c21.
     *
     * @param cCRT_C21 the new c cr t_ c21
     */
    public void setcCRT_C21(int cCRT_C21) {
        this.cCRT_C21 = cCRT_C21;
    }

    /**
     * Gets the c cnp j_ c02.
     *
     * @return the c cnp j_ c02
     */
    public String getcCNPJ_C02() {
        return cCNPJ_C02;
    }

    /**
     * Sets the c cnp j_ c02.
     *
     * @param cCNPJ_C02 the new c cnp j_ c02
     */
    public void setcCNPJ_C02(String cCNPJ_C02) {
        this.cCNPJ_C02 = cCNPJ_C02;
    }

    /**
     * Gets the x nome_ c03.
     *
     * @return the x nome_ c03
     */
    public String getxNome_C03() {
        return xNome_C03;
    }

    /**
     * Sets the x nome_ c03.
     *
     * @param xNome_C03 the new x nome_ c03
     */
    public void setxNome_C03(String xNome_C03) {
        this.xNome_C03 = xNome_C03;
    }

    /**
     * Gets the x fant_ c04.
     *
     * @return the x fant_ c04
     */
    public String getxFant_C04() {
        return xFant_C04;
    }

    /**
     * Sets the x fant_ c04.
     *
     * @param xFant_C04 the new x fant_ c04
     */
    public void setxFant_C04(String xFant_C04) {
        this.xFant_C04 = xFant_C04;
    }

    /**
     * Gets the x lgr_ c06.
     *
     * @return the x lgr_ c06
     */
    public String getxLgr_C06() {
        return xLgr_C06;
    }

    /**
     * Sets the x lgr_ c06.
     *
     * @param xLgr_C06 the new x lgr_ c06
     */
    public void setxLgr_C06(String xLgr_C06) {
        this.xLgr_C06 = xLgr_C06;
    }

    /**
     * Gets the nro_ c07.
     *
     * @return the nro_ c07
     */
    public String getNro_C07() {
        return nro_C07;
    }

    /**
     * Sets the nro_ c07.
     *
     * @param nro_C07 the new nro_ c07
     */
    public void setNro_C07(String nro_C07) {
        this.nro_C07 = nro_C07;
    }

    /**
     * Gets the xCpl_C08.
     *
     * @return the xCpl_C08
     */
    public String getxCpl_C08() {
        return xCpl_C08;
    }

    /**
     * Sets the xCpl_C08.
     *
     * @param xCpl_C08 the new xCpl_C08.
     */
    public void setxCpl_C08(String xCpl_C08) {
        this.xCpl_C08 = xCpl_C08;
    }

    /**
     * Gets the x bairro_ c09.
     *
     * @return the x bairro_ c09
     */
    public String getxBairro_C09() {
        return xBairro_C09;
    }

    /**
     * Sets the x bairro_ c09.
     *
     * @param xBairro_C09 the new x bairro_ c09
     */
    public void setxBairro_C09(String xBairro_C09) {
        this.xBairro_C09 = xBairro_C09;
    }

    /**
     * Gets the c mun_ c10.
     *
     * @return the c mun_ c10
     */
    public String getcMun_C10() {
        return cMun_C10;
    }

    /**
     * Sets the c mun_ c10.
     *
     * @param cMun_C10 the new c mun_ c10
     */
    public void setcMun_C10(String cMun_C10) {
        this.cMun_C10 = cMun_C10;
    }

    /**
     * Gets the x mun_ c11.
     *
     * @return the x mun_ c11
     */
    public String getxMun_C11() {
        return xMun_C11;
    }

    /**
     * Sets the x mun_ c11.
     *
     * @param xMun_C11 the new x mun_ c11
     */
    public void setxMun_C11(String xMun_C11) {
        this.xMun_C11 = xMun_C11;
    }

    /**
     * Gets the x u f_ c12.
     *
     * @return the x u f_ c12
     */
    public String getxUF_C12() {
        return xUF_C12;
    }

    /**
     * Sets the x u f_ c12.
     *
     * @param xUF_C12 the new x u f_ c12
     */
    public void setxUF_C12(String xUF_C12) {
        this.xUF_C12 = xUF_C12;
    }

    /**
     * Gets the c ce p_ c13.
     *
     * @return the c ce p_ c13
     */
    public String getcCEP_C13() {
        return cCEP_C13;
    }

    /**
     * Sets the c ce p_ c13.
     *
     * @param cCEP_C13 the new c ce p_ c13
     */
    public void setcCEP_C13(String cCEP_C13) {
        this.cCEP_C13 = cCEP_C13;
    }

    /**
     * Gets the c pais_ c14.
     *
     * @return the c pais_ c14
     */
    public String getcPais_C14() {
        return cPais_C14;
    }

    /**
     * Sets the c pais_ c14.
     *
     * @param cPais_C14 the new c pais_ c14
     */
    public void setcPais_C14(String cPais_C14) {
        this.cPais_C14 = cPais_C14;
    }

    /**
     * Gets the x pais_ c15.
     *
     * @return the x pais_ c15
     */
    public String getxPais_C15() {
        return xPais_C15;
    }

    /**
     * Sets the x pais_ c15.
     *
     * @param xPais_C15 the new x pais_ c15
     */
    public void setxPais_C15(String xPais_C15) {
        this.xPais_C15 = xPais_C15;
    }

    /**
     * Gets the fone_ c16.
     *
     * @return the fone_ c16
     */
    public String getFone_C16() {
        return fone_C16;
    }

    /**
     * Sets the fone_ c16.
     *
     * @param fone_C16 the new fone_ c16
     */
    public void setFone_C16(String fone_C16) {
        this.fone_C16 = fone_C16;
    }

    /**
     * Gets the c i e_ c17.
     *
     * @return the c i e_ c17
     */
    public String getcIE_C17() {
        return cIE_C17;
    }

    /**
     * Sets the c i e_ c17.
     *
     * @param cIE_C17 the new c i e_ c17
     */
    public void setcIE_C17(String cIE_C17) {
        this.cIE_C17 = cIE_C17;
    }

    /**
     * Gets the c ies t_ c18.
     *
     * @return the c ies t_ c18
     */
    public String getcIEST_C18() {
        return cIEST_C18;
    }

    /**
     * Sets the c ies t_ c18.
     *
     * @param cIEST_C18 the new c ies t_ c18
     */
    public void setcIEST_C18(String cIEST_C18) {
        this.cIEST_C18 = cIEST_C18;
    }

    /**
     * Gets the c cp f_ e03.
     *
     * @return the c cp f_ e03
     */
    public String getcCPF_E03() {
        return cCPF_E03;
    }

    /**
     * Sets the c cp f_ e03.
     *
     * @param cCPF_E03 the new c cp f_ e03
     */
    public void setcCPF_E03(String cCPF_E03) {
        this.cCPF_E03 = cCPF_E03;
    }

    /**
     * Gets the x nome_ e04.
     *
     * @return the x nome_ e04
     */
    public String getxNome_E04() {
        return xNome_E04;
    }

    /**
     * Sets the x nome_ e04.
     *
     * @param xNome_E04 the new x nome_ e04
     */
    public void setxNome_E04(String xNome_E04) {
        this.xNome_E04 = xNome_E04;
    }

    /**
     * Gets the v b c_ w03.
     *
     * @return the v b c_ w03
     */
    public double getvBC_W03() {
        return vBC_W03;
    }

    /**
     * Sets the v b c_ w03.
     *
     * @param vBC_W03 the new v b c_ w03
     */
    public void setvBC_W03(double vBC_W03) {
        this.vBC_W03 = vBC_W03;
    }

    /**
     * Gets the v icm s_ w04.
     *
     * @return the v icm s_ w04
     */
    public double getvICMS_W04() {
        return vICMS_W04;
    }

    /**
     * Sets the v icm s_ w04.
     *
     * @param vICMS_W04 the new v icm s_ w04
     */
    public void setvICMS_W04(double vICMS_W04) {
        this.vICMS_W04 = vICMS_W04;
    }

    /**
     * Gets the v bcs t_ w05.
     *
     * @return the v bcs t_ w05
     */
    public double getvBCST_W05() {
        return vBCST_W05;
    }

    /**
     * Sets the v bcs t_ w05.
     *
     * @param vBCST_W05 the new v bcs t_ w05
     */
    public void setvBCST_W05(double vBCST_W05) {
        this.vBCST_W05 = vBCST_W05;
    }

    /**
     * Gets the v s t_ w06.
     *
     * @return the v s t_ w06
     */
    public double getvST_W06() {
        return vST_W06;
    }

    /**
     * Sets the v s t_ w06.
     *
     * @param vST_W06 the new v s t_ w06
     */
    public void setvST_W06(double vST_W06) {
        this.vST_W06 = vST_W06;
    }

    /**
     * Gets the v prod_ w07.
     *
     * @return the v prod_ w07
     */
    public double getvProd_W07() {
        return vProd_W07;
    }

    /**
     * Sets the v prod_ w07.
     *
     * @param vProd_W07 the new v prod_ w07
     */
    public void setvProd_W07(double vProd_W07) {
        this.vProd_W07 = vProd_W07;
    }

    /**
     * Gets the v frete_ w08.
     *
     * @return the v frete_ w08
     */
    public double getvFrete_W08() {
        return vFrete_W08;
    }

    /**
     * Sets the tpSaidaDanfe.
     *
     * @param tpSaidaDanfe the new tpSaidaDanfe
     */
    public void setTpSaidaDanfe(Integer tpSaidaDanfe) {
        this.tpSaidaDanfe = tpSaidaDanfe;
    }

    /**
     * Gets the tpSaidaDanfe.
     *
     * @return the tpSaidaDanfe
     */
    public Integer getTpSaidaDanfe() {
        return tpSaidaDanfe;
    }

    /**
     * Sets the tpLayoutDanfe.
     *
     * @param tpLayoutDanfe the new tpLayoutDanfe
     */
    public void setTpLayoutDanfe(Integer tpLayoutDanfe) {
        this.tpLayoutDanfe = tpLayoutDanfe;
    }

    /**
     * Gets the tpLayoutDanfe.
     *
     * @return the tpLayoutDanfe
     */
    public Integer getTpLayoutDanfe() {
        return tpLayoutDanfe;
    }

    /**
     * Sets the v frete_ w08.
     *
     * @param vFrete_W08 the new v frete_ w08
     */
    public void setvFrete_W08(double vFrete_W08) {
        this.vFrete_W08 = vFrete_W08;
    }

    /**
     * Gets the v seg_ w09.
     *
     * @return the v seg_ w09
     */
    public double getvSeg_W09() {
        return vSeg_W09;
    }

    /**
     * Sets the v seg_ w09.
     *
     * @param vSeg_W09 the new v seg_ w09
     */
    public void setvSeg_W09(double vSeg_W09) {
        this.vSeg_W09 = vSeg_W09;
    }

    /**
     * Gets the v desc_ w10.
     *
     * @return the v desc_ w10
     */
    public double getvDesc_W10() {
        return vDesc_W10;
    }

    /**
     * Sets the v desc_ w10.
     *
     * @param vDesc_W10 the new v desc_ w10
     */
    public void setvDesc_W10(double vDesc_W10) {
        this.vDesc_W10 = vDesc_W10;
    }

    /**
     * Gets the v i i_ w11.
     *
     * @return the v i i_ w11
     */
    public double getvII_W11() {
        return vII_W11;
    }

    /**
     * Sets the v i i_ w11.
     *
     * @param vII_W11 the new v i i_ w11
     */
    public void setvII_W11(double vII_W11) {
        this.vII_W11 = vII_W11;
    }

    /**
     * Gets the v pi s_ w13.
     *
     * @return the v pi s_ w13
     */
    public double getvPIS_W13() {
        return vPIS_W13;
    }

    /**
     * Sets the v pi s_ w13.
     *
     * @param vPIS_W13 the new v pi s_ w13
     */
    public void setvPIS_W13(double vPIS_W13) {
        this.vPIS_W13 = vPIS_W13;
    }

    /**
     * Gets the v cofin s_ w14.
     *
     * @return the v cofin s_ w14
     */
    public double getvCOFINS_W14() {
        return vCOFINS_W14;
    }

    /**
     * Sets the v cofin s_ w14.
     *
     * @param vCOFINS_W14 the new v cofin s_ w14
     */
    public void setvCOFINS_W14(double vCOFINS_W14) {
        this.vCOFINS_W14 = vCOFINS_W14;
    }

    /**
     * Gets the v outro_ w15.
     *
     * @return the v outro_ w15
     */
    public double getvOutro_W15() {
        return vOutro_W15;
    }

    /**
     * Sets the v outro_ w15.
     *
     * @param vOutro_W15 the new v outro_ w15
     */
    public void setvOutro_W15(double vOutro_W15) {
        this.vOutro_W15 = vOutro_W15;
    }

    /**
     * Gets the v n f_ w16.
     *
     * @return the v n f_ w16
     */
    public double getvNF_W16() {
        return vNF_W16;
    }

    /**
     * Sets the v n f_ w16.
     *
     * @param vNF_W16 the new v n f_ w16
     */
    public void setvNF_W16(double vNF_W16) {
        this.vNF_W16 = vNF_W16;
    }

    /**
     * Gets the mod frete_ x02.
     *
     * @return the mod frete_ x02
     */
    public int getModFrete_X02() {
        return modFrete_X02;
    }

    /**
     * Sets the mod frete_ x02.
     *
     * @param modFrete_X02 the new mod frete_ x02
     */
    public void setModFrete_X02(int modFrete_X02) {
        this.modFrete_X02 = modFrete_X02;
    }

    /**
     * Gets the items.
     *
     * @return the items
     */
    public List<BnfItem> getItems() {
        return items;
    }

    /**
     * Sets the items.
     *
     * @param items the new items
     */
    public void setItems(List<BnfItem> items) {
        this.items = items;
    }

    /**
     * Gets the pagamentos.
     *
     * @return the pagamentos
     */
    public List<BnfPag> getPagamentos() {
        return pagamentos;
    }

    /**
     * Sets the pagamentos.
     *
     * @param pagamentos the new pagamentos
     */
    public void setPagamentos(List<BnfPag> pagamentos) {
        this.pagamentos = pagamentos;
    }

    /**
     * Gets the v ip i_ w12.
     *
     * @return the v ip i_ w12
     */
    public double getvIPI_W12() {
        return vIPI_W12;
    }

    /**
     * Sets the v ip i_ w12.
     *
     * @param vIPI_W12 the new v ip i_ w12
     */
    public void setvIPI_W12(double vIPI_W12) {
        this.vIPI_W12 = vIPI_W12;
    }

    /**
     * Gets the v ecf serial.
     *
     * @return the vEcfSerial
     */
    public String getvEcfSerial() {
        return vEcfSerial;
    }

    /**
     * Sets the v ecf serial.
     *
     * @param vEcfSerial the vEcfSerial to set
     */
    public void setvEcfSerial(String vEcfSerial) {
        this.vEcfSerial = vEcfSerial;
    }

    /**
     * Gets the versao.
     *
     * @return the versao
     */
    public String getVersao() {
        return versao;
    }


    /**
     * Sets the versao.
     *
     * @param versao the new versao
     */
    public void setVersao(String versao) {
        this.versao = versao;
    }

    /**
     * Gets the vICMSDeson_W04a.
     *
     * @return the vICMSDeson_W04a
     */
    public double getvICMSDeson_W04a() {
        return vICMSDeson_W04a;
    }

    /**
     * Sets the versao.
     *
     * @param vICMSDeson_W04a
     */
    public void setvICMSDeson_W04a(double vICMSDeson_W04a) {
        this.vICMSDeson_W04a = vICMSDeson_W04a;
    }
}
