package org.qtproject.example.bnf;

public class BnfItem {

    /**
     * @param args
     */
    int nItem_H02;
    String cEAN_I03;
    String cProd_I02;
    String xProd_I04;
    String cNCM_I05;
    String cCFOP_I08;
    String uCom_I09;
    double qCom_I10;
    double vUnCom_I10a;
    double vProd_I11;
    String uTrib_I13;
    double qTrib_I14;
    double vUnTrib_I14a;
    double vDesc_I17;
    double vOutro_I17a;
    int indTot_I17b;
    int orig_N11;
    String cCST_N12;

    //Imposto ICMS
    String modBC_N13 = null;
    String pRedBC_N14 = null;
    String vBC_N15 = null;
    String pICMS_N16 = null;
    String vICMS_N17 = null;
    String modBCST_N18 = null;
    String pMVAST_N19 = null;
    String pRedBCST_N20 = null;
    String vBCST_N21 = null;
    String pICMSST_N22 = null;
    String vICMSST_N23 = null;
    String vICMSDeson_N27a = null;
    String motDesICMS_N28 = null;
    String vTotTrib_M02 = null;

    //Simples
    String cCSOSN_N12a = null;
    String vBCSTRet_N26 = null;
    String vICMSSTRet_N27 = null;
    String pCredSN_N29 = null;
    String vCredICMSSN_N30 = null;

    //Pis
    String CSTPis_Q06 = null;
    String vBCPis_Q07 = null;
    String pPIS_Q08 = null;
    String vPIS_Q09 = null;
    String qBCProdPis_Q10 = null;
    String vAliqProdPis_Q11 = null;

    //Cofins
    String CSTCofins_S06 = null;
    String vBCCofins_S07 = null;
    String pCOFINS_S08 = null;
    String vCOFINS_S11 = null;
    String qBCProdCofins_S09 = null;
    String vAliqProdCofins_S10 = null;

    public String getCSTPis_Q06() {
        return CSTPis_Q06;
    }

    public void setCSTPis_Q06(String cSTPis_Q06) {
        CSTPis_Q06 = cSTPis_Q06;
    }

    public String getvBCPis_Q07() {
        return vBCPis_Q07;
    }

    public void setvBCPis_Q07(String vBCPis_Q07) {
        this.vBCPis_Q07 = vBCPis_Q07;
    }

    public String getpPIS_Q08() {
        return pPIS_Q08;
    }

    public void setpPIS_Q08(String pPIS_Q08) {
        this.pPIS_Q08 = pPIS_Q08;
    }

    public String getvPIS_Q09() {
        return vPIS_Q09;
    }

    public void setvPIS_Q09(String vPIS_Q09) {
        this.vPIS_Q09 = vPIS_Q09;
    }

    public String getqBCProdPis_Q10() {
        return qBCProdPis_Q10;
    }

    public void setqBCProdPis_Q10(String qBCProdPis_Q10) {
        this.qBCProdPis_Q10 = qBCProdPis_Q10;
    }

    public String getvAliqProdPis_Q11() {
        return vAliqProdPis_Q11;
    }

    public void setvAliqProdPis_Q11(String vAliqProdPis_Q11) {
        this.vAliqProdPis_Q11 = vAliqProdPis_Q11;
    }

    public String getCSTCofins_S06() {
        return CSTCofins_S06;
    }

    public void setCSTCofins_S06(String cSTCofins_S06) {
        CSTCofins_S06 = cSTCofins_S06;
    }

    public String getvBCCofins_S07() {
        return vBCCofins_S07;
    }

    public void setvBCCofins_S07(String vBCCofins_S07) {
        this.vBCCofins_S07 = vBCCofins_S07;
    }

    public String getpCOFINS_S08() {
        return pCOFINS_S08;
    }

    public void setpCOFINS_S08(String pCOFINS_S08) {
        this.pCOFINS_S08 = pCOFINS_S08;
    }

    public String getvCOFINS_S11() {
        return vCOFINS_S11;
    }

    public void setvCOFINS_S11(String vCOFINS_S11) {
        this.vCOFINS_S11 = vCOFINS_S11;
    }

    public String getqBCProdCofins_S09() {
        return qBCProdCofins_S09;
    }

    public void setqBCProdCofins_S09(String qBCProdCofins_S09) {
        this.qBCProdCofins_S09 = qBCProdCofins_S09;
    }

    public String getvAliqProdCofins_S10() {
        return vAliqProdCofins_S10;
    }

    public void setvAliqProdCofins_S10(String vAliqProdCofins_S10) {
        this.vAliqProdCofins_S10 = vAliqProdCofins_S10;
    }

    public String getpRedBC_N14() {
        return pRedBC_N14;
    }

    public void setpRedBC_N14(String pRedBC_N14) {
        this.pRedBC_N14 = pRedBC_N14;
    }

    public String getModBCST_N18() {
        return modBCST_N18;
    }

    public void setModBCST_N18(String modBCST_N18) {
        this.modBCST_N18 = modBCST_N18;
    }

    public String getpMVAST_N19() {
        return pMVAST_N19;
    }

    public void setpMVAST_N19(String pMVAST_N19) {
        this.pMVAST_N19 = pMVAST_N19;
    }

    public String getpRedBCST_N20() {
        return pRedBCST_N20;
    }

    public void setpRedBCST_N20(String pRedBCST_N20) {
        this.pRedBCST_N20 = pRedBCST_N20;
    }

    public String getvBCST_N21() {
        return vBCST_N21;
    }

    public void setvBCST_N21(String vBCST_N21) {
        this.vBCST_N21 = vBCST_N21;
    }

    public String getpICMSST_N22() {
        return pICMSST_N22;
    }

    public void setpICMSST_N22(String pICMSST_N22) {
        this.pICMSST_N22 = pICMSST_N22;
    }

    public String getvICMSST_N23() {
        return vICMSST_N23;
    }

    public void setvICMSST_N23(String vICMSST_N23) {
        this.vICMSST_N23 = vICMSST_N23;
    }

    public String getvICMSDeson_N27a() {
        return vICMSDeson_N27a;
    }

    public void setvICMSDeson_N27a(String vICMSDeson_N27a) {
        this.vICMSDeson_N27a = vICMSDeson_N27a;
    }

    public String getMotDesICMS_N28() {
        return motDesICMS_N28;
    }

    public void setMotDesICMS_N28(String motDesICMS_N28) {
        this.motDesICMS_N28 = motDesICMS_N28;
    }

    public String getpCredSN_N29() {
        return pCredSN_N29;
    }

    public void setpCredSN_N29(String pCredSN_N29) {
        this.pCredSN_N29 = pCredSN_N29;
    }

    public String getvCredICMSSN_N30() {
        return vCredICMSSN_N30;
    }

    public void setvCredICMSSN_N30(String vCredICMSSN_N30) {
        this.vCredICMSSN_N30 = vCredICMSSN_N30;
    }

    public String getvBCSTRet_N26() {
        return vBCSTRet_N26;
    }

    public void setvBCSTRet_N26(String vBCSTRet_N26) {
        this.vBCSTRet_N26 = vBCSTRet_N26;
    }

    public String getvICMSSTRet_N27() {
        return vICMSSTRet_N27;
    }

    public void setvICMSSTRet_N27(String vICMSSTRet_N27) {
        this.vICMSSTRet_N27 = vICMSSTRet_N27;
    }

    public String getcCSOSN_N12a() {
        return cCSOSN_N12a;
    }

    public void setcCSOSN_N12a(String cCSOSN_N12a) {
        this.cCSOSN_N12a = cCSOSN_N12a;
    }

    public double getvDesc_I17() {
        return vDesc_I17;
    }

    public void setvDesc_I17(double vDesc_I17) {
        this.vDesc_I17 = vDesc_I17;
    }

    public double getvOutro_I17a() {
        return vOutro_I17a;
    }

    public void setvOutro_I17a(double vOutro_I17a) {
        this.vOutro_I17a = vOutro_I17a;
    }

    public int getnItem_H02() {
        return nItem_H02;
    }

    public void setnItem_H02(int nItem_H02) {
        this.nItem_H02 = nItem_H02;
    }

    public String getcEAN_I03() {
        return cEAN_I03;
    }

    public void setcEAN_I03(String cEAN_I03) {
        this.cEAN_I03 = cEAN_I03;
    }

    public String getcProd_I02() {
        return cProd_I02;
    }

    public void setcProd_I02(String cProd_I02) {
        this.cProd_I02 = cProd_I02;
    }

    public String getxProd_I04() {
        return xProd_I04;
    }

    public void setxProd_I04(String xProd_I04) {
        this.xProd_I04 = xProd_I04;
    }

    public String getcNCM_I05() {
        return cNCM_I05;
    }

    public void setcNCM_I05(String cNCM_I05) {
        this.cNCM_I05 = cNCM_I05;
    }

    public String getcCFOP_I08() {
        return cCFOP_I08;
    }

    public void setcCFOP_I08(String cCFOP_I08) {
        this.cCFOP_I08 = cCFOP_I08;
    }

    public String getuCom_I09() {
        return uCom_I09;
    }

    public void setuCom_I09(String uCom_I09) {
        this.uCom_I09 = uCom_I09;
    }

    public double getqCom_I10() {
        return qCom_I10;
    }

    public void setqCom_I10(double qCom_I10) {
        this.qCom_I10 = qCom_I10;
    }

    public double getvUnCom_I10a() {
        return vUnCom_I10a;
    }

    public void setvUnCom_I10a(double vUnCom_I10a) {
        this.vUnCom_I10a = vUnCom_I10a;
    }

    public double getvProd_I11() {
        return vProd_I11;
    }

    public void setvProd_I11(double vProd_I11) {
        this.vProd_I11 = vProd_I11;
    }

    public String getuTrib_I13() {
        return uTrib_I13;
    }

    public void setuTrib_I13(String uTrib_I13) {
        this.uTrib_I13 = uTrib_I13;
    }

    public double getqTrib_I14() {
        return qTrib_I14;
    }

    public void setqTrib_I14(double qTrib_I14) {
        this.qTrib_I14 = qTrib_I14;
    }

    public double getvUnTrib_I14a() {
        return vUnTrib_I14a;
    }

    public void setvUnTrib_I14a(double vUnTrib_I14a) {
        this.vUnTrib_I14a = vUnTrib_I14a;
    }

    public int getIndTot_I17b() {
        return indTot_I17b;
    }

    public void setIndTot_I17b(int indTot_I17b) {
        this.indTot_I17b = indTot_I17b;
    }

    public int getOrig_N11() {
        return orig_N11;
    }

    public void setOrig_N11(int orig_N11) {
        this.orig_N11 = orig_N11;
    }

    public String getcCST_N12() {
        return cCST_N12;
    }

    public void setcCST_N12(String cCST_N12) {
        this.cCST_N12 = cCST_N12;
    }

    public String getModBC_N13() {
        return modBC_N13;
    }

    public void setModBC_N13(String modBC_N13) {
        this.modBC_N13 = modBC_N13;
    }

    public String getvBC_N15() {
        return vBC_N15;
    }

    public void setvBC_N15(String vBC_N15) {
        this.vBC_N15 = vBC_N15;
    }

    public String getpICMS_N16() {
        return pICMS_N16;
    }

    public void setpICMS_N16(String pICMS_N16) {
        this.pICMS_N16 = pICMS_N16;
    }

    public String getvICMS_N17() {
        return vICMS_N17;
    }

    public void setvICMS_N17(String vICMS_N17) {
        this.vICMS_N17 = vICMS_N17;
    }

    public String getvTotTrib_M02() {
        return vTotTrib_M02;
    }

    public void setvTotTrib_M02(String vTotTrib_M02) {
        this.vTotTrib_M02 = vTotTrib_M02;
    }
}
