package org.qtproject.example.bnf;

public class BnfPag {

    /**
     * @param args
     */
    private int tPag_YA02;
    private double vPag_YA03;

    public int getTipo() {
        return tPag_YA02;
    }

    public void setTipo(int tipo) {
        this.tPag_YA02 = tipo;
    }

    public double getValor() {
        return vPag_YA03;
    }

    public void setValor(double valor) {
        this.vPag_YA03 = valor;
    }
}
