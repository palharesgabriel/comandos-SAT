package org.qtproject.example;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.net.Uri;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URLDecoder;
import java.util.List;
import android.widget.Toast;

import org.qtproject.example.Helper;
import org.qtproject.example.util.LogFile;

import org.qtproject.example.discovery.DiscoveryThread;
import org.qtproject.example.discovery.DiscoveryInfo;

public class Commands {

    public static void getVersion(Context ctx){
        LogFile.writeToLog(ctx, "Comunicação recebida do Servidor");

        Intent i = new Intent(Actions.VERSION);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //i.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
        i.addFlags(Helper.getIntentFlagToVersion());
        i.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);

        LogFile.writeToLog(ctx, "Intent de Versão para o FM");

        ctx.startActivity(i);
    }

    public static void checkoutOperation(Context ctx, String json, String serieModelo){
        Log.i("CheckoutBody", "Inicio");
        Log.i("Serie e Modelo",serieModelo);
        Log.i("JSON", json);
        LogFile.writeToLog(ctx, "Comunicação recebida do Servidor");

        try {
            File userFile = new File(Environment.getExternalStorageDirectory(), "bnf.json");
            Uri fileUri = userFile.exists() ?
                   Uri.fromFile(userFile) :
                   Helper.createCacheFile(ctx, json, "json");

            Log.i("File", fileUri.toString());

            Intent checkoutIntent = new Intent(Actions.CHECKOUT);
            checkoutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //checkoutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
            checkoutIntent.addFlags(Helper.getIntentFlagToVersion());
            checkoutIntent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            checkoutIntent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);

            checkoutIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            if (!userFile.exists())
              checkoutIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

            checkoutIntent.setDataAndType(fileUri, "application/json");

            LogFile.writeToLog(ctx, "Intent de Checkout para o FM");
            ctx.startActivity(checkoutIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void invoiceLookup(Context ctx, String id){
        LogFile.writeToLog(ctx, "Comunicação recebida do Servidor");

        Intent i = new Intent(Actions.INVOICE_LOOKUP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //i.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
        i.addFlags(Helper.getIntentFlagToVersion());
        i.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);

        i.putExtra("EXTRA_IN_ACCESS_KEY_STRING", id);

        LogFile.writeToLog(ctx, "Intent de Lookup para o FM");
        ctx.startActivity(i);
    }

}
