package org.qtproject.example;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import org.qtproject.example.util.LogFile;

public class BroadcastRegisters {

    public static BroadcastReceiver versionReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            LogFile.writeToLog(context, "Resposta recebida do FM");

            String result = intent.getStringExtra("EXTRA_OUT_MESSAGE_STRING");

            if(result == null){
                QtBridge.getVersionFromAndroid("SEM CONTEUDO");
            }else{
                String retorno = Helper.retornarResposta(intent);
                QtBridge.getVersionFromAndroid(retorno);
            }
       }
    };

    public static BroadcastReceiver baseReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String result = intent.getStringExtra("MESSAGE_STRING");

            if(result == null){
                //QtBridge.getResultFromAndroid("SEM CONTEUDO");
            }else{
                //Helper.retornarResposta(intent);
            }
        }
    };

    public static BroadcastReceiver checkoutReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            LogFile.writeToLog(context, "Resposta recebida do FM");

            String result = intent.getStringExtra("EXTRA_OUT_CHECKOUT_ACCESS_KEY_STRING");
            if(result == null){
                QtBridge.getCheckoutFromAndroid("Venda não foi executada com sucesso.");
            }else{
                String retorno = Helper.retornarResposta(intent);
                QtBridge.getCheckoutFromAndroid(retorno);
            }
        }
    };

    public static BroadcastReceiver lookupReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogFile.writeToLog(context, "Resposta recebida do FM");

            int state = intent.getIntExtra("EXTRA_OUT_INVOICE_LOOKUP_SERIES_INT", -1);

            if(state == -1){
                QtBridge.getLookupFromAndroid("Não encontrado");
            }else{
                String retorno = Helper.retornarResposta(intent);
                QtBridge.getLookupFromAndroid(retorno);
            }
        }
    };

}
