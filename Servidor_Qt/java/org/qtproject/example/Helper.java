package org.qtproject.example;

import android.content.Context;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.content.Intent;
import org.json.JSONObject;
import android.os.Bundle;
import android.util.Log;
import android.os.Build;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.stream.Format;
import org.simpleframework.xml.stream.Style;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.qtproject.example.bnf.BnfData;
import org.qtproject.example.util.LogFile;

public class Helper {
    static final Integer FIX_BNF_CODE = null;

    public static String retornarResposta(Intent intent){
        String retorno = "";
        JSONObject jObj = new JSONObject();

        try {
            Bundle bundle = intent.getExtras();

            if (bundle != null) {
                for (String key : bundle.keySet()) {
                    Object value = bundle.get(key);

                    String data = ((value == null) ? "null" : value.toString());
                    jObj.put(key, data);
                }
            }

            retorno = jObj.toString();
        }catch(Exception e){
            retorno = "Erro na formação da resposta";
        }

        return retorno;
    }

    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"; // W3C standard for XML
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT) {
        @Override
        public StringBuffer format(Date date, StringBuffer toAppendTo, java.text.FieldPosition pos) {
            StringBuffer toFix = super.format(date, toAppendTo, pos);
            return toFix.insert(toFix.length() - 2, ':');
        }
    };

    public static Uri exposeFile(Context context, File file) throws Exception {
        if (FIX_BNF_CODE == null)
            return Uri.fromFile(file);

        BufferedReader br = new BufferedReader(new FileReader(file));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }

            String bnf = sb.toString();
            String ext = file.getName().substring(file.getName().lastIndexOf('.') + 1);

            return createCacheFile(context, bnf, ext.toLowerCase());
        } finally {
            br.close();
        }
    }

    public static Uri shareData(Context context, String content, String extension) throws Exception {
        return shareData(context, new ByteArrayInputStream(content.getBytes(Charset.forName("UTF-8"))), extension);
    }

    public static Uri shareData(Context context, File content) throws Exception {
        return shareData(context, new FileInputStream(content), content.getName().substring(content.getName().lastIndexOf('.') + 1));
    }

    private static Uri shareData(Context context, InputStream content, String extension) throws Exception {
        File dir = getShareDir(context);
        File dataFile = File.createTempFile("data_", "." + extension, dir);

        OutputStream writer = new FileOutputStream(dataFile);

        byte[] buffer = new byte[1024];
        while(true) {
            int count = content.read(buffer);
            if (count <= 0)
                break;
            writer.write(buffer, 0, count);
        }
        writer.close();

        return FileProvider.getUriForFile(context, "br.com.bematech.fm.client.fileprovider", dataFile);
    }

    private static File getShareDir(Context context) throws IOException {
        File dir = new File(context.getCacheDir(), "share");
        if(!dir.exists() && !dir.mkdirs())
            throw new IOException("Share directory could not be created");
        return dir;
    }

    public static Uri createCacheFile(Context context, String content, String extension) throws Exception {

        if(FIX_BNF_CODE != null) {
            if(extension.equals("json"))
                content = fixBnfJson(context, content);
            else
                content = fixBnfXml(context, content);
        }

        File dir = getShareDir(context);

        File bnfFile = File.createTempFile("checkout_", "." + extension, dir);
        FileWriter writer = new FileWriter(bnfFile);
        writer.write(content);
        writer.close();

        return FileProvider.getUriForFile(context, "br.com.bematech.fm.client.fileprovider", bnfFile);
    }

    private static <T> T jsonToObj(String jsonStr, Class<T> clazz) {
        Gson gson = new GsonBuilder().setVersion(1.0).serializeNulls().create();
        return gson.fromJson(jsonStr, clazz);
    }

    private static String objToJson(Object obj) {
        Gson gson = new GsonBuilder().setVersion(1.0).serializeNulls().create();
        return gson.toJson(obj);
    }

    private static String objToXmlString(Object obj) throws Exception {
        StringWriter sw = new StringWriter();
        Serializer serializer = new Persister();
        serializer.write(obj, sw);
        return sw.toString();
    }

    private static <T> T xmlStringToObj(String xml, Class<T> clazz) throws Exception {
        StringReader sr = new StringReader(xml);

        Style caseInsensitive = new Style(){
            @Override
            public String getElement(String arg0)
            {
                return arg0.toLowerCase();
            }

            @Override
            public String getAttribute(String arg0)
            {
                return arg0.toLowerCase();
            }
        };

        Serializer serializer = new Persister(new AnnotationStrategy(), new Format(caseInsensitive));
        return serializer.read(clazz, sr);
    }

    private static void fixBnfData(Context context, BnfData bnfData) {
        if (FIX_BNF_CODE == null)
            return;

        bnfData.setDhEmi_B09(dateFormatter.format(new Date()));
        bnfData.setSerie_B07("102");
        int value = context.getSharedPreferences("dummy", 0).getInt("nota", FIX_BNF_CODE);
        bnfData.setnNF_B08(value);
        context.getSharedPreferences("dummy", 0).edit().putInt("nota", value + 1).commit();
    }

    private static String fixBnfJson(Context context, String bnfJson) {
        if (FIX_BNF_CODE == null)
            return bnfJson;

        BnfData bnfData = jsonToObj(bnfJson, BnfData.class);
        fixBnfData(context, bnfData);
        return objToJson(bnfData);
    }

    private static String fixBnfXml(Context context, String bnfXml) throws Exception {
        if (FIX_BNF_CODE == null)
            return bnfXml;

        BnfData bnfData = xmlStringToObj(bnfXml, BnfData.class);
        fixBnfData(context, bnfData);
        return objToXmlString(bnfData);
    }

    public static String decodeUrl(String url) {
        try {
            return URLDecoder.decode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int getIntentFlagToVersion(){
//        if(Build.VERSION.SDK_INT > 21){
//            return Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
//        }
//        else{
            return Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
//        }
    }
}
