package org.qtproject.example;

public class Actions {
    public final static String BASE_ACTIVITY  = "br.com.bematech.fm.action.BASE_ACTIVITY";
    public final static String CHECKOUT = "br.com.bematech.fm.action.CHECKOUT";
    public final static String INVOICE_LOOKUP = "br.com.bematech.fm.action.INVOICE_LOOKUP";
    public final static String VERSION  = "br.com.bematech.fm.action.APP_VERSION";
}
