package org.qtproject.example;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.widget.Toast;
import android.util.Base64;

import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfiumCore;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by bimolaboratorio on 15/09/17.
 */

public class PdfToBmpParser {
    static void generateBmpFromPDF(Context context){
        File dir = new File(Environment.getExternalStorageDirectory() + File.separator + "Documents");
        File qtRestDir = new File(dir, "QtRest");
        File pdfDir = new File(qtRestDir, "pdf-images");

        if(!pdfDir.exists()){
            pdfDir.mkdirs();
        }
        File file = new File(pdfDir, "PdfTemp.pdf");
        File fileImg = new File(pdfDir, "temp.png");
        ParcelFileDescriptor fd = null;
        if(file.exists()){
            try {
                fd = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_WRITE);
                //Toast.makeText(context, "Sucesso na abertura do arquivo", Toast.LENGTH_LONG).show();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                //Toast.makeText(context, "Falha na abertura do arquivo", Toast.LENGTH_LONG).show();
            }

            int pageNum = 0;
            PdfiumCore pdfiumCore = new PdfiumCore(context);
            Bitmap bitmap = null;

            try {
                PdfDocument pdfDocument = pdfiumCore.newDocument(fd);
                pdfiumCore.openPage(pdfDocument, pageNum);
                int width = pdfiumCore.getPageWidthPoint(pdfDocument, pageNum);
                int height = pdfiumCore.getPageHeight(pdfDocument, pageNum);
                bitmap = Bitmap.createBitmap(606, 1975, Bitmap.Config.ARGB_8888);
                pdfiumCore.renderPageBitmap(pdfDocument, bitmap, pageNum, 0, 0, 606, 1975);
                pdfiumCore.closeDocument(pdfDocument);
                //Toast.makeText(context, "Sucesso na criação do bmp", Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
                //Toast.makeText(context, "Falha na criação do bmp", Toast.LENGTH_LONG).show();
            }

            try {
                FileOutputStream fout = new FileOutputStream(fileImg);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fout);
                fout.flush();
                fout.close();
                //Toast.makeText(context, "Sucesso na criação do arquivo bmp", Toast.LENGTH_LONG).show();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                //Toast.makeText(context, "Falha na criação do arquivo bmp", Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
                //Toast.makeText(context, "Falha no fechamento do arquivo bmp", Toast.LENGTH_LONG).show();
            }
        }
    }

    static void decodePdfFromBase64(String imageData)
    {
        byte[] imgBytesData = android.util.Base64.decode(imageData, Base64.DEFAULT);
        File dir = new File(Environment.getExternalStorageDirectory() + File.separator + "Documents");
        File qtRestDir = new File(dir, "QtRest");
        File pdfDir = new File(qtRestDir, "pdf-images");
        if(!pdfDir.exists()){
            pdfDir.mkdirs();
        }
        File file = new File(pdfDir, "PdfTemp.pdf");
        FileOutputStream fileOutputStream;
        try{
            fileOutputStream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }

        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
        try{
            bufferedOutputStream.write(imgBytesData);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try{
                bufferedOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
