package org.qtproject.example;

public class Nota{
    public String serie;
    public String modelo;

    public Nota(String s, String m){
        this.serie = s;
        this.modelo = m;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Nota other = (Nota) obj;
        if (serie != other.serie)
            return false;
        if (modelo != other.modelo)
            return false;
        return true;
    }
}
