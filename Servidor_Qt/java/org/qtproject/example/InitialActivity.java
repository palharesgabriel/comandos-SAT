/*package org.qtproject.example;

import org.qtproject.example.R;
import org.qtproject.qt5.android.bindings.QtActivity;
import org.qtproject.qt5.android.bindings.QtService;
import org.qtproject.example.BackgroundService;
import android.content.Intent;
import android.app.Activity;
import android.util.Log;
import android.os.Bundle;
import android.widget.Toast;

public class InitialActivity extends QtActivity {

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Log.i("Activity", "Starting service!");
        Intent intent = new Intent(this, BackgroundService.class);
        startService(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
    @Override
    public void onPause() {
        super.onPause();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}*/

package org.qtproject.example;

import android.app.Service;
import android.os.IBinder;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.app.Activity;
import org.qtproject.qt5.android.bindings.QtService;
import org.qtproject.qt5.android.bindings.QtActivity;
import android.util.Log;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import android.widget.Toast;
import android.view.View;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;

public class InitialActivity extends QtActivity {

    BroadcastReceiver versionReceiver = BroadcastRegisters.versionReceiver;
    BroadcastReceiver baseReceiver = BroadcastRegisters.baseReceiver;
    BroadcastReceiver checkoutReceiver = BroadcastRegisters.checkoutReceiver;
    BroadcastReceiver lookupReceiver = BroadcastRegisters.lookupReceiver;

    // Helper para diminuir uma linha no bloco principal
    private static IntentFilter createFilter(String action){
        IntentFilter filter = new IntentFilter(action);
        filter.addAction(action);

        return filter;
    }

    // Registrar os BroadcastReceivers aqui
    private void registerReceivers(){
        IntentFilter baseFilter = createFilter(Actions.BASE_ACTIVITY);
        registerReceiver(baseReceiver, baseFilter);

        IntentFilter checkoutFilter = createFilter(Actions.CHECKOUT);
        registerReceiver(checkoutReceiver, checkoutFilter);

        IntentFilter lookupFilter = createFilter(Actions.INVOICE_LOOKUP);
        registerReceiver(lookupReceiver, lookupFilter);

        IntentFilter versionFilter = createFilter(Actions.VERSION);
        registerReceiver(versionReceiver, versionFilter);
    }

    // Desregistrar (?) aqui
    private void unregisterReceivers(){
        unregisterReceiver(baseReceiver);
        unregisterReceiver(checkoutReceiver);
        unregisterReceiver(lookupReceiver);
        unregisterReceiver(versionReceiver);
    }

   @Override
   public void onCreate(Bundle bundle) {
      super.onCreate(bundle);
      Log.i("Service", "Service created!");

      registerReceivers();
      Toast.makeText(this, "***SERVIDOR REST***", Toast.LENGTH_SHORT).show();
//      Intent serviceIntent = new Intent(this, BackgroundService.class);
//      startService(serviceIntent);
//      finish();
   }

   @Override
   public void onDestroy() {
      super.onDestroy();
      unregisterReceivers();
   }
}

