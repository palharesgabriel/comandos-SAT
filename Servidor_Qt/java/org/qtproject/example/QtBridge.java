package org.qtproject.example;

// Utilizado para comunicação com o .cpp
public class QtBridge {
    public static native void getCheckoutFromAndroid(String res);
    public static native void getLookupFromAndroid(String res);
    public static native void getVersionFromAndroid(String res);

    // Descobrimento
    public static native void descobrimentoConcluido(String res);
}
