#ifndef SERIALDISCOVERYAGENT_H
#define SERIALDISCOVERYAGENT_H

#include <QObject>
#include <QDebug>
#include <QString>
#include <QByteArray>
#include <QSignalMapper>
#include <QTimer>
#include <QEventLoop>
#include <QSerialPort>
#include <QSerialPortInfo>

#include "dbmanager.h"
#include "devices/serialdeviceinfo.h"
#include "serialmanager.h"

class SerialDiscoveryAgent : public QObject
{
    Q_OBJECT
public:
    explicit SerialDiscoveryAgent(QObject *parent = 0);

    void SerialDeviceDiscovery(SerialDeviceInfo, const QString& = "", void (*f)(const QString&) = nullptr);
private:
};

#endif // SERIALDISCOVERYAGENT_H
