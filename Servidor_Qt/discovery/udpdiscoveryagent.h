#ifndef DISCOVERYAGENT_H
#define DISCOVERYAGENT_H

#include <QObject>
#include <QUdpSocket>
#include <QDebug>
#include <QString>
#include <QByteArray>
#include <QSignalMapper>
#include <QTimer>
#include <QEventLoop>

#include "udpdiscoveryinfo.h"
#include "dbmanager.h"

class UDPDiscoveryAgent : public QObject
{
    Q_OBJECT
public:
    explicit UDPDiscoveryAgent(QObject *parent = 0);

    void UDPBroadcastDeviceDiscovery(UDPDiscoveryInfo,int);
    void UDPBroadcastDeviceDiscovery(QVector<UDPDiscoveryInfo>,int);
private:
    QUdpSocket *udpSocketGet;
private slots:
    void UDPBroadcastDeviceFound(QString);
};

#endif // DISCOVERYAGENT_H
