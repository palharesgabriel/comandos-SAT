#ifndef DISCOVERYINFO_H
#define DISCOVERYINFO_H

#include <QString>
#include <QMap>
#include <QVector>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>

struct UDPDiscoveryField
{
    QString name;
    int begin;

    UDPDiscoveryField(){}

    UDPDiscoveryField(QString name, int begin)
    {
        this->name = name;
        this->begin = begin;
    }
};

class UDPDiscoveryInfo
{
public:
    QString tagName; // Como é salvo no banco de dados
    int originPort; // Porta de origem
    int destPort; // Porta de destino
    QString discoveryString; // O que é enviado para a rede
    QString resultString; // O que é recebido da rede
    QString MACFilter = "";
    QVector<UDPDiscoveryField> fields; // Os campos que são necessários

    UDPDiscoveryInfo();
    UDPDiscoveryInfo(QString tag, int origin, int dest, QString discovery, QString result);
    UDPDiscoveryInfo(QString jsonText);

    void setMACFilter(QString filter);
    void addIPField(int begin);
    void addMACField(int begin);
    QString toJsonText(void);
};

#endif // DISCOVERYINFO_H
