#include "serialdiscoveryagent.h"

SerialDiscoveryAgent::SerialDiscoveryAgent(QObject *parent) : QObject(parent){}
//---------------------------------------------------------------------------------------------------------------
void SerialDiscoveryAgent::SerialDeviceDiscovery(SerialDeviceInfo discoveryDev, const QString& comando, void (*f)(const QString&))
{
    qDebug() << "Find me serial device" << discoveryDev.tag << discoveryDev.pid << discoveryDev.vid << "...";

    QList<QSerialPortInfo> devices = QSerialPortInfo::availablePorts();
    bool bstatus;
    SerialManager *smanager = new SerialManager(this);

    quint16 iPid = discoveryDev.pid.toUInt(&bstatus,16);
    quint16 iVid = discoveryDev.vid.toUInt(&bstatus,16);

    qDebug() << "uint:" << iPid << iVid;

    if(devices.size() == 0)
    {
        return;
    }

    foreach(QSerialPortInfo dev, devices)
    {
        qDebug() << "Found device" << dev.description() << "on port" << dev.portName() << "from"
                     << dev.manufacturer() << "serial number"<< dev.serialNumber() << "pid vid"
                         << dev.productIdentifier() << dev.vendorIdentifier();

        if(dev.vendorIdentifier() == iVid && dev.productIdentifier() == iPid)
        {
            qDebug() << "Dispositivo que estava sendo buscado é esse [^]";
            QString data = smanager->sendToSerialPort(dev.portName(), comando);

            if(data.length() > 0 && f != nullptr)
            {
                // algo foi retornado aqui
                f(data);
            }
        }
    }

    DbManager::printAllSerialDevices();
}
