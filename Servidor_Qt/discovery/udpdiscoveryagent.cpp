#include "udpdiscoveryagent.h"

UDPDiscoveryAgent::UDPDiscoveryAgent(QObject *parent) : QObject(parent){}
//---------------------------------------------------------------------------------------------------------------
void UDPDiscoveryAgent::UDPBroadcastDeviceDiscovery(UDPDiscoveryInfo info, int discoveryDelay)
{
    QSignalMapper* signalMapper = new QSignalMapper (this) ;

    udpSocketGet  = new QUdpSocket(this);
    QHostAddress bcast = QHostAddress(QHostAddress::Broadcast);

    udpSocketGet->bind(info.originPort, QUdpSocket::ShareAddress);

    connect(udpSocketGet, SIGNAL(readyRead()), signalMapper, SLOT(map())) ;

    signalMapper->setMapping(udpSocketGet, info.toJsonText()) ;

    connect(signalMapper, SIGNAL(mapped(const QString &)), this, SLOT(UDPBroadcastDeviceFound(const QString &))) ;

    QString message = info.discoveryString;
    QByteArray datagram = message.toUtf8();
    udpSocketGet->writeDatagram(datagram, bcast, info.destPort);

    QTimer t;
    t.start(discoveryDelay*1000);

    QEventLoop loop;
    connect(&t, SIGNAL(timeout()), &loop, SLOT(quit()));
    loop.exec();
    udpSocketGet->close();
}
//---------------------------------------------------------------------------------------------------------------
void UDPDiscoveryAgent::UDPBroadcastDeviceDiscovery(QVector<UDPDiscoveryInfo> deviceInfo, int discoveryDelay)
{
    foreach(UDPDiscoveryInfo info, deviceInfo)
    {
        UDPBroadcastDeviceDiscovery(info, discoveryDelay);
    }
}
//---------------------------------------------------------------------------------------------------------------
void UDPDiscoveryAgent::UDPBroadcastDeviceFound(QString jsonText)
{
    qDebug() << jsonText;

    QByteArray datagram;
    QHostAddress printerIp;
    quint16 printerPort;
    UDPDiscoveryInfo info = UDPDiscoveryInfo(jsonText);

    while (udpSocketGet->hasPendingDatagrams())
    {
        datagram.resize(udpSocketGet->pendingDatagramSize());
        udpSocketGet->readDatagram(datagram.data(), datagram.size(), &printerIp, &printerPort);

        if(QString::compare(info.resultString, datagram.mid(0, info.resultString.length())) == 0)
        {
            QString macAddr;
            QString ipAddr;
            bool ok;
            QString code;
            int readSize = 0;

            foreach(UDPDiscoveryField field, info.fields)
            {
                readSize = QString::compare(field.name, "MAC") == 0 ? 6 : 4;
                QByteArray desiredData = datagram.mid(field.begin, readSize);

                for(int i=0;i<desiredData.length();i++)
                {
                    if(QString::compare(field.name, "MAC") == 0)
                    {
                        if(i)
                        {
                            macAddr.append(":");
                        }

                        code = desiredData.mid(i,1).toHex();
                        macAddr.append(QString("%1").arg(code.toInt(&ok, 16), 1, 16, QChar('0')));
                    }
                    else // IP
                    {
                        if(i)
                        {
                            ipAddr.append(".");
                        }

                        code = desiredData.mid(i,1).toHex();
                        ipAddr.append(QString("%1").arg(code.toInt(&ok, 16), 1, 10, QChar('0')));
                    }
                }
            }

            if(info.MACFilter == "" || macAddr.compare(info.MACFilter) == 0)
            {
                DbManager::addNetworkDevice(info.tagName, ipAddr, macAddr);
            }
        }
    }

    DbManager::printAllNetworkDevices();
}
