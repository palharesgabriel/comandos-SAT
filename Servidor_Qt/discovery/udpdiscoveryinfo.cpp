#include "udpdiscoveryinfo.h"

#define TAG_NAME            "tag"
#define ORIGIN_PORT         "originPort"
#define DEST_PORT           "destPort"
#define DISCOVERY_STRING    "discoveryString"
#define RESULT_STRING       "resultString"
#define MAC_FILTER          "filtroMAC"

#define FIELDS_OBJ          "fields"
#define FIELD_ID            "name"
#define INDEX_BEGIN         "begin"

UDPDiscoveryInfo::UDPDiscoveryInfo(){}
//-----------------------------------------------------------------------------------------------
UDPDiscoveryInfo::UDPDiscoveryInfo(QString tag, int origin, int dest, QString discovery, QString result)
{
    tagName = tag;
    originPort = origin;
    destPort = dest;
    discoveryString = discovery;
    resultString = result;
}
//-----------------------------------------------------------------------------------------------
void UDPDiscoveryInfo::setMACFilter(QString filter)
{
    MACFilter = filter;
}
//-----------------------------------------------------------------------------------------------
UDPDiscoveryInfo::UDPDiscoveryInfo(QString jsonText)
{
    QJsonDocument doc = QJsonDocument::fromJson(jsonText.toUtf8());
    QJsonObject objectJason = doc.object();

    tagName = objectJason.value(TAG_NAME).toString();
    originPort = objectJason.value(ORIGIN_PORT).toInt();
    destPort = objectJason.value(DEST_PORT).toInt();
    discoveryString = objectJason.value(DISCOVERY_STRING).toString();
    resultString = objectJason.value(RESULT_STRING).toString();
    MACFilter    = objectJason.value(MAC_FILTER).toString();

    QJsonArray fieldArray;
    fieldArray = objectJason.value(FIELDS_OBJ).toArray();

    foreach(QJsonValue jval, fieldArray)
    {
        QJsonObject jobj = jval.toObject();
        int begin = jobj.value(INDEX_BEGIN).toInt();

        if(QString::compare(jobj.value(FIELD_ID).toString(), "IP") == 0)
        {
            this->addIPField(begin);
        }
        else
        {
            this->addMACField(begin);
        }
    }
}
//-----------------------------------------------------------------------------------------------
void UDPDiscoveryInfo::addIPField(int begin)
{
    UDPDiscoveryField f = UDPDiscoveryField("IP",begin);
    fields.append(f);
}
//-----------------------------------------------------------------------------------------------
void UDPDiscoveryInfo::addMACField(int begin)
{
    UDPDiscoveryField f = UDPDiscoveryField("MAC",begin);
    fields.append(f);
}
//-----------------------------------------------------------------------------------------------
QString UDPDiscoveryInfo::toJsonText()
{
    QJsonObject obj;
    obj.insert(TAG_NAME, QJsonValue::fromVariant(this->tagName));
    obj.insert(ORIGIN_PORT, QJsonValue::fromVariant(this->originPort));
    obj.insert(DEST_PORT, QJsonValue::fromVariant(this->destPort));
    obj.insert(DISCOVERY_STRING, QJsonValue::fromVariant(this->discoveryString));
    obj.insert(RESULT_STRING, QJsonValue::fromVariant(this->resultString));
    obj.insert(MAC_FILTER, QJsonValue::fromVariant(this->MACFilter));

    QJsonArray fieldArray;

    foreach(UDPDiscoveryField f, fields)
    {
        QJsonObject fieldObj;
        fieldObj.insert(FIELD_ID, QJsonValue::fromVariant(f.name));
        fieldObj.insert(INDEX_BEGIN, QJsonValue::fromVariant(f.begin));

        fieldArray.push_back(fieldObj);
    }

    obj.insert(FIELDS_OBJ, fieldArray);

    QJsonDocument doc(obj);

    return doc.toJson();
}
