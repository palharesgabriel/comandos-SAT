import QtQuick 2.0

Rectangle {
    width: 500
    height: 500
    color: "white"

    Column {
        anchors.fill: parent

        Text {
            id: title
            objectName: "logText"
            color: "black"
            font.pixelSize: parent.width / 28
            text: "Esperando uma requisição..."
            width: parent.width
            horizontalAlignment: Text.AlignLeft
        }
    }
}
