#ifndef PDFMANAGER_H
#define PDFMANAGER_H

#include <QImage>
#include <QString>
#include <QStandardPaths>
#include <QDir>
#include <QDebug>
#include <QString>
#include <QByteArray>
#include <QFile>
#include <QCoreApplication>
#include <QProcess>


class PdfManager
{
public:
    PdfManager();
    static QImage getPdfImageFromAndroid();
    static QString createPdfFromBase64(QString);
    static QString callGhostScriptToConvertPDFtoJpeg(QString, QString);
};

#endif // PDFMANAGER_H
