#include "comandossat.h"

ComandosSAT::ComandosSAT()
{

}

void ComandosSAT::tratarDescoberta(const QString& resposta)
{
    // aqui deveria vir o conteúdo da resposta, se tudo estiver correto
}


QString ComandosSAT::ativarSAT(const QMap<QString, QVariant> json)
{
    //complemento
    QString tamanho;

    //parametros
    QString numeroSessao     = json.value("numeroSessao").toString();
    QString subComando       = json.value("subComando").toString();
    QString codigoDeAtivacao = json.value("codigoDeAtivacao").toString();
    QString cnpj             = json.value("CNPJ").toString();
    QString cuf              = json.value("cUF").toString();

    QString comando = CMD_ATIVAR_SAT "|" + numeroSessao + "|" + subComando
            + "|" + codigoDeAtivacao + "|" + cnpj + "|" + cuf;

    qDebug() << comando;
    tamanho = complementarDeNove(comando);
    qDebug() << tamanho;
    comando = STRING_VER "|" + tamanho + "|" + comando;
    qDebug() << comando;

    return comando;
}

QString ComandosSAT::comunicarCertificadoCPBR(const QMap<QString, QVariant> json)
{
    //complemento
    QString tamanho;

    //parametros
    QString numeroSessao     = json.value("numeroSessao").toString();
    QString codigoDeAtivacao = json.value("codigoDeAtivacao").toString();
    QString certificado      = json.value("certificado").toString();

    QString comando = CMD_CERT_CPBR "|" + numeroSessao + "|" + codigoDeAtivacao
            + "|" + certificado;


    tamanho = complementarDeNove(comando);
    qDebug() << tamanho;
    comando = STRING_VER "|" + tamanho + "|" + comando;

    return comando;
}

QString ComandosSAT::enviarDadosVenda(const QMap<QString, QVariant> json)
{
    //complemento
    QString tamanho;

    //parametros
    QString numeroSessao     = json.value("numeroSessao").toString();
    QString codigoDeAtivacao = json.value("codigoDeAtivacao").toString();
    QString dadosVenda       = json.value("dadosVenda").toString();

    QString comando = CMD_ENVIAR_DVENDA "|" + numeroSessao + "|" + codigoDeAtivacao
            + "|" + dadosVenda;


    tamanho = complementarDeNove(comando);
    comando = STRING_VER "|" + tamanho + "|" + comando;

    return comando;
}

QString ComandosSAT::associarAssinatura(const QMap<QString, QVariant> json)
{
    //complemento
    QString tamanho;

    //parametros
    QString numeroSessao     = json.value("numeroSessao").toString();
    QString codigoDeAtivacao = json.value("codigoDeAtivacao").toString();
    QString cnpjValue        = json.value("CNPJvalue").toString();
    QString assinaturaCNPJs  = json.value("assinaturaCNPJs").toString();

    QString comando = CMD_ASSOC_ASSIN "|" + numeroSessao + "|" + codigoDeAtivacao
            + "|" + cnpjValue + "|" + assinaturaCNPJs;

    qDebug() << comando;
    tamanho = complementarDeNove(comando);
    qDebug() << tamanho;
    comando = STRING_VER "|" + tamanho + "|" + comando;
    qDebug() << comando;

    return comando;
}


QString ComandosSAT::atualizarSoftwareSAT(const QMap<QString, QVariant> json)
{
    //complemento
    QString tamanho;

    //parametros
    QString numeroSessao     = json.value("numeroSessao").toString();
    QString codigoDeAtivacao = json.value("codigoDeAtivacao").toString();

    QString comando = CMD_ATUALIZA_SAT "|" + numeroSessao + "|" + codigoDeAtivacao;

    qDebug() << comando;
    tamanho = complementarDeNove(comando);
    qDebug() << tamanho;
    comando = STRING_VER "|" + tamanho + "|" + comando;
    qDebug() << comando;

    return comando;
}


QString ComandosSAT::extrairLogs(const QMap<QString, QVariant> json)
{
    //complemento
    QString tamanho;

    //parametros
    QString numeroSessao     = json.value("numeroSessao").toString();
    QString codigoDeAtivacao = json.value("codigoDeAtivacao").toString();

    QString comando = CMD_EXTRAIR_LOG "|" + numeroSessao+ "|" + codigoDeAtivacao;

    qDebug() << comando;
    tamanho = complementarDeNove(comando);
    qDebug() << tamanho;
    comando = STRING_VER "|" + tamanho + "|" + comando;
    qDebug() << comando;

    return comando;
}

QString ComandosSAT::bloquearSAT(const QMap<QString, QVariant> json){
    //complemento
    QString tamanho;

    //parametros
    QString numeroSessao     = json.value("numeroSessao").toString();
    QString codigoDeAtivacao = json.value("codigoDeAtivacao").toString();

    QString comando = CMD_BLOQUEA_SAT "|" + numeroSessao + "|" + codigoDeAtivacao;

    qDebug() << comando;
    tamanho = complementarDeNove(comando);
    qDebug() << tamanho;
    comando = STRING_VER "|" + tamanho + "|" + comando;
    qDebug() << comando;

    return comando;
}

QString ComandosSAT::desbloquearSAT(const QMap<QString, QVariant> json){
    //complemento
    QString tamanho;

    //parametros
    QString numeroSessao     = json.value("numeroSessao").toString();
    QString codigoDeAtivacao = json.value("codigoDeAtivacao").toString();

    QString comando = CMD_DESBLOQUEA_SAT "|" + numeroSessao + "|" + codigoDeAtivacao;

    qDebug() << comando;
    tamanho = complementarDeNove(comando);
    qDebug() << tamanho;
    comando = STRING_VER "|" + tamanho + "|" + comando;
    qDebug() << comando;

    return comando;
}

QString ComandosSAT::trocarCodigoDeAtivacao(const QMap<QString, QVariant> json){
    //complemento
    QString tamanho;

    //parametros
    QString numeroSessao     = json.value("numeroSessao").toString();
    QString codigoDeAtivacao = json.value("codigoDeAtivacao").toString();
    QString opcao            = json.value("opcao").toString();
    QString novoCodigo       = json.value("novoCodigo").toString();
    QString confNovoCodigo   = json.value("confNovoCodigo").toString();

    QString comando = CMD_TROCA_COD_ATIVA "|" + numeroSessao + "|" + codigoDeAtivacao
            + "|" + opcao + "|" + novoCodigo + "|" + confNovoCodigo;

    qDebug() << comando;
    tamanho = complementarDeNove(comando);
    qDebug() << tamanho;
    comando = STRING_VER "|" + tamanho + "|" + comando;
    qDebug() << comando;

    return comando;
}

QString ComandosSAT::consultarStatusOperacional(const QMap<QString, QVariant> json)
{
    //complemento
    QString tamanho;

    //parametros
    QString numeroSessao     = json.value("numeroSessao").toString();
    QString codigoDeAtivacao = json.value("codigoDeAtivacao").toString();

    QString comando = CMD_CONSULT_STA_OP "|" + numeroSessao + "|" +codigoDeAtivacao;

    qDebug() << comando;
    tamanho = complementarDeNove(comando);
    qDebug() << tamanho;
    comando = STRING_VER "|" + tamanho + "|" + comando;
    qDebug() << comando;

    return comando;
}

QString ComandosSAT::consultarNumeroSessao(const QMap<QString, QVariant> json)
{
    //complemento
    QString tamanho;

    //parametros
    QString numeroSessao     = json.value("numeroSessao").toString();
    QString codigoDeAtivacao = json.value("codigoDeAtivacao").toString();
    QString cNumeroDeSessao  = json.value("cNumeroDeSessao").toString();

    QString comando = CMD_CONSULT_NUMSE "|" + numeroSessao + "|" + codigoDeAtivacao
                      + "|" + cNumeroDeSessao;

    qDebug() << comando;
    tamanho = complementarDeNove(comando);
    qDebug() << tamanho;
    comando = STRING_VER "|" + tamanho + "|" + comando;
    qDebug() << comando;

    return comando;
}

QString ComandosSAT::configurarInterfaceDeRede (const QMap<QString, QVariant> json)
{
    //complemento
    QString tamanho;

    //parametros
    QString numeroSessao     = json.value("numeroSessao").toString();
    QString codigoDeAtivacao = json.value("codigoDeAtivacao").toString();
    QString dadosConfiguracao  = json.value("cNumeroDeSessao").toString();

    QString comando = CMD_CONFIG_INTREDE "|" + numeroSessao + "|" + codigoDeAtivacao
                      + "|" + dadosConfiguracao;

    qDebug() << comando;
    tamanho = complementarDeNove(comando);
    qDebug() << tamanho;
    comando = STRING_VER "|" + tamanho + "|" + comando;
    qDebug() << comando;

    return comando;
}

QString ComandosSAT::cancelarUltimaVenda(QMap<QString, QVariant> json){

    //Parametros
    QString numeroSessao;
    QString codigoDeAtivacao;
    QString chave;
    QString dadosCancelamento;

    //Complementos
    QString tamanho;
    QString comando;

    numeroSessao      = json.value("numeroSessao").toString();
    codigoDeAtivacao  = json.value("codigoDeAtivacao").toString();
    chave             = json.value("chave").toString();
    dadosCancelamento = json.value("dadosCancelamento").toString();

    comando  = CMD_CANCEL_ULTVENDA "|" + numeroSessao + "|" +
            codigoDeAtivacao + "|" + chave + "|" + dadosCancelamento;

    tamanho = complementarDeNove(comando);
    comando = STRING_VER "|" + tamanho + "|" + comando;

    return comando;

}

QString ComandosSAT::consultarSAT(QMap<QString, QVariant> json){
    //Parametros
    QString numeroSessao;

    //Complementos
    QString tamanho;
    QString comando;

    numeroSessao = json.value("numeroSessao").toString();

    comando = CMD_CONSULTAR_SAT "|" + numeroSessao;

    tamanho = complementarDeNove(comando);
    comando = STRING_VER "|" + tamanho + "|" + comando;

    return comando;

}


QString ComandosSAT::testeFimAFim(QMap<QString, QVariant> json)
{
    //Parametros
    QString numeroSessao;
    QString codigoDeAtivacao;
    QString dadosVenda;

    //Complementos
    QString comando;
    QString tamanho;

    numeroSessao     = json.value("numeroSessao").toString();
    codigoDeAtivacao = json.value("codigoDeAtivacao").toString();
    dadosVenda       = json.value("dadosVenda").toString();

    comando = CMD_TFIM_FIM "|" + numeroSessao + "|" +
            codigoDeAtivacao + "|" + dadosVenda;

    tamanho = complementarDeNove(comando);
    comando = STRING_VER "|" + tamanho + "|" + comando;

    return comando;
}



int ComandosSAT::complementarDeNoventa(const QString comando)
{
    return 90 - comando.length();
}


QString ComandosSAT::complementarDeNove(const QString comando)
{
    int tamanho = comando.length();
    QString com = QString::number(tamanho);
    QString res;
    int i = 0;

    while( i < com.length())
    {
       if(com.at(i).isNumber())
       {
           int val = 9 - com.at(i).digitValue();
           res += QString::number(val) ;
       }

       i++;
    }

    return res;
}
