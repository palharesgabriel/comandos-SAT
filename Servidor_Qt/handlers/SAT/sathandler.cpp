#include "sathandler.h"

SATHandler::SATHandler(HobrasoftHttpd::HttpConnection *parent) : HobrasoftHttpd::HttpRequestHandler(parent)
{
    qDebug() << "SATHANDLER";
    SAT_ONE_Info = SerialDeviceInfo("SAT1","0x0108","0x0b1b");
    SAT_TWO_Info = SerialDeviceInfo("SAT2","0x0109","0x0b1b");
    comandosSAT  = ComandosSAT();
    smanager = new SerialManager(this);
}
//---------------------------------------------------------------------------------------------------------------
SATHandler::~SATHandler(){}
void teste(QList<QString> lista)
{
    qDebug() << "funcao teste";

    if(lista.size() > 0)
    {
        qDebug() << lista;
    }
}
//---------------------------------------------------------------------------------------------------------------
void SATHandler::service(HobrasoftHttpd::HttpRequest *request, HobrasoftHttpd::HttpResponse *response)
{
    QString path = request->path();
    QString metodo = request->method();
    QString conteudo = request->body();

    if(path.endsWith("/descobrir"))
    {
        SerialDiscoveryAgent *ag = new SerialDiscoveryAgent(this);
        ag->SerialDeviceDiscovery(SAT_ONE_Info, "comando1", ComandosSAT::tratarDescoberta);
        ag->SerialDeviceDiscovery(SAT_TWO_Info, "comando2", ComandosSAT::tratarDescoberta);

        sendResponse(response, "OK");
    }

    else if(path.endsWith("/ativarSAT"))
    {
        QStringList campos;
        campos << "numeroSessao" << "subComando" << "codigoDeAtivacao"
               << "CNPJ"         << "cUF";

        int retorno = 0;
        QString resposta = "";

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            resposta = comandosSAT.ativarSAT(jsonMap);
            resposta = smanager->sendToSerialPort("COM6", resposta);
        }


        sendResponse(response, resposta.toUtf8());
    }

    else if(path.endsWith("/certificadoicpbr"))
    {
        QStringList campos;
        campos << "numeroSessao" << "codigoDeAtivacao" << "certificado";

        int retorno = 0;
        QString resposta = "";

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            resposta = comandosSAT.comunicarCertificadoCPBR(jsonMap);
            resposta = smanager->sendToSerialPort("COM6", resposta);
        }


        sendResponse(response, resposta.toUtf8());
    }

    else if(path.endsWith("/dadosvenda"))
    {
        QStringList campos;
        campos << "numeroSessao" << "codigoDeAtivacao" << "dadosVenda";

        int retorno = 0;
        QString resposta = "";

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            resposta = comandosSAT.enviarDadosVenda(jsonMap);
            resposta = smanager->sendToSerialPort("COM6", resposta);
        }


        sendResponse(response, resposta.toUtf8());
    }

    else if(path.endsWith("/associarassinatura"))
        {
            QStringList campos;
            campos << "numeroSessao" << "codigoDeAtivacao" << "CNPJvalue" << "assinaturaCNPJs";

            int retorno = 0;
            QString resposta = "";

            if((retorno = getValuesFromJSON(conteudo, campos)))
            {
                resposta = comandosSAT.associarAssinatura(jsonMap);
                resposta = smanager->sendToSerialPort("COM6", resposta);
            }


            sendResponse(response, resposta.toUtf8());
        }

        else if(path.endsWith("/atualizarsat"))
        {
            QStringList campos;
            campos << "numeroSessao" << "codigoDeAtivacao";

            int retorno = 0;
            QString resposta = "";

            if((retorno = getValuesFromJSON(conteudo, campos)))
            {
                resposta = comandosSAT.atualizarSoftwareSAT(jsonMap);
                resposta = smanager->sendToSerialPort("COM6", resposta);
            }


            sendResponse(response, resposta.toUtf8());
        }

        else if(path.endsWith("/extrairlogs"))
        {
            QStringList campos;
            campos << "numeroSessao" << "codigoDeAtivacao";

            int retorno = 0;
            QString resposta = "";

            if((retorno = getValuesFromJSON(conteudo, campos)))
            {
                resposta = comandosSAT.extrairLogs(jsonMap);
                resposta = smanager->sendToSerialPort("COM6", resposta);
            }


            sendResponse(response, resposta.toUtf8());
        }

    else if(path.endsWith("/bloquearsat"))
        {
            QStringList campos;
            campos << "numeroSessao" << "codigoDeAtivacao";

            int retorno = 0;
            QString resposta = "";

            if((retorno = getValuesFromJSON(conteudo, campos)))
            {
                resposta = comandosSAT.bloquearSAT(jsonMap);
                resposta = smanager->sendToSerialPort("COM6", resposta);
            }


            sendResponse(response, resposta.toUtf8());
        }

        else if(path.endsWith("/desbloquearsat"))
        {
            QStringList campos;
            campos << "numeroSessao" << "codigoDeAtivacao";

            int retorno = 0;
            QString resposta = "";

            if((retorno = getValuesFromJSON(conteudo, campos)))
            {
                resposta = comandosSAT.desbloquearSAT(jsonMap);
                resposta = smanager->sendToSerialPort("COM6", resposta);
            }


            sendResponse(response, resposta.toUtf8());
        }

        else if(path.endsWith("/trocarcodativacao"))
        {
            QStringList campos;
            campos << "numeroSessao" << "codigoDeAtivacao" << "opcao"
                   << "novoCodigo"   << "confNovoCodigo";

            int retorno = 0;
            QString resposta = "";

            if((retorno = getValuesFromJSON(conteudo, campos)))
            {
                resposta = comandosSAT.trocarCodigoDeAtivacao(jsonMap);
                resposta = smanager->sendToSerialPort("COM6", resposta);
            }


            sendResponse(response, resposta.toUtf8());
        }

    else if(path.endsWith("/cancelarUltimaVenda"))
       {
           QStringList campos;
           campos << "numeroSessao" << "codigoDeAtivacao" << "chave" <<
                     "dadosCancelamento";

           int retorno = 0;
           QString resposta = "";
           if(retorno = getValuesFromJSON(conteudo, campos))
           {
               resposta = comandosSAT.cancelarUltimaVenda(jsonMap);
               resposta = smanager->sendToSerialPort("COM6", resposta);
           }
       }
       else if(path.endsWith("/testeFimAFim"))
       {
           QStringList campos;
           campos << "numeroSessao" << "codigoDeAtivacao" << "dadosVenda";

           int retorno = 0;
           QString resposta = "";
           if(retorno = getValuesFromJSON(conteudo, campos))
           {
               resposta = comandosSAT.testeFimAFim(jsonMap);
               resposta = smanager->sendToSerialPort("COM6", resposta);
           }
       }
       else if(path.endsWith("/consultarSAT"))
       {
           QStringList campos;
           campos << "numeroSessao";

           int retorno = 0;
           QString resposta = "";
           if(retorno = getValuesFromJSON(conteudo, campos))
           {
               resposta = comandosSAT.consultarSAT(jsonMap);
               resposta = smanager->sendToSerialPort("COM6", resposta);
           }
       }
       else if(path.endsWith("/trocarcodativacao"))
       {
           QStringList campos;
           campos << "numeroSessao" << "codigoDeAtivacao" << "opcao"
                  << "novoCodigo"   << "confNovoCodigo";

           int retorno = 0;
           QString resposta = "";

           if((retorno = getValuesFromJSON(conteudo, campos)))
           {
               resposta = comandosSAT.trocarCodigoDeAtivacao(jsonMap);
               resposta = smanager->sendToSerialPort("COM6", resposta);
           }


           sendResponse(response, resposta.toUtf8());
       }
   //=========================================================================
       else if(path.endsWith("/statusoperacional"))
       {
           QStringList campos;
           campos << "numeroSessao" << "codigoDeAtivacao";

           int retorno = 0;
           QString resposta = "";

           if((retorno = getValuesFromJSON(conteudo, campos)))
           {
               resposta = comandosSAT.ativarSAT(jsonMap);
               resposta = smanager->sendToSerialPort("COM6", resposta);
           }

           sendResponse(response, resposta.toUtf8());
       }
       //=========================================================================
       else if(path.endsWith("/consultarnumerosessao"))
       {
           QStringList campos;
           campos << "numeroSessao" << "codigoDeAtivacao" << "cNumeroDeSessao";

           int retorno = 0;
           QString resposta = "";

           if((retorno = getValuesFromJSON(conteudo, campos)))
           {
               resposta = comandosSAT.ativarSAT(jsonMap);
               resposta = smanager->sendToSerialPort("COM6", resposta);
           }

           sendResponse(response, resposta.toUtf8());
       }
       //=========================================================================
       else if(path.endsWith("/configurarinterfacederede"))
       {
           QStringList campos;
           campos << "numeroSessao" << "codigoDeAtivacao" << "dadosConfiguracao";

           int retorno = 0;
           QString resposta = "";

           if((retorno = getValuesFromJSON(conteudo, campos)))
           {
               resposta = comandosSAT.ativarSAT(jsonMap);
               resposta = smanager->sendToSerialPort("COM6", resposta);
           }

           sendResponse(response, resposta.toUtf8());
       }

    // Nenhum serviço
    else
    {
        sendResponse(response, "Serviço inexistente");
        return;
    }

}
//---------------------------------------------------------------------------------------------------------------
