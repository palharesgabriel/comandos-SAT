#ifndef DEFINES_H
#define DEFINES_H

#define STRING_VER          "B3m4"
#define CMD_ATIVAR_SAT      "AtivarSAT"
#define CMD_CERT_CPBR       "ComunicarCertificadoICPBRASIL"
#define CMD_ENVIAR_DVENDA   "EnviarDadosVenda"
#define CMD_CANCEL_ULTVENDA "CancelarUltimaVenda"
#define CMD_CONSULTAR_SAT   "ConsultarSAT"
#define CMD_TFIM_FIM        "TesteFimAFim"
#define CMD_CONSULT_STA_OP  "ConsultarStatusOperacional"
#define CMD_CONSULT_NUMSE   "ConsultarNumeroSessao"
#define CMD_CONFIG_INTREDE  "ConfigurarInterfaceDeRede"
#define CMD_ASSOC_ASSIN     "AssociarAssinatura"
#define CMD_ATUALIZA_SAT    "AtualizarSoftwareSAT"
#define CMD_EXTRAIR_LOG     "ExtrairLogs"
#define CMD_BLOQUEA_SAT     "BloquearSAT"
#define CMD_DESBLOQUEA_SAT  "DesbloquearSAT"
#define CMD_TROCA_COD_ATIVA "TrocarCodigoDeAtivacao"

#endif // DEFINES_H
