#ifndef COMANDOSSAT_H
#define COMANDOSSAT_H

#include <QString>
#include <QVector>
#include <QDebug>
#include <QStringList>
#include <QObject>

//Serial
#include <QSerialPort>
#include <QSerialPortInfo>

#include "dbmanager.h"
#include "serialmanager.h"
#include "defines.h"

class ComandosSAT
{
public:
    ComandosSAT();
    static void tratarDescoberta(const QString& resposta);
    QString ativarSAT(const QMap<QString, QVariant> json);
    QString comunicarCertificadoCPBR(const QMap<QString, QVariant> json);
    QString enviarDadosVenda(const QMap<QString, QVariant> json);
    QString cancelarUltimaVenda(QMap<QString, QVariant> json);
    QString consultarSAT(QMap<QString, QVariant> json);
    QString testeFimAFim(QMap<QString, QVariant> json);
    QString bloquearSAT(const QMap<QString, QVariant> json);
    QString desbloquearSAT(const QMap<QString, QVariant> json);
    QString trocarCodigoDeAtivacao(const QMap<QString, QVariant> json);
    QString consultarStatusOperacional(const QMap<QString, QVariant> json);
    QString consultarNumeroSessao(const QMap<QString, QVariant> json);
    QString configurarInterfaceDeRede(const QMap<QString, QVariant> json);
    QString associarAssinatura(const QMap<QString, QVariant> json);
    QString atualizarSoftwareSAT(const QMap<QString, QVariant> json);
    QString extrairLogs(const QMap<QString, QVariant> json);

    int complementarDeNoventa(const QString comando);
    QString complementarDeNove(const QString comando);
};

#endif // COMANDOSSAT_H
