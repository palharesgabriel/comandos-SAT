#ifndef SATHANDLER_H
#define SATHANDLER_H

#include <QObject>
#include <QDebug>
#include <QVector>
#include <QVariant>

#include "httpserver/httprequesthandler.h"
#include "httpserver/httprequest.h"
#include "httpserver/httpresponse.h"
#include "httpserver/httpconnection.h"
#include "httpserver/staticfilecontroller.h"

// ??
#include "dbmanager.h"
#include "discovery/serialdiscoveryagent.h"
#include "devices/serialdeviceinfo.h"

// Generic handler functions
#include "handler_templates/generichandler.h"
#include "comandossat.h"
#include "serialmanager.h"

class SATHandler : public HobrasoftHttpd::HttpRequestHandler, public GenericHandler {
    Q_OBJECT
  public:

   ~SATHandler();

    /**
     * @brief Construktor
     */
    SATHandler(HobrasoftHttpd::HttpConnection *parent);

    /**
     * @brief Processes the request
     *
     * Own specialized classes are created and called for dynamic content.
     * General request handler is created for other static content requests.
     */
    void service(HobrasoftHttpd::HttpRequest *request, HobrasoftHttpd::HttpResponse *response);

  private:
    SerialDeviceInfo SAT_ONE_Info;
    SerialDeviceInfo SAT_TWO_Info;
    ComandosSAT comandosSAT;
    SerialManager *smanager;
};

#endif // SATHANDLER_H
