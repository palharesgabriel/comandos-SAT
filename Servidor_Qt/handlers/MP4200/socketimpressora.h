#ifndef SOCKETIMPRESSORA_H
#define SOCKETIMPRESSORA_H

#include <QString>
#include <QObject>
#include <QByteArray>
#include <QByteArrayList>
#include <QTcpSocket>

class SocketImpressora : public QObject
{
    Q_OBJECT
public:
    SocketImpressora(QObject *parent = 0);

    void envioParticionado(QByteArrayList, bool);
private:
    QTcpSocket *socket;
public slots:
    bool conexaoHost(QString, int);
    bool enviarDados(QByteArray);
    void encerrarConexao();
    QByteArray lerDados();
};

#endif // SOCKETIMPRESSORA_H
