#ifndef BITMAP_H
#define BITMAP_H

#include <QImage>
#include <QString>
#include <QMatrix>
#include <QBitArray>
#include <QByteArray>
#include <QByteArrayList>
#include <QDebug>
#include <QRgb>
#include <bitset>

#include "defines.h"

class Bitmap
{
public:
    Bitmap();
    QByteArrayList gerarCodigos(QString, int, int, int, int*);
    QByteArrayList gerarCodigosPDF(QImage, int, int, int, int*);
    QByteArrayList gerarCodigosEspecial(QString, int, int, int, int, int, int*);
    QByteArrayList gravarBitmapVolatil(QString, int, int, int*);
    QByteArrayList gravarBitmapNaoVolatil(bool, QString, int, int, int*);

private:
    QImage _imagem;
    QBitArray _bitsImagem;
    int _bufferW;
    int _bufferH;

    int Base64ParaQImage(QString);
    void paraBitArray(int dithering);
    int paraGrayScale(void);
    QByteArrayList imprimirImagem(void);
    QByteArrayList gravacaoMemoria(void);

    int aplicarDitheringBayer(int, int, int);
    void aplicarDitheringfloydSteinberg(void);
};

#endif // BITMAP_H
