#ifndef COMANDOSIMPRESSORA_H
#define COMANDOSIMPRESSORA_H

#include <QStringList>
#include <QString>
#include <QUdpSocket>
#include <QDebug>
#include <QObject>
#include <QDataStream>
#include <QVector>
#include <QMap>
#include <QBitArray>
#include <QByteArray>
#include <QByteArrayList>
#include <QEventLoop>
#include <QTimer>
#include <QSignalMapper>
#include <QVariant>
#include <QCoreApplication>
#include <QProcess>

#include "socketimpressora.h"
#include "defines.h"
#include "bitmap.h"

//PDF
#include "pdfmanager.h"

#if defined(Q_OS_ANDROID)
#include <QAndroidJniObject>
#include <QtAndroid>
#include <jni.h>
#endif

class SocketImpressora;

class ComandosImpressora : public QObject
{
    Q_OBJECT
public:
    explicit ComandosImpressora(QObject *parent = 0);

    // Operacionais
    bool conectar(QString, int);
    void desconectar();
    void delay(int ms);

    // Comandos definidos em DLL
    int ajustaLarguraPapel(const QMap<QString,QVariant>);
    bool acionarGuilhotina(const QMap<QString,QVariant>);
    int imprimir(const QMap<QString,QVariant>);
    int imprimir(const QString);
    int imprimirFormatado(const QMap<QString,QVariant>);
    int leStatus(void);
    QByteArray leStatusEstendido(void);
    bool limparBuffer(void);
    int printerReset(void);
    int selecionaQualidade(const QMap<QString,QVariant>);
    int verificaPapel(void);
    int configuraExtratoLongo(const QMap<QString,QVariant>);

    // Bitmap
    int imprimirBitmap(QMap<QString,QVariant>);
    int imprimirBitmapEspecial(QMap<QString,QVariant>);
    int gravarBitmapVolatil(QMap<QString,QVariant>);
    int imprimirBitmapVolatil(QMap<QString,QVariant>);
    int gravarBitmapNaoVolatil(QMap<QString,QVariant>);
    int gravarBitmapNaoVolatil(QVector<QMap<QString,QVariant>>);
    int imprimirBitmapNaoVolatil(QMap<QString,QVariant>);

    // Codigos de barra
    int configuraCodigoBarras(const QMap<QString,QVariant>);
    int imprimeCodigoBarrasCODABAR(const QMap<QString,QVariant>);
    int imprimeCodigoBarrasCODE128(const QMap<QString,QVariant>);
    int imprimeCodigoBarrasCODE39(const QMap<QString,QVariant>);
    int imprimeCodigoBarrasCODE93(const QMap<QString,QVariant>);
    int imprimeCodigoBarrasEAN13(const QMap<QString,QVariant>);
    int imprimeCodigoBarrasEAN8(const QMap<QString,QVariant>);
    int imprimeCodigoBarrasISBN(const QMap<QString,QVariant>);
    int imprimeCodigoBarrasITF(const QMap<QString,QVariant>);
    int imprimeCodigoBarrasMSI(const QMap<QString,QVariant>);
    int imprimeCodigoBarrasPDF417(const QMap<QString,QVariant>);
    int imprimeCodigoBarrasPLESSEY(const QMap<QString,QVariant>);
    int imprimeCodigoBarrasUPCA(const QMap<QString,QVariant>);
    int imprimeCodigoBarrasUPCE(const QMap<QString,QVariant>);
    int imprimeCaracterGrafico(void);

    // PDF
    int imprimirPDF(QMap<QString, QVariant>);

    // Auxiliares
    QByteArray mudarTipoLetra(int);

private:
    SocketImpressora socket;
    QUdpSocket *udpSocketSend;
    QUdpSocket *udpSocketGet;

    QBitArray bitArrayFromByteArray(const QByteArray);
};

#endif // COMANDOSIMPRESSORA_H
