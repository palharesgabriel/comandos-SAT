#include "bitmap.h"

#define NODITHERING 0
#define BAYER 1
#define FLOYD 2
#define DEFAULT_DITHER FLOYD

const int PATTERN_BAYER[16][16] = {
    {   0,192, 48,240, 12,204, 60,252,  3,195, 51,243, 15,207, 63,255 },
    { 128, 64,176,112,140, 76,188,124,131, 67,179,115,143, 79,191,127 },
    {  32,224, 16,208, 44,236, 28,220, 35,227, 19,211, 47,239, 31,223 },
    { 160, 96,144, 80,172,108,156, 92,163, 99,147, 83,175,111,159, 95 },
    {   8,200, 56,248,  4,196, 52,244, 11,203, 59,251,  7,199, 55,247 },
    { 136, 72,184,120,132, 68,180,116,139, 75,187,123,135, 71,183,119 },
    {  40,232, 24,216, 36,228, 20,212, 43,235, 27,219, 39,231, 23,215 },
    { 168,104,152, 88,164,100,148, 84,171,107,155, 91,167,103,151, 87 },
    {   2,194, 50,242, 14,206, 62,254,  1,193, 49,241, 13,205, 61,253 },
    { 130, 66,178,114,142, 78,190,126,129, 65,177,113,141, 77,189,125 },
    {  34,226, 18,210, 46,238, 30,222, 33,225, 17,209, 45,237, 29,221 },
    { 162, 98,146, 82,174,110,158, 94,161, 97,145, 81,173,109,157, 93 },
    {  10,202, 58,250,  6,198, 54,246,  9,201, 57,249,  5,197, 53,245 },
    { 138, 74,186,122,134, 70,182,118,137, 73,185,121,133, 69,181,117 },
    {  42,234, 26,218, 38,230, 22,214, 41,233, 25,217, 37,229, 21,213 },
    { 170,106,154, 90,166,102,150, 86,169,105,153, 89,165,101,149, 85 }
    };

Bitmap::Bitmap(){}
//---------------------------------------------------------------------------------------------------------------
int Bitmap::Base64ParaQImage(QString dadosBase64)
{
    QByteArray temp;
    QString base64;
    QStringList base64HeaderFilter = {"data:image/png;base64,","data:image/bmp;base64,",
                                "data:image/jpg;base64,","data:image/jpeg;base64,","data:image/gif;base64,"};

    if(dadosBase64 == "")
    {
        return 0;
    }

    foreach(QString s, base64HeaderFilter)
    {
        if(dadosBase64.startsWith(s))
        {
            dadosBase64 = dadosBase64.right(dadosBase64.length() - s.length());
            break;
        }
    }

    temp.append(dadosBase64);

    qDebug() << "Tentando carregar imagem";

    if(_imagem.loadFromData(QByteArray::fromBase64(temp)))
    {
        qDebug() << "não carreguei imagem ):";
        return 1;
    }

    return 0;
}
//---------------------------------------------------------------------------------------------------------------
QByteArrayList Bitmap::gerarCodigos(QString dadosBase64, int tipoImpressao, int dithering, int pb, int* codigoErro)
{
    if(!Base64ParaQImage(dadosBase64))
    {
        int erro = -3;
        *codigoErro = erro;
        return {};
    }

    qDebug() << "Carreguei imagem";

    if(tipoImpressao == 1)
    {
        QMatrix m;
        m.rotate(-90);

        _imagem = _imagem.transformed(m);
        _bufferW = _imagem.width();
        _bufferH = _imagem.height();
    }

    _bufferW = _imagem.width();
    _bufferH = _imagem.height();

    if(pb == 0)
    {
        paraGrayScale();
    }

    if(dithering == FLOYD)
    {
        aplicarDitheringfloydSteinberg();
    }

    _imagem.save("teste.bmp","BMP");

    paraBitArray(dithering);

    QByteArrayList comandos = imprimirImagem();

    if(!comandos.empty()){
        *codigoErro = 1;
    }

    return comandos;
}
//---------------------------------------------------------------------------------------------------------------
QByteArrayList Bitmap::gerarCodigosPDF(QImage pdfImage, int tipoImpressao, int dithering, int pb, int* codigoErro)
{
    _imagem = pdfImage;

    qDebug() << "Carreguei imagem";

    if(tipoImpressao == 1)
    {
        QMatrix m;
        m.rotate(-90);

        _imagem = _imagem.transformed(m);
        _bufferW = _imagem.width();
        _bufferH = _imagem.height();
    }

    _bufferW = _imagem.width();
    _bufferH = _imagem.height();

    if(pb == 0)
    {
        paraGrayScale();
    }

    if(dithering == FLOYD)
    {
        aplicarDitheringfloydSteinberg();
    }

    _imagem.save("teste.bmp","BMP");

    paraBitArray(dithering);

    QByteArrayList comandos = imprimirImagem();

    if(!comandos.empty()){
        *codigoErro = 1;
    }

    return comandos;
}
//---------------------------------------------------------------------------------------------------------------
QByteArrayList Bitmap::gerarCodigosEspecial(QString dadosBase64, int escalaX, int escalaY, int rotacao, int dithering, int pb, int* codigoErro)
{
    if(!Base64ParaQImage(dadosBase64))
    {
        int erro = -3;
        *codigoErro = erro;
        return {};
    }

    qDebug() << "Carreguei imagem";

    QMatrix m;
    m.rotate(rotacao);

    if(dithering == FLOYD)
    {
        aplicarDitheringfloydSteinberg();
    }

    _imagem = _imagem.transformed(m);
    _bufferW = _imagem.width();
    _bufferH = _imagem.height();

    _imagem = _imagem.scaled( _imagem.width() * escalaX/100.0, _imagem.height() * escalaY/100.0, Qt::KeepAspectRatio);

    _bufferW = _imagem.width();
    _bufferH = _imagem.height();

    if(pb == 0)
    {
        paraGrayScale();
    }

    if(dithering == FLOYD)
    {
        aplicarDitheringfloydSteinberg();
    }

    _imagem.save("teste.bmp","BMP");

    paraBitArray(dithering);

    QByteArrayList comandos = imprimirImagem();

    if(!comandos.empty()){
        *codigoErro = 1;
    }

    return comandos;
}
//---------------------------------------------------------------------------------------------------------------
QByteArrayList Bitmap::gravarBitmapVolatil(QString dadosBase64, int dithering, int pb, int* codigoErro)
{
    if(!Base64ParaQImage(dadosBase64))
    {
        int erro = -3;
        *codigoErro = erro;
        return {};
    }

    if(pb == 0)
    {
        paraGrayScale();
    }

    QMatrix m;
    m.rotate(-90);

    if(dithering == FLOYD)
    {
        aplicarDitheringfloydSteinberg();
    }

    _imagem = _imagem.transformed(m);
    _imagem = _imagem.mirrored();

    _bufferW = _imagem.width();
    _bufferH = _imagem.height();

    paraBitArray(dithering);

    QByteArrayList comandos = gravacaoMemoria();

    QByteArray inicioGravar;

    inicioGravar.append(0x1D);
    inicioGravar.append(0X2A);
    inicioGravar.append((unsigned char)(int)(_bufferW/8.0 + 0.9));
    inicioGravar.append((unsigned char)(int)(_bufferH/8.0 + 0.9));

    comandos.prepend(inicioGravar);

    if(!comandos.empty())
    {
        *codigoErro = 1;
    }

    return comandos;
}
//---------------------------------------------------------------------------------------------------------------
QByteArrayList Bitmap::gravarBitmapNaoVolatil(bool unico, QString dadosBase64, int dithering, int pb, int* codigoErro)
{
    if( !Base64ParaQImage(dadosBase64) )
    {
        int erro = -3;
        *codigoErro = erro;
        return {};
    }

    if(pb == 0){
        paraGrayScale();
    }

    QMatrix m;
    m.rotate(-90);

    if(dithering == FLOYD)
    {
        aplicarDitheringfloydSteinberg();
    }

    _imagem = _imagem.transformed(m);
    _imagem = _imagem.mirrored();

    _bufferW = _imagem.width();
    _bufferH = _imagem.height();

    _imagem.save("teste_threshold.bmp","BMP");

    paraBitArray(dithering);

    QByteArrayList comandos = gravacaoMemoria();

    QByteArray inicioGravar;

    int xl, xh, yl, yh = 0;

    if(unico)
    {
        inicioGravar.append(0x1C);
        inicioGravar.append(0X71);
        inicioGravar.append((unsigned char) 1);
    }

    int tempW = _bufferW/8;
    int tempH = _bufferH/8;

    if( tempW >= 256){
        xh = (int)(_bufferW/256);
        xl = _bufferW%tempW;
    }else{
        xh = 0;
        xl = (int)(_bufferW/8.0 + 0.9);
    }

    if( tempH >= 256){
        yh = (int)(_bufferH/256);
        yl = _bufferH%tempH;
    }else{
        yh = 0;
        yl = (int)(_bufferH/8.0 + 0.9);
    }

    inicioGravar.append((unsigned char)xl);
    inicioGravar.append((unsigned char)xh);
    inicioGravar.append((unsigned char)yl);
    inicioGravar.append((unsigned char)yh);

    comandos.prepend(inicioGravar);

    if(!comandos.empty()){
        *codigoErro = 1;
    }

    return comandos;
}
//---------------------------------------------------------------------------------------------------------------
int Bitmap::paraGrayScale(void)
{
    for (int i = 0; i < _imagem.height(); i++)
    {
        uchar* scan = _imagem.scanLine(i);
        int depth = 4;

        for (int j = 0; j < _imagem.width(); j++)
        {
            QRgb* rgbpixel = reinterpret_cast<QRgb*>(scan + j*depth);

            if(qAlpha(*rgbpixel) == 0) // Se for 100% transparente, transforma em branco.
            {
                *rgbpixel = QColor(255, 255, 255).rgba();
            }else
            {
                int gray = qGray(*rgbpixel);
                *rgbpixel = QColor(gray, gray, gray).rgba();
            }
        }
    }

    return 1;
}
// ---------------------------------------------------------------------------
int Bitmap::aplicarDitheringBayer(int x, int y, int pixelValue)
{
    return ( pixelValue >= PATTERN_BAYER[x & 15][y & 15]) ? 0x00 : 0xFF;
}
// ---------------------------------------------------------------------------
// Helper do Floyd-Steinberg
// Soma dois números e força o valor mínimo para zero e o máximo para 255
uint8_t saturated_add(uint8_t val1, int8_t val2)
{
  int16_t val1_int = val1;
  int16_t val2_int = val2;
  int16_t tmp = val1_int + val2_int;

  if(tmp > 255)
  {
    return 255;
  }
  else if(tmp < 0)
  {
    return 0;
  }
  else
  {
    return tmp;
  }
}
// ---------------------------------------------------------------------------
void Bitmap::aplicarDitheringfloydSteinberg()
{
    int err;
    int c;
    uint8_t pixelLBelow,pixelBelow,pixelRBelow,pixelRight;
    int count = 0;
    QRgb* setPx;

    qDebug() << _imagem.width();
    qDebug() << _imagem.height();

    int depth = _imagem.format();

    for(int i=0; i<_imagem.height(); i++)
    {
        uchar *scan = _imagem.scanLine(i);
        uchar *next = _imagem.scanLine(i+1);

        for(int j=0; j<_imagem.width(); j++)
        {
            QRgb* currpx = reinterpret_cast<QRgb*>(scan + j*4);
            int currpxValue = (int)qGray(*currpx);

            if(currpxValue > 127)
            {
                err = currpxValue - 255;
                *currpx = qRgb(255,255,255);
            }
            else
            {
                err = currpxValue;
                *currpx = 0;
            }

            pixelLBelow = err * 3/16; //a
            pixelBelow  = err * 5/16; //b
            pixelRBelow = err * 1/16; //c
            pixelRight  = err * 7/16; //d

            if(i < _imagem.height()-1)
            {
                setPx = reinterpret_cast<QRgb*>(next + j*4);
                c = saturated_add((int)qGray(*setPx), pixelBelow);
                *setPx = qRgb(c,c,c);

                if(j <_imagem.width() -1)
                {
                    setPx = reinterpret_cast<QRgb*>(next + (j+1)*4);
                    c = saturated_add((int)qGray(*setPx), pixelRBelow);
                    *setPx = qRgb(c,c,c);
                }

                if(j > 0)
                {
                    setPx = reinterpret_cast<QRgb*>(next + (j-1)*4);
                    c = saturated_add((int)qGray(*setPx), pixelLBelow);
                    *setPx = qRgb(c,c,c);
                }
            }
            if(j < _imagem.width() - 1)
            {
                setPx = reinterpret_cast<QRgb*>(scan + (j+1)*4);
                c = saturated_add((int)qGray(*setPx), pixelRight);
                *setPx = qRgb(c,c,c);
            }

        }
    }

    qDebug() << count;
    _imagem.save("dither.bmp","BMP");
}
//---------------------------------------------------------------------------------------------------------------
void Bitmap::paraBitArray(int dithering)
{
    _bitsImagem.resize(_bufferW * _bufferH);
    bool cor = true;

    for (int i = 0; i < _imagem.height(); i++)
    {
        for(int j = 0; j < _imagem.width(); j++)
        {
            uchar c = (uchar)(_imagem.pixel(j,i));

            if (dithering == NODITHERING)
            {
                if(c > 127)
                {
                    cor = false;
                }else
                {
                    cor = true;
                }
            } else if(dithering == BAYER)
            {
                cor = aplicarDitheringBayer(j,i,c);
            }else
            {
                cor = 255 - c;
            }

            _bitsImagem[j+i*_bufferW] = cor;
        }
    }

}
// ---------------------------------------------------------------------------
QByteArrayList Bitmap::imprimirImagem(void)
{
    QByteArrayList dadosRetorno;

    QByteArray modoImagem;

    modoImagem.append(CMD_ESC);
    modoImagem.append(CMD_ASTERISCO);
    modoImagem.append(CMD_EXCLAMACAO);
    modoImagem.append((unsigned char)(_bufferW & 0xFF));
    modoImagem.append((unsigned char)((_bufferW >> 8) & 0xFF));

    QByteArray printFeed;

    printFeed.append(CMD_ESC);
    printFeed.append(0x4A);
    printFeed.append(0x30);

    QByteArray lineSpaceTrinta;

    lineSpaceTrinta.append(CMD_ESC);
    lineSpaceTrinta.append(0x33);
    lineSpaceTrinta.append(0x1e);

    QByteArray comando;

    // Inicializar impressora
    comando.append(CMD_ESC);
    comando.append(0x40);

    dadosRetorno.append(comando);
    comando.clear();

    // Espaco linha
    comando.append(CMD_ESC);
    comando.append(0x33);
    comando.append(0x18);

    dadosRetorno.append(comando);
    comando.clear();

    QByteArray imageDataLine;
    int offset = 0;

    while (offset < _bufferH)
    {
        dadosRetorno.append(modoImagem);

        for (int x = 0; x < _bufferW; ++x)
        {
            for (int k = 0; k < 3; ++k)
            {
                std::bitset<8> slice = 0;

                for (int b = 0; b < 8; ++b)
                {
                    int y = (((offset / 8) + k) * 8) + b;
                    int i = (y * _bufferW) + x;

                    bool v = false;

                    if (i < _bitsImagem.size())
                    {
                        v = _bitsImagem[i];
                    }

                    slice[7 - b] = ((v ? 1 : 0));
                }

                imageDataLine.append((unsigned char)slice.to_ullong());
            }
        }

        dadosRetorno.append(imageDataLine);
        imageDataLine.clear();

        offset += 24;
        dadosRetorno.append(printFeed);
    }//while

    dadosRetorno.append(lineSpaceTrinta);

    return dadosRetorno;
}
//---------------------------------------------------------------------------------------------------------------
QByteArrayList Bitmap::gravacaoMemoria(void)
{
    QByteArrayList dadosRetorno;

    QByteArray imageDataLine;

    for (int x = 0; x < _bufferW; x++)
    {
        for (int ySalto = 0; ySalto < _bufferH; ySalto += 8)
        {
            std::bitset<8> slice = 0;

            for (int b = 0; b < 8; b++)
            {
                int y = ySalto+b;
                int pos =  y + (x*_bufferH);

                bool v = false;
                if (pos < _bitsImagem.size())
                {
                    v = _bitsImagem[pos];
                }

                slice[7 - b] = ((v ? 1 : 0));
            }
            imageDataLine.append((unsigned char)slice.to_ullong());
        }

        dadosRetorno.append(imageDataLine);
        imageDataLine.clear();
    }

    return dadosRetorno;
}
