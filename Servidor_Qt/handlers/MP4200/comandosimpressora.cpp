#include "comandosimpressora.h"

ComandosImpressora::ComandosImpressora(QObject *parent) : QObject(parent), socket(parent){}
//-----------------------------------------------------------------------------------------------
bool ComandosImpressora::conectar(QString ip, int porta)
{
    return socket.conexaoHost(ip, porta);
}
//-----------------------------------------------------------------------------------------------
void ComandosImpressora::desconectar()
{
    socket.encerrarConexao();
}
//-----------------------------------------------------------------------------------------------
void ComandosImpressora::delay(int ms)
{
    QTimer t;
    t.start(ms);

    QEventLoop loop;
    connect(&t, SIGNAL(timeout()), &loop, SLOT(quit()));
    loop.exec();
}
//-----------------------------------------------------------------------------------------------
int ComandosImpressora::imprimir(const QMap<QString,QVariant> json)
{
    QString texto = json.value("texto").toString();

    return imprimir(texto);
}
//-----------------------------------------------------------------------------------------------
int ComandosImpressora::imprimir(QString txt)
{
    QVector<QString> textoSeparado;
    QString texto = txt;

    int maximoCaracteres = 100;
    QString quebraLinha = "\n\r";

    for (int n = 0; n < 100; n++) {
        textoSeparado.clear();
        for(int j=0; j<texto.length(); j+=maximoCaracteres){
            textoSeparado.append(texto.section("", j, j+maximoCaracteres));
        }
    }

    for(int j=0; j<textoSeparado.length(); j++)
    {
        if(!socket.enviarDados((textoSeparado.at(j)).toUtf8()))
        {
            return 0;
        }
    }

    if(socket.enviarDados(quebraLinha.toUtf8()))
    {
        return 1;
    }

    else
    {
        qDebug("Erro ao enviar dado para ComandosImpressora");
        return 0;
    }
}
//---------------------------------------------------------------------------------------------------------------
int ComandosImpressora::imprimirFormatado(const QMap<QString, QVariant> json)
{
    // Esse tipo da letra está confuso. Da DLL:
    // 1 = COMPRIMIDO, 2 = NORMAL, 3 = ELITE
    // ... seja lá o que isso significa
    QString texto = json.value("texto").toString();
    int letra = json.value("letra").toInt();
    int negrito = json.value("negrito").toInt();
    int italico = json.value("italico").toInt();
    int sublinhado = json.value("sublinhado").toInt();
    int expandido = json.value("expandido").toInt();

    QByteArrayList comando;
    QByteArray modificadorLetra;

    comando.append(mudarTipoLetra(letra));

    if(negrito)
    {
        modificadorLetra.append(CMD_ESC);
        modificadorLetra.append(CMD_E);

        comando.append(modificadorLetra);
        modificadorLetra.clear();
    }

    if(italico)
    {
        modificadorLetra.append(CMD_ESC);
        modificadorLetra.append(CMD_4);

        comando.append(modificadorLetra);
        modificadorLetra.clear();
    }

    if(sublinhado)
    {
        modificadorLetra.append(CMD_ESC);
        modificadorLetra.append(CMD_DASH);
        modificadorLetra.append(0x01);

        comando.append(modificadorLetra);
        modificadorLetra.clear();
    }

    if(expandido)
    {
        modificadorLetra.append(CMD_ESC);
        modificadorLetra.append(CMD_W);
        modificadorLetra.append(0x01);

        comando.append(modificadorLetra);
        modificadorLetra.clear();
    }

    socket.envioParticionado(comando, false);

    if(!imprimir(texto))
    {
        return false;
    }

    comando.clear();

    if(negrito)
    {
        modificadorLetra.append(CMD_ESC);
        modificadorLetra.append(CMD_F);

        comando.append(modificadorLetra);
        modificadorLetra.clear();
    }

    if(italico)
    {
        modificadorLetra.append(CMD_ESC);
        modificadorLetra.append(CMD_5);

        comando.append(modificadorLetra);
        modificadorLetra.clear();
    }

    if(sublinhado)
    {
        modificadorLetra.append(CMD_ESC);
        modificadorLetra.append(CMD_DASH);
        modificadorLetra.append((char)0x00);

        comando.append(modificadorLetra);
        modificadorLetra.clear();
    }

    if(expandido)
    {
        modificadorLetra.append(CMD_ESC);
        modificadorLetra.append(CMD_W);
        modificadorLetra.append((char)0x00);

        comando.append(modificadorLetra);
        modificadorLetra.clear();
    }

    comando.append(mudarTipoLetra(2));

    socket.envioParticionado(comando, false);

    return 1;
}
//---------------------------------------------------------------------------------------------------------------
int ComandosImpressora::leStatus(void)
{
    QByteArray comando;
    comando.append(CMD_ENQ);

    if(socket.enviarDados(comando))
    {
        QByteArray resposta =  socket.lerDados();
        qDebug() << resposta.toHex();

        QBitArray bits =  bitArrayFromByteArray(resposta);
        qDebug() << "array de bits:" << bits;


        if(bits.size()>0)
        {
             QByteArray testHex;
            //Pouco papel
            if( bits.at(BIT_POSITION_PAPER_NEAR_END) == 1 && bits.at(BIT_POSITION_PRINT_HEAD_DOWN) == 0 && bits.at(BIT_POSITION_PRINTER_ONLINE))
            {
                return 5;
            }
            //Sem papel
            else if( bits.at(BIT_POSITION_PAPER_NEAR_END) == 1 && bits.at(BIT_POSITION_DRAWER_PIN_HIGH) == 1 && bits.at(BIT_POSITION_PAPER_OUT) == 1)
            {
                return 32;
            }
            //Tampa aberta
            else if( bits.at(BIT_POSITION_PRINT_HEAD_DOWN) == 1)
            {
                return 9;
            }
            else if(bits.at(BIT_POSITION_PRINTER_ONLINE) == 1)
            {
                return 24;
            }
        }
        else
        {
            return 0;
        }
    }

    return 0;
}
//---------------------------------------------------------------------------------------------------------------
QByteArray ComandosImpressora::leStatusEstendido()
{
    QByteArray comando;
    comando.append(CMD_GS);
    comando.append(CMD_F8h);
    comando.append(CMD_1);

    if(socket.enviarDados(comando))
    {
       QByteArray resposta =  socket.lerDados();
       qDebug() << resposta;

       return resposta;
    }

    else
    {
        QByteArray resposta;
        return resposta.setNum(0);
    }
}
//---------------------------------------------------------------------------------------------------------------
QByteArray ComandosImpressora::mudarTipoLetra(int tipo)
{
    QByteArray comando;

    switch(tipo)
    {
        case 1: // comprimido
            comando.append(CMD_ESC);
            comando.append(CMD_SI);
        break;

        case 2: // normal
            comando.append(CMD_ESC);
            comando.append(CMD_H);
            comando.append(0x12);
        break;

        case 3: // famosa elite
            comando.append(CMD_ESC);
            comando.append(0x50);
        break;

        default:
            comando.append(CMD_ESC);
            comando.append(CMD_H);
        break;
    }

    return comando;
}
//---------------------------------------------------------------------------------------------------------------
bool ComandosImpressora::limparBuffer(void)
{
    QByteArray comando;
    comando.append(CMD_STX);
    return socket.enviarDados(comando);
}
//---------------------------------------------------------------------------------------------------------------
bool ComandosImpressora::acionarGuilhotina(const QMap<QString, QVariant> json)
{
    QByteArray comando;
    int tipo = json.value("tipo").toInt();

    qDebug() << "acionar";

    if(tipo == 1)
    {
        comando.append(CMD_ESC);
        comando.append(CMD_i);
    }
    else if(tipo == 0)
    {
        comando.append(CMD_ESC);
        comando.append(CMD_m);
    }
    else
    {
        return -2;
    }

    return socket.enviarDados(comando);
}
//---------------------------------------------------------------------------------------------------------------
QBitArray ComandosImpressora::bitArrayFromByteArray(const QByteArray byteArray)
{
    QBitArray bits(byteArray.count()*8);

    // Convert from QByteArray to QBitArray
    for(int i=0; i<byteArray.count(); ++i)
    {
        for(int b=0; b<8;b++)
        {
            bits.setBit( i*8+b, byteArray.at(i)&(1<<(7-b)));
        }
    }
    return bits;
}
//---------------------------------------------------------------------------------------------------------------
int ComandosImpressora::ajustaLarguraPapel(const QMap<QString,QVariant> json){
    QByteArray comando;
    comando.append(CMD_GS);
    comando.append(CMD_F9h);
    comando.append(CMD_EXCLAMACAO);

    int tamanho = json.value("tamanho").toInt();

    if((tamanho == 48) || (tamanho == 58) || (tamanho == 76) || (tamanho == 80) || (tamanho == 112))
    {
        comando.append((unsigned char)tamanho);
        return socket.enviarDados(comando);
    }

    else
    {
        return -4;
    }
}
//---------------------------------------------------------------------------------------------------------------
//TODO: Função incompleta (verificar manual da dll)
// Ainda tem que verificar o modelo da impressora.
int ComandosImpressora::selecionaQualidade(const QMap<QString,QVariant> json)
{
    QByteArray comando;
    comando.append(CMD_GS);
    comando.append(CMD_F9h);
    comando.append(CMD_DASH);

    int qualidade = json.value("qualidade").toInt();

    if(qualidade < 0 || qualidade > 4)
    {
        return -4;
    }

    comando.append(qualidade);

    return socket.enviarDados(comando);
}
//---------------------------------------------------------------------------------------------------------------
//Em teste
int ComandosImpressora::configuraExtratoLongo(const QMap<QString,QVariant> json)
{
    QByteArray comando;
    int linhas = json.value("linhas").toInt();

    if(linhas<1 || linhas>150)
    {
        return -2;
    }
    else
    {
        comando.append(CMD_ESC);
        comando.append(CMD_43);
        comando.append(linhas);
    }

    return socket.enviarDados(comando); //return 1?
}
//---------------------------------------------------------------------------------------------------------------
int ComandosImpressora::verificaPapel()
{
    QByteArray comando;
    QBitArray resultado(8);
    comando.append(CMD_ESC);
    comando.append(CMD_76);
    if(socket.enviarDados(comando)){
        QByteArray resposta =  socket.lerDados();
        resultado =  bitArrayFromByteArray(resposta);
        qDebug() << "teste verificar papel";
        if(resultado.testBit(2) && resultado.testBit(3))
        {
            //Papel presente
            return 1;
        }
        else if(!resultado.testBit(2) && !resultado.testBit(3))
        {
            //Papel não está presente
            return 2;
        }
        else if(!(resultado.testBit(2) && resultado.testBit(3)))
        {
            //Erro desconhecido
            return 3;
        }
        else if(resultado.testBit(4) || resultado.testBit(7))
        {
            //Erro de execução da função
            return -1;
        }
    }
    else
    {
        //Problemas de verificação do papel
        return 0;
    }

    return 0;
}
//---------------------------------------------------------------------------------------------------------------
int ComandosImpressora::printerReset()
{
    QByteArray comando;
    comando.append(CMD_ESC);
    comando.append(CMD_ARROBA);
    return socket.enviarDados(comando);
}
//---------------------------------------------------------------------------------------------------------------
int ComandosImpressora::imprimirBitmap(QMap<QString,QVariant> json)
{
    Bitmap bmp;

    int* codigoErro = (int*)malloc(sizeof(int));
    QString dados = json.value("imagem").toString();
    int tipo = json.value("tipo").toInt();
    int dithering = json.value("dithering").toInt();
    int pb = json.value("pb").toInt();

    QByteArrayList comandos = bmp.gerarCodigos(dados, tipo, dithering, pb, codigoErro);

    int retorno = *codigoErro;

    if(retorno == 1)
    {
        for(int i = 0; i < comandos.length(); i++)
        {
            socket.enviarDados(comandos[i]);
        }
    }

    free(codigoErro);
    return retorno;
}
//---------------------------------------------------------------------------------------------------------------
int ComandosImpressora::imprimirBitmapEspecial(QMap<QString,QVariant> json)
{
    Bitmap bmp;

    int* codigoErro = (int*)malloc(sizeof(int));
    QString dados = json.value("imagem").toString();
    int escalaX = json.value("escalaX").toInt();
    int escalaY = json.value("escalaY").toInt();
    int rotacao = json.value("rotacao").toInt();
    int dithering = json.value("dithering").toInt();
    int pb = json.value("pb").toInt();

    QByteArrayList comandos = bmp.gerarCodigosEspecial(dados, escalaX, escalaY, rotacao, dithering, pb, codigoErro);

    int retorno = *codigoErro;

    if(retorno == 1)
    {
        for(int i = 0; i < comandos.length(); i++)
        {
            socket.enviarDados(comandos[i]);
        }
    }

    free(codigoErro);
    return retorno;
}
//---------------------------------------------------------------------------------------------------------------
int ComandosImpressora::gravarBitmapVolatil(QMap<QString,QVariant> json)
{
    Bitmap bmp;

    int* codigoErro = (int*)malloc(sizeof(int));
    QString dados = json.value("imagem").toString();
    int dithering = json.value("dithering").toInt();
    int pb = json.value("pb").toInt();

    QByteArrayList comandos = bmp.gravarBitmapVolatil(dados, dithering, pb, codigoErro);

    int retorno = *codigoErro;

    if(retorno == 1)
    {
        for(int i = 0; i < comandos.length(); i++)
        {
            socket.enviarDados(comandos[i]);
        }
    }

    free(codigoErro);
    return retorno;
}
//---------------------------------------------------------------------------------------------------------------
int ComandosImpressora::imprimirBitmapVolatil(QMap<QString,QVariant> json)
{
    int modo = json.value("modo").toInt();
    if(modo >= 0 && modo <= 3 )
    {
        QByteArray comandos;

        comandos.append(0x1D);
        comandos.append(0x2F);
        comandos.append((unsigned char) modo);

        return socket.enviarDados(comandos);
    }else
    {
        return 0;
    }
}
//---------------------------------------------------------------------------------------------------------------
int ComandosImpressora::gravarBitmapNaoVolatil(QMap<QString,QVariant> json)
{
    Bitmap bmp;

    int* codigoErro = (int*)malloc(sizeof(int));
    QString dados = json.value("imagem").toString();
    int dithering = json.value("dithering").toInt();
    int pb = json.value("pb").toInt();

    QByteArrayList comandos = bmp.gravarBitmapNaoVolatil(true, dados, dithering, pb, codigoErro);

    int retorno = *codigoErro;

    if(retorno == 1)
    {
        for(int i = 0; i < comandos.length(); i++)
        {
            socket.enviarDados(comandos[i]);
        }
    }

    free(codigoErro);
    return retorno;
}

int ComandosImpressora::gravarBitmapNaoVolatil(QVector<QMap<QString,QVariant>> json)
{
    Bitmap bmp;

    int* codigoErro = (int*)malloc(sizeof(int));
    int retorno = 0;

    QByteArrayList comandos;
    QByteArray inicioGravar;

    inicioGravar.append(0x1C);
    inicioGravar.append(0X71);
    inicioGravar.append((unsigned char) json.length());

    comandos.append(inicioGravar);

    for(int i=0; i<json.length(); i++)
    {
        QString dados = json.at(i).value("imagem").toString();
        int dithering = json.at(i).value("dithering").toInt();
        int pb = json.at(i).value("pb").toInt();

        comandos.append(bmp.gravarBitmapNaoVolatil(false, dados, dithering, pb, codigoErro));

        retorno = *codigoErro;
    }

    if(retorno == 1)
    {
        for(int i = 0; i < comandos.length(); i++)
        {
            socket.enviarDados(comandos[i]);
        }
    }

    free(codigoErro);
    return retorno;
}

int ComandosImpressora::imprimirBitmapNaoVolatil(QMap<QString,QVariant> json)
{
    Bitmap bmp;

    int modo = json.value("modo").toInt();
    int indice = json.value("indice").toInt();

    if( (modo >= 0 && modo <= 3 )||(modo >= 48 && modo <= 51)){
        QByteArray comandos;

        comandos.append(0x1C);
        comandos.append(0x70);
        comandos.append((unsigned char) indice);
        comandos.append((unsigned char) modo);

        return socket.enviarDados(comandos);
    }else{
        return 0;
    }
}
//---------------------------------------------------------------------------------------------------------------
int ComandosImpressora::configuraCodigoBarras(const QMap<QString,QVariant> json)
{
    //Checagens
    int altura = json.value("altura").toInt();
    int largura = json.value("largura").toInt();
    int posicaoCaracteres = json.value("posicaocaracteres").toInt();
    int fonte = json.value("fonte").toInt();
    int margem = json.value("margem").toInt();

    if (altura < 1 || altura > 255)
    {
        qDebug() << "Erro no valor da altura";
        return -2;
    }

    if (largura < 0 || largura > 2)
    {
        qDebug() << "Erro no valor da largura";
        return -2;
    }

    if (posicaoCaracteres < 0 || posicaoCaracteres > 3)
    {
        qDebug() << "Erro no valor da posição dos caracteres";
        return -2;
    }

    if (fonte < 0 || fonte > 1)
    {
        qDebug() << "Erro no valor da fonte";
        return -2;
    }

    if (margem < 0 || margem > 575)
    {
        qDebug() << "Erro no valor da margem";
        return -2;
    }

    //Configuracoes prévias
    // largura da barra
    if (largura == 0)
    {
        largura = 2; //fina
    }
    else if(largura==1)
    {
        largura = 3; //normal
    }
    else
    {
        largura = 4; //grossa
    }

    QByteArray comando = QByteArray();

    //configurando altura - numero entre 1 e 255
    comando.append(CMD_GS);
    comando.append('h');
    comando.append(altura);
    //1D 68 n

    //configurando largura - numero 2 a 4
    comando.append(CMD_GS);
    comando.append('w');
    comando.append(largura);
    //1D 77 n

    //configurando caracteres posicao - numero de 0 a 3
    comando.append(CMD_GS);
    comando.append('H');
    comando.append(posicaoCaracteres);
    //1D 48 n

    //configurando fonte - numeros 0, 1, 48 ou 49
    comando.append(CMD_GS);
    comando.append('f');
    comando.append(fonte);

    comando.append(CMD_GS);
    comando.append('k');
    comando.append(0x84);
    comando.append(margem%256);// n1 - byte menos significativo
    comando.append(margem/256);// n2 - byte mais significativo

    return socket.enviarDados(comando);
}
//--------------------------------------------------------------------------------------------------------------
int ComandosImpressora::imprimeCodigoBarrasCODABAR(const QMap<QString,QVariant> json){
    //usei a segunda versão do código (GS k G n d1 ... dn)
    QByteArray comando;
    QString codigo = json.value("codigo").toString();

    //123-ABC/001
    QByteArray listCode = codigo.toLocal8Bit();

    //chacando o tamanho do codigo passado
    if (listCode.length() > 40 || listCode.length() < 1)
    {
        qDebug() << "Problemas com o tamanho do codigo passado";
        return -2;
    }

    //montando os caracteres permitidos no CODABAR
    char caracteresPermitidos[7];
    memset( caracteresPermitidos, '\0', 7 );
    strcpy( caracteresPermitidos, "$+-./:" );

    for(int i=0; i<listCode.length(); i++)
    {
        if(!isalnum(listCode[i]))
        {
            if ( strrchr(caracteresPermitidos, listCode[i]) == NULL)
            {
                qDebug() << "Problema com o caractere do codigo informado";
                return -2;
            }
        }
    }

    comando.append(CMD_GS);
    comando.append(0x6b);
    comando.append(0x47);
    comando.append(listCode.length());

    for (int i=0; i<listCode.length(); i++)
    {
        qDebug() << listCode[i] ;
        comando.append(listCode[i]);
    }

    return socket.enviarDados(comando);
}
//--------------------------------------------------------------------------------------------------------------
int ComandosImpressora::imprimeCodigoBarrasCODE128(const QMap<QString,QVariant> json)
{
    QString codigo = json.value("codigo").toString();
    QByteArray comando;
    QByteArray listCodes = codigo.toUtf8();

    if (listCodes.length() < 1 || listCodes.length() > 84)
    {
        qDebug() << "Problema com o tamanho do codigo informado - deve ser entre 1 e 84 digitos";
    }

    for (int i=0; i<listCodes.length(); i++)
    {
        if(!isascii(listCodes[i])){
            qDebug() << "Problemas com os valores do codigo informado - devem estar no formato ascii";
            return -2;
        }
    }

    comando.append(CMD_GS);
    comando.append(0x6b);
    comando.append(0x49);
    comando.append(listCodes.length());

    for (int i=0; i<listCodes.length(); i++)
    {
        qDebug() << listCodes[i];
        comando.append((int)listCodes[i]);
    }

    return socket.enviarDados(comando);
}
//--------------------------------------------------------------------------------------------------------------
int ComandosImpressora::imprimeCodigoBarrasCODE39(const QMap<QString,QVariant> json)
{
    QString codigo = json.value("codigo").toString();
    QByteArray comando;
    QByteArray listCodes = codigo.toUtf8();

    if (listCodes.length() < 1 || listCodes.length() > 30)
    {
        qDebug() << "Problema no tamanho do codigo informado";
    }

    //checagem de caracteres
    char caracteresPermitidos[8];
    memset( caracteresPermitidos, '\0', 8 );
    strcpy( caracteresPermitidos, " $%+-./" );

    for(int i=0; i<listCodes.length(); i++)
    {
        if(!isalnum(listCodes[i])){

            if ( strrchr( caracteresPermitidos, listCodes[i] ) == NULL )
            {
                qDebug() << "Problema com o caractere do codigo informado";
                return -2;
            }
        }
    }

    comando.append(CMD_GS);
    comando.append(0x6B);
    comando.append(0x45);
    comando.append(listCodes.length());

    for (int i=0; i<listCodes.size(); i++)
    {
        qDebug() << listCodes[i];
        comando.append((int)listCodes[i]);
    }

    return socket.enviarDados(comando);
}
//--------------------------------------------------------------------------------------------------------------
int ComandosImpressora::imprimeCodigoBarrasCODE93(const QMap<QString,QVariant> json)
{
    QString codigo = json.value("codigo").toString();
    QByteArray comando;
    QByteArray listCodes = codigo.toUtf8();

    if(listCodes.length() < 1 || listCodes.length() > 30)
    {
        qDebug() << "Problemas com o tamanho do codigo informado";
        return -2;
    }

    //checagem de caracteres
    char caracteresPermitidos[8];
    memset( caracteresPermitidos, '\0', 8 );
    strcpy( caracteresPermitidos, " $%+-./" );

    for(int i=0; i<listCodes.length(); i++)
    {
        if(!isalnum(listCodes[i])){

            if ( strrchr( caracteresPermitidos, listCodes[i] ) == NULL )
            {
                qDebug() << "Problema com o caractere do codigo informado";
                return -2;
            }
        }
    }

    //adicionando os comandos
    comando.append(CMD_GS);
    comando.append(0x6b);
    comando.append(0x48);
    comando.append(listCodes.length());

    for (int i=0; i<listCodes.size(); i++)
    {
        qDebug() << listCodes[i];
        comando.append((int)listCodes[i]);
    }

    return socket.enviarDados(comando);
}
//--------------------------------------------------------------------------------------------------------------
int ComandosImpressora::imprimeCodigoBarrasEAN13(const QMap<QString,QVariant> json)
{
    QString codigo = json.value("codigo").toString();
    QByteArray comando;
    QByteArray listCodes = codigo.toUtf8();

    if (listCodes.length() < 1 || listCodes.length() > 12)
    {
        qDebug() << "Problema com o numero de caracteres: deve ser maior que 0 e menor ou igual a 12";
        return -2;
    }

    for(int i=0; i<listCodes.length(); i++)
    {
        if(!isdigit(listCodes[i])){
            qDebug() << "Problema com algum caractere passado no código. Permitidos apenas numeros";
        }
    }

    comando.append(CMD_GS);
    comando.append(0x6b);
    comando.append(0x43);
    comando.append(0x0C);

    for (int i=0; i<listCodes.length(); i++)
    {
        qDebug() << listCodes[i];
        comando.append((int)listCodes[i]);
    }

    return socket.enviarDados(comando);
}
//--------------------------------------------------------------------------------------------------------------
int ComandosImpressora::imprimeCodigoBarrasEAN8(const QMap<QString,QVariant> json)
{
    QString codigo = json.value("codigo").toString();
    QByteArray comando;
    QByteArray listCodes = codigo.toUtf8();

    if(listCodes.length() < 1 || listCodes.length() > 7)
    {
        qDebug() << "Problemas com o tamanho do codigo informado 1-7";
        return -2;
    }

    for(int i=0; i<listCodes.length(); i++)
    {
        if(!isdigit(listCodes[i])){
            qDebug() << "Problemas com os digitos informados no codigo. Premitidos apenas números";
            return -2;
        }
    }

    comando.append(CMD_GS);
    comando.append(0x6b);
    comando.append(0x44);
    comando.append(0x07);

    for (int i=0; i<listCodes.length(); i++)
    {
        qDebug() << listCodes[i];
        comando.append((int)listCodes[i]);
    }

    return socket.enviarDados(comando);
}
//--------------------------------------------------------------------------------------------------------------
//TODO: Não está imprimindo quando há um número após o X do código de barras
int ComandosImpressora::imprimeCodigoBarrasISBN(const QMap<QString,QVariant> json)
{
    QString codigo = json.value("codigo").toString();
    qDebug() << codigo;
    QByteArray comando;
    QByteArray listCodes = codigo.toUtf8();

    if(listCodes.length() == 0 )
    {
        qDebug() << "Texto sem conteúdo";
        return -2;
    }

    // verifica se o codigo e numerico ou "-X "
    for(int i=0; i<listCodes.length(); i++)
    {
        // se nao for um caracter numerico?
        if(!isdigit(listCodes[i]))
        {
            // se nao for um caracter "-" ou "X"?
            if( listCodes[i] != '\x2d' && listCodes[i] != '\x58' && listCodes[i] != '\x20' )
            {
                qDebug("Problemas na formatação do código informado. Permitidos caracteres numéricos, espaco, traco e X");
                return -2;
            }
        }
    }

    comando.append(CMD_GS);
    comando.append(0x6b);
    comando.append(0x15);

    int i=0;
    for (i=0; i<listCodes.length(); i++)
    {
        comando.append(listCodes[i]);
    }

    char c = 0;
    comando.append(c);
    //comando.append('\x00');

    return socket.enviarDados(comando);
}
//--------------------------------------------------------------------------------------------------------------
int ComandosImpressora::imprimeCodigoBarrasITF(const QMap<QString,QVariant> json)
{
    QString codigo = json.value("codigo").toString();
    QByteArray comando;
    QByteArray listCodes = codigo.toUtf8();

    if(listCodes.length() < 1 || listCodes.length() > 60)
    {
        qDebug() << "Problemas com o tamanho do codigo informado 1-60";
        return -2;
    }

    for(int i=0; i<listCodes.length(); i++)
    {
        if(!isdigit(listCodes[i])){
            qDebug() << "Problemas na formatação do código informado. Permitidos apenas caracteres numéricos.";
            return -2;
        }
    }

    comando.append(CMD_GS);
    comando.append(0x6b);
    comando.append(0x46);
    comando.append(listCodes.length());

    for(int i=0; i<listCodes.length(); i++)
    {
        comando.append(listCodes[i]);
    }

    return socket.enviarDados(comando);
}
//--------------------------------------------------------------------------------------------------------------
int ComandosImpressora::imprimeCodigoBarrasMSI(const QMap<QString,QVariant> json)
{
    QString codigo = json.value("codigo").toString();
    QByteArray comando;
    QByteArray listCodes = codigo.toUtf8();

    if(listCodes.length() < 1 || listCodes.length() > 32)
    {
        qDebug() << "Problemas com o tamanho do codigo informado 1-32";
        return -2;
    }

    for(int i=0; i<listCodes.length(); i++)
    {
        if(!isdigit(listCodes[i])){
            qDebug() << "Problemas na formatação do código informado. Permitidos apenas caracteres numéricos.";
            return -2;
        }
    }

    comando.append(CMD_GS);
    comando.append(0x6b);
    comando.append(0x82);
    comando.append(listCodes.length());

    for(int i=0; i<listCodes.length(); i++){
        comando.append(listCodes[i]);
    }

    return socket.enviarDados(comando);
}
//--------------------------------------------------------------------------------------------------------------
int ComandosImpressora::imprimeCodigoBarrasPDF417(const QMap<QString,QVariant> json)
{
    QString codigo = json.value("codigo").toString();
    int correcaoErros = json.value("correcao").toInt();
    int altura = json.value("altura").toInt();
    int largura = json.value("largura").toInt();
    int colunas = json.value("colunas").toInt();

    QByteArray comando;
    QByteArray listCodes = codigo.toUtf8();

    if ( correcaoErros < 0 || correcaoErros > 8 )
    {
        qDebug() << "Problema no valor da correcao de erros 0-8";
        return -2;
    }

    if ( altura < 1 || altura > 8 )
    {
        qDebug() << "Problema no valor da altura 1-8";
        return -2;
    }

    if ( largura < 1 || largura > 4 )
    {
        qDebug() << "Problema no valor da largura 1-4";
        return -2;
    }

    if ( colunas < 0 || colunas > 30 )
    {
        qDebug() << "Problema no valor das colunas 0-30";
        return -2;
    }

    if (listCodes.length() < 1 || listCodes.length() > 1024)
    {
        qDebug() << "Problema no tamanho do codigo passado, 1-1024";
        return -2;
    }

    /*
    iQtdeBytes = (int) strlen( cCodigo );
    iLowByte = (int) iQtdeBytes % 256;
    iHighByte = (int) iQtdeBytes / 256;

    // GS k � - impressao do codigo de barras PDF417

    cComando[0] = GS;                   // GS
    cComando[1] = 'k';                   // k
    cComando[2] = '\x80';                   // 128d (80h)
    cComando[3] = iCorrecaoErros;           // (0-8)
    cComando[4] = iAltura;                  // (1-8)
    cComando[5] = iLargura;                 // (1-4)
    cComando[6] = iColunas;                 // (0-30)
    cComando[7] = iLowByte;                 // byte menos significativo
    cComando[8] = iHighByte;                // byte mais significativo


*/

    int quantidadeBytes = codigo.length();
    int lowByte = quantidadeBytes%256;
    int highByte = quantidadeBytes/256;

    comando.append(CMD_GS);
    comando.append(0x6b);
    comando.append(0x80);
    comando.append(correcaoErros);
    comando.append(altura);
    comando.append(largura);
    comando.append(colunas);
    comando.append(lowByte);
    comando.append(highByte);

    for(int i=0; i<listCodes.length(); i++)
    {
        comando.append(listCodes[i]);
    }

    comando.append('\0');
    return socket.enviarDados(comando);
}
//--------------------------------------------------------------------------------------------------------------
int ComandosImpressora::imprimeCodigoBarrasPLESSEY(const QMap<QString,QVariant> json)
{
    QString codigo = json.value("codigo").toString();
    QByteArray comando;
    QByteArray listCodes = codigo.toUtf8();

    if(listCodes.length() < 1 || listCodes.length() > 26)
    {
        qDebug() << "Problemas com o tamanho do codigo informado 1-32";
        return -2;
    }

    for(int i=0; i<listCodes.length(); i++)
    {
        if(!isalnum(listCodes[i])){
            qDebug() << "Problemas na formatação do código informado. Permitidos apenas caracteres numéricos e letras.";
            return -2;
        }
    }

    comando.append(CMD_GS);
    comando.append(0x6b);
    comando.append(0x83);
    comando.append(listCodes.length());

    for(int i=0; i<listCodes.length(); i++){
        comando.append(listCodes[i]);
    }

    return socket.enviarDados(comando);
}
//--------------------------------------------------------------------------------------------------------------
int ComandosImpressora::imprimeCodigoBarrasUPCA(const QMap<QString,QVariant> json)
{
    QString codigo = json.value("codigo").toString();
    QByteArray comando;
    QByteArray listCodes = codigo.toUtf8();

    if(listCodes.length() < 1 || listCodes.length() > 11)
    {
        qDebug() << "Problemas com o tamanho do codigo informado 1-32";
        return -2;
    }

    for(int i=0; i<listCodes.length(); i++)
    {
        if(!isdigit(listCodes[i])){
            qDebug() << "Problemas na formatação do código informado. Permitidos apenas caracteres numéricos e letras.";
            return -2;
        }
    }

    comando.append(CMD_GS);
    comando.append(0x6b);
    comando.append('\x00');

    for(int i=0; i<listCodes.length(); i++)
    {
        comando.append(listCodes[i]);
    }

    comando.append('\x00');

    return socket.enviarDados(comando);
}
//--------------------------------------------------------------------------------------------------------------
int ComandosImpressora::imprimeCodigoBarrasUPCE(const QMap<QString,QVariant> json)
{
    QString codigo = json.value("codigo").toString();
    QByteArray comando;
    QByteArray listCodes = codigo.toUtf8();

    if(listCodes.length() < 1 || listCodes.length() > 6)
    {
        qDebug() << "Problemas com o tamanho do codigo informado 1-32";
        return -2;
    }

    for(int i=0; i<listCodes.length(); i++)
    {
        if(!isdigit(listCodes[i])){
            qDebug() << "Problemas na formatação do código informado. Permitidos apenas caracteres numéricos e letras.";
            return -2;
        }
    }

    comando.append(CMD_GS);
    comando.append(0x6b);
    comando.append('\x01');

    for(int i=0; i<listCodes.length(); i++)
    {
        comando.append(listCodes[i]);
    }

    comando.append('\x00');

    return socket.enviarDados(comando);
}
//--------------------------------------------------------------------------------------------------------------
int ComandosImpressora::imprimeCaracterGrafico()
{
    QByteArray comando;

    //comandos para habilitar o modo grÃ¡fico com 9 pinos
//    comando.append(QChar(27));
//    comando.append(QChar(94));
//    comando.append(QChar(18));
//    comando.append(QChar(0));

//    //Exemplo
//    '              1 2 3 4 5 6 7 8 9
//    ' bit 7 = 128  *               *
//    ' bit 6 = 064  * *             *
//    ' bit 5 = 032  * * *           *
//    ' bit 4 = 016  * * * *         *
//    ' bit 3 = 008  * * * * *       *
//    ' bit 2 = 004  * * * * * *     *
//    ' bit 1 = 002  * * * * * * *   *
//    ' bit 0 = 001  * * * * * * * * *

    comando.append(QChar(255));
    comando.append(QChar(000));
    comando.append(QChar(000));
    comando.append(QChar(000));
    comando.append(QChar(127));
    comando.append(QChar(000));
    comando.append(QChar(000));
    comando.append(QChar(000));
    comando.append(QChar(063));
    comando.append(QChar(000));
    comando.append(QChar(000));
    comando.append(QChar(000));
    comando.append(QChar(031));
    comando.append(QChar(000));
    comando.append(QChar(000));
    comando.append(QChar(000));
    comando.append(QChar(015));
    comando.append(QChar(000));
    comando.append(QChar(000));
    comando.append(QChar(000));
    comando.append(QChar(007));
    comando.append(QChar(000));
    comando.append(QChar(000));
    comando.append(QChar(000));
    comando.append(QChar(003));
    comando.append(QChar(000));
    comando.append(QChar(000));
    comando.append(QChar(000));
    comando.append(QChar(001));
    comando.append(QChar(000));
    comando.append(QChar(000));
    comando.append(QChar(000));
    comando.append(QChar(255));
    comando.append(QChar(000));
    comando.append(QChar(000));
    comando.append(QChar(000));

    //descarregar buffer na impressora
    comando.append(QChar(13));
    comando.append(QChar(10));


    return socket.enviarDados(comando);
}
//--------------------------------------------------------------------------------------------------------------

int ComandosImpressora::imprimirPDF(const QMap<QString, QVariant> json)
{
    Bitmap bmp;
    QImage pdfImage;
    int* codigoErro = (int*)malloc(sizeof(int));
    int pb = json.value("pb").toInt();

    QString pdf = json.value("pdf").toString();
#if defined(Q_OS_ANDROID)
    QAndroidJniObject pdfBase64String = QAndroidJniObject::fromString(pdf);
    QAndroidJniObject::callStaticMethod<void>("org/qtproject/example/PdfToBmpParser", "decodePdfFromBase64", "(Ljava/lang/String;)V", pdfBase64String.object<jstring>());
    QAndroidJniObject::callStaticMethod<void>("org/qtproject/example/PdfToBmpParser","generateBmpFromPDF",
                                              "(Landroid/content/Context;)V", QtAndroid::androidContext().object());
    pdfImage = PdfManager::getPdfImageFromAndroid();    
#elif defined(Q_OS_WIN)
    QString programa = QCoreApplication::applicationDirPath() + "/util/ghostscript/windows/gswin32c.exe";
    QProcess processo;
    QString pdfDir = QCoreApplication::applicationDirPath() + "/util/ghostscript/temp.pdf";
    QString imagemGeradaDir = QCoreApplication::applicationDirPath() + "/util/ghostscript/temp.jpeg";
    QStringList argumentos;
    argumentos << "-dBATCH" << "-dNOPAUSE" << "-dSAFER" << "-sDEVICE=jpeg" << "-dJPEGQ=99" << "-r200" << "-sOutputFile=" + imagemGeradaDir << pdfDir;
    qDebug() << argumentos;
    processo.execute(programa, argumentos);
    processo.waitForFinished(-1);
    processo.close();
    pdfImage.load(imagemGeradaDir);
#endif

    //       QByteArray temp;
    //       temp.append(conteudoPDF);
    //       QByteArray pdfData=QByteArray::fromBase64(temp);
    //       QString pdfCaminho;
    //       pdfCaminho = "temp.pdf";
    //       #if defined(Q_OS_ANDROID)
    //        pdfCaminho = "/sdcard/temp.pdf";
    //       #endif
    //       QFile pdfArquivo(pdfCaminho);
    //       pdfArquivo.open(QIODevice::WriteOnly);
    //       pdfArquivo.write(pdfData);
    //       pdfArquivo.close();
    //       QProcess processo;
    //       QString programa;


    if(pdfImage.isNull())
    {
        qDebug() << "Problemas na geração do qimage a partir do pdf.";
        return -2;
    }

    QByteArrayList comandos = bmp.gerarCodigosPDF(pdfImage, 0, 0, pb, codigoErro);
    int retorno = *codigoErro;

    if(retorno == 1)
    {
        for(int i = 0; i < comandos.length(); i++)
        {
            socket.enviarDados(comandos[i]);
        }
    }

    free(codigoErro);
    return retorno;
}


//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------







