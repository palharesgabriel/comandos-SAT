#ifndef MP4200_H
#define MP4200_H

#include <QObject>
#include <QDebug>
#include "httpserver/httprequesthandler.h"
#include "httpserver/httprequest.h"
#include "httpserver/httpresponse.h"
#include "httpserver/httpconnection.h"
#include "httpserver/staticfilecontroller.h"
#include "jsonparser.h"
#include "logfile.h"

#if defined(Q_OS_ANDROID)
#include <QAndroidJniObject>
#include <QtAndroid>
#include <jni.h>
#endif

class MP4200Handler : public HobrasoftHttpd::HttpRequestHandler {
    Q_OBJECT
  public:

   ~MP4200Handler();

    /**
     * @brief Construktor
     */
    MP4200Handler(HobrasoftHttpd::HttpConnection *parent);

    /**
     * @brief Processes the request
     *
     * Own specialized classes are created and called for dynamic content.
     * General request handler is created for other static content requests.
     */
    void service(HobrasoftHttpd::HttpRequest *request, HobrasoftHttpd::HttpResponse *response);

    static QString responseString;

  private:
    static void writeResponse(HobrasoftHttpd::HttpResponse*);
};

#endif // MP4200HANDLER_H
