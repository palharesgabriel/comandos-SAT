#include "socketimpressora.h"

SocketImpressora::SocketImpressora(QObject *parent) : QObject(parent)
{
    socket = new QTcpSocket(this);
}
//--------------------------------------------------------------------------------
bool SocketImpressora::conexaoHost(QString host, int port)
{
    socket->connectToHost(host, port);
    return socket->waitForConnected();
}
//--------------------------------------------------------------------------------
bool SocketImpressora::enviarDados(QByteArray data)
{
    if(socket->state() == QAbstractSocket::ConnectedState)
    {
        socket->write(data);
        return socket->waitForBytesWritten();
    }

    else
    {
        return false;
    }

}
//--------------------------------------------------------------------------------
void SocketImpressora::envioParticionado(QByteArrayList content, bool quebralinha)
{
    QString quebra = "\r\n";

    for(int i=0; i<content.length(); i++)
    {
        socket->write(content[i]);
    }

    if(quebralinha == true)
    {
      socket->write(quebra.toUtf8());
    }
}
//--------------------------------------------------------------------------------
QByteArray SocketImpressora::lerDados()
{
    QByteArray resposta;
    socket->waitForReadyRead(3000);
    while (socket->bytesAvailable() > 0)
    {
        resposta.append(socket->readAll());
    }
    return resposta;
}
//--------------------------------------------------------------------------------
void SocketImpressora::encerrarConexao()
{
    socket->flush();
    socket->close();
}
