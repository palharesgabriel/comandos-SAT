#include "mp4200handler.h"

// Response string from Android
QString MP4200Handler::responseString = "";

MP4200Handler::MP4200Handler(HobrasoftHttpd::HttpConnection *parent) : HobrasoftHttpd::HttpRequestHandler(parent)
{
    qDebug() << "MP4200HANDLER";
    udpInfo = UDPBroadcastDeviceInfo("mp4200th",5001,1460,19,11,"MP4200FIND","MP4200FOUND");
    printerCommands = new ComandosImpressora(this);
}
//---------------------------------------------------------------------------------------------------------------
MP4200Handler::~MP4200Handler(){}
//-----------------------------------------------------------------------------------------------
#if defined(Q_OS_ANDROID)
void MP4200Handler::descobrimentoConcluido(JNIEnv *env, jobject /*obj*/, jstring n)
{
    QString qstr(env->GetStringUTFChars(n, 0));

    QStringList result = qstr.split("|*|", QString::SkipEmptyParts);

    if(qstr == "")
    {
        return;
    }

    if(result.length() == 0){
        QStringList singleDev = qstr.split("-");
        DbManager::addNetworkDevice(singleDev[0], singleDev[1], singleDev[0]);
        return;
    }

    foreach(QString device, result){
        QStringList fields = device.split("-");
        DbManager::addNetworkDevice(fields[0], fields[1], fields[2]);
    }
}
#endif
//---------------------------------------------------------------------------------------------------------------
QString MP4200Handler::conectarImpressora(QString json, ComandosImpressora* comandos, QVector<UDPBroadcastDeviceInfo::NetworkDevice> devices)
{
    if(devices.isEmpty())
    {
        return "Nenhuma impressora adicionada. Por favor, utilize o serviço de descobrimento. [Vazio]";
    }

    if(!JSONFieldExists("mac", json))
    {
        QVector<UDPBroadcastDeviceInfo::NetworkDevice> devices = DbManager::getNetworkDevices("mp4200th");

        if(!comandos->conectar(devices.first().IP, 9100))
        {
            return "Nenhuma impressora adicionada. Por favor, utilize o serviço de descobrimento. [Conexao]";
        }
    }
    else
    {
        QStringList campos;
        campos << "mac";

        QMap<QString, QString> jsonValues = JsonParser::parseBody(json, campos);
        UDPBroadcastDeviceInfo::NetworkDevice dev = DbManager::getNetworkDeviceByMac(jsonValues.value("mac"));

        if(!comandos->conectar(dev.IP, 9100))
        {
            return "A impressora passada pelo parametro mac não foi encontrada.";
        }
    }

    return "";
}
//-----------------------------------------------------------------------------------------------
void MP4200Handler::service(HobrasoftHttpd::HttpRequest *request, HobrasoftHttpd::HttpResponse *response)
{
    QString path = request->path();
    QString metodo = request->method();
    QString conteudo = request->body();
    QVector<UDPBroadcastDeviceInfo::NetworkDevice> devices = DbManager::getNetworkDevices("mp4200th");
    MP4200Handler::responseString = "";

    if(conteudo != "" && !JsonParser::bemFormado(conteudo))
    {
        sendResponse(response, "Json mal formado");
        return;
    }

    if(!path.endsWith("/descobrir"))
    {
        QString res = conectarImpressora(conteudo, printerCommands, devices);
        if(res != "")
        {
            sendResponse(response, res.toUtf8());
            return;
        }
    }

    if(path.endsWith("/descobrir"))
    {
        QStringList campos;
        campos << "mac";

        QMap<QString, QString> jsonValues = JsonParser::parseBody(conteudo, campos);
        UDPDiscoveryInfo mpinfo = udpInfo.getUDPBroadcastInfo(jsonValues.value("mac"));

        DbManager::removeNetworkDevices("mp4200th");

    #if defined(Q_OS_ANDROID)
        QString json = mpinfo.toJsonText();

        QAndroidJniObject::callStaticMethod<void>("org/qtproject/example/discovery/DiscoveryAgent",
                                                  "UDPBroadcastDeviceDiscovery",
                                                  "(Landroid/content/Context;Ljava/lang/String;I)V",
                                                  QtAndroid::androidContext().object(),
                                                  QAndroidJniObject::fromString(json).object<jstring>(),
                                                  discoveryDelay);
        printerCommands->delay((discoveryDelay+1)*1000);
    #else
        UDPDiscoveryAgent* ag = new UDPDiscoveryAgent(this);
        ag->UDPBroadcastDeviceDiscovery(mpinfo, discoveryDelay);
        delete ag;
    #endif
        QVector<UDPBroadcastDeviceInfo::NetworkDevice> devices = DbManager::getNetworkDevices("mp4200th");
        QVector<QMap<QString,QVariant>> valores;
        QMap<QString,QVariant> map;

        foreach(UDPBroadcastDeviceInfo::NetworkDevice dev, devices)
        {
            map.insert("id", "");
            map.insert("ip", dev.IP);
            map.insert("mac", dev.mac);
            map.insert("nome", dev.modelo);

            valores.append(map);
        }

        QString res = jsonDiscoveryArray(valores);
        sendResponse(response, res.toUtf8());
    }

    else if(path.endsWith("/listarimpressoras"))
    {
        QVector<QMap<QString,QVariant>> valores;
        QMap<QString,QVariant> map;

        foreach(UDPBroadcastDeviceInfo::NetworkDevice dev, devices)
        {
            map.insert("ip", dev.IP);
            map.insert("mac", dev.mac);
            map.insert("nome", dev.modelo);

            valores.append(map);
        }

        QString res = jsonDiscoveryArray(valores);
        sendResponse(response, res.toUtf8());
    }

    // ===================================================================
    // DLL COMMANDS ROUTES
    // ===================================================================

    else if(path.endsWith("/imprimir"))
    {
        QStringList campos;
        campos << "texto";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimir(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/cortar"))
    {
        QStringList campos;
        campos << "tipo";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->acionarGuilhotina(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/imprimirformatado"))
    {
        QStringList campos;
        campos << "texto" << "letra" << "negrito" << "italico" << "sublinhado" << "expandido";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimirFormatado(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/status"))
    {
        int res = printerCommands->leStatus();
        sendResponse(response, res);
    }

    else if(path.endsWith("/statusestendido"))
    {
        QByteArray res = printerCommands->leStatusEstendido();
        sendResponse(response, res);
    }

    else if(path.endsWith("/limparbuffer"))
    {
        int res = printerCommands->limparBuffer();
        sendResponse(response, res);
    }

    else if(path.endsWith("/reset"))
    {
        int res = printerCommands->printerReset();
        sendResponse(response, res);
    }

    else if(path.endsWith("/selecionaqualidade"))
    {
        QStringList campos;
        campos << "qualidade";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->selecionaQualidade(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/ajustalargura"))
    {
        QStringList campos;
        campos << "tamanho";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->ajustaLarguraPapel( jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/configuraextratolongo"))
    {
        QStringList campos;
        campos << "linhas";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->configuraExtratoLongo(jsonMap);
        }

        sendResponse(response, retorno);
    }
    else if(path.endsWith("/verificapapel"))
    {
        int res = printerCommands->verificaPapel();
        sendResponse(response, QString::number(res).toUtf8());
    }

    // ===================================================================
    // BITMAP ROUTES
    // ===================================================================

    else if(path.endsWith("/bitmap"))
    {
        QStringList campos;
        campos << "imagem" << "tipo" << "dithering" << "pb";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimirBitmap(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/bitmapespecial"))
    {
        QStringList campos;
        campos << "imagem" << "escalaX" << "escalaY" << "rotacao" << "dithering" << "pb";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimirBitmapEspecial(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/gravarbitmapvolatil"))
    {
        QStringList campos;
        campos << "imagem" << "dithering" << "pb";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->gravarBitmapVolatil(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/bitmapvolatil"))
    {
        QStringList campos;
        campos << "modo";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimirBitmapVolatil(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/gravar1bitmapnv"))
    {
        QStringList campos;
        campos << "imagem" << "dithering" << "pb";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->gravarBitmapNaoVolatil(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/gravarnbitmapnv"))
    {
        QStringList campos;
        campos << "imagem" << "dithering" << "pb";
        int retorno = 0;

        QVector<QMap<QString,QVariant>> bmpArray = getVectorFromJSONArray(conteudo, "bitmaps", campos);

        if(!bmpArray.isEmpty())
        {
            retorno = printerCommands->gravarBitmapNaoVolatil(bmpArray);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/bitmapnv"))
    {
        QStringList campos;
        campos << "indice" << "modo";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimirBitmapVolatil(jsonMap);
        }

        sendResponse(response, retorno);
    }

    // ===================================================================
    // BARCODE ROUTES
    // ===================================================================
    else if(path.endsWith("/configuracodigobarras"))
    {
        QStringList campos;
        campos << "altura" << "largura" << "posicaocaracteres" << "fonte" << "margem";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->configuraCodigoBarras(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/codigocodabar"))
    {
        QStringList campos;
        campos << "codigo";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimeCodigoBarrasCODABAR(jsonMap);
        }
        sendResponse(response, retorno);
    }

    else if(path.endsWith("/codigo128"))
    {
        QStringList campos;
        campos << "codigo";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimeCodigoBarrasCODE128(jsonMap);
        }
        sendResponse(response, retorno);
    }

    else if(path.endsWith("/codigo39"))
    {
        QStringList campos;
        campos << "codigo";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimeCodigoBarrasCODE39(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/codigo93"))
    {
        QStringList campos;
        campos << "codigo";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimeCodigoBarrasCODE93(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/codigoean13"))
    {
        QStringList campos;
        campos << "codigo";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimeCodigoBarrasEAN13(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/codigoean8"))
    {
        QStringList campos;
        campos << "codigo";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimeCodigoBarrasEAN8(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/codigoisbn"))
    {
        QStringList campos;
        campos << "codigo";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimeCodigoBarrasISBN(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/codigoitf"))
    {
        QStringList campos;
        campos << "codigo";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimeCodigoBarrasITF(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/codigopdf417"))
    {
        QStringList campos;
        campos << "codigo" << "correcao" << "altura" << "largura" << "colunas" ;
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimeCodigoBarrasPDF417(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/codigoplessey"))
    {
        QStringList campos;
        campos << "codigo";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimeCodigoBarrasPLESSEY(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/codigoupca"))
    {
        QStringList campos;
        campos << "codigo";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimeCodigoBarrasUPCA(jsonMap);
        }

        sendResponse(response, retorno);
    }

    else if(path.endsWith("/codigoupce"))
    {
        QStringList campos;
        campos << "codigo";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimeCodigoBarrasUPCE(jsonMap);
        }

        sendResponse(response, retorno);
    }

    //TODO: verificar na dll como está sendo impresso
    else if(path.endsWith("/caracter"))
    {
        int res = printerCommands->imprimeCaracterGrafico();
        sendResponse(response, res);
    }

    // ===================================================================
    // PDF ROUTES
    // ===================================================================

//#if defined(Q_OS_ANDROID)
    else if(path.endsWith("/pdf"))
    {
        QStringList campos;
        campos << "pdf" << "pb";
        int retorno = 0;

        if((retorno = getValuesFromJSON(conteudo, campos)))
        {
            retorno = printerCommands->imprimirPDF(jsonMap);
        }

        sendResponse(response, retorno);


//        QStringList campos;
//        campos << "pdf";
//        int res = printerCommands->imprimirPDF(getValuesFromJSON(conteudo, campos));
//        sendResponse(response, QString::number(res).toUtf8());
    }
//#endif

    // Nenhum serviço
    else
    {
        sendResponse(response, "Serviço inexistente");
        printerCommands->desconectar();
        return;
    }
}

