#ifndef MP4200_H
#define MP4200_H

#include <QObject>
#include <QDebug>
#include <QVector>
#include <QVariant>

#include "httpserver/httprequesthandler.h"
#include "httpserver/httprequest.h"
#include "httpserver/httpresponse.h"
#include "httpserver/httpconnection.h"
#include "httpserver/staticfilecontroller.h"

// ??
#include "dbmanager.h"
#include "discovery/udpdiscoveryagent.h"

// Generic handler functionss
#include "handler_templates/generichandler.h"
#include "devices/udpbroadcastdeviceinfo.h"

//PRINTER
#include "socketimpressora.h"
#include "comandosimpressora.h"

#if defined(Q_OS_ANDROID)
#include <QAndroidJniObject>
#include <QtAndroid>
#include <jni.h>
#endif

class MP4200Handler : public HobrasoftHttpd::HttpRequestHandler, public GenericHandler {
    Q_OBJECT
  public:

   ~MP4200Handler();

    /**
     * @brief Construktor
     */
    MP4200Handler(HobrasoftHttpd::HttpConnection *parent);

    /**
     * @brief Processes the request
     *
     * Own specialized classes are created and called for dynamic content.
     * General request handler is created for other static content requests.
     */
    void service(HobrasoftHttpd::HttpRequest *request, HobrasoftHttpd::HttpResponse *response);
    //static DiscoveryInfo retornarInformacoesDescobrimento(QString filter);

#if defined(Q_OS_ANDROID)
    static void descobrimentoConcluido(JNIEnv *env, jobject /*obj*/, jstring n);
#endif

    UDPBroadcastDeviceInfo udpInfo;

  private:
    static QString responseString;    
    ComandosImpressora* printerCommands;
    QString conectarImpressora(QString, ComandosImpressora*, QVector<UDPBroadcastDeviceInfo::NetworkDevice>);
};

#endif // MP4200HANDLER_H
