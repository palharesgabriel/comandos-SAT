
#include "fmhandler.h"
#if defined(Q_OS_ANDROID)

FMHandler::~FMHandler() {}
FMHandler::FMHandler(HobrasoftHttpd::HttpConnection *parent) : HobrasoftHttpd::HttpRequestHandler(parent) {}

QString FMHandler::responseString = "";
//-----------------------------------------------------------------------------------------------
void FMHandler::getCheckout(JNIEnv *env, jobject /*obj*/, jstring n)
{
    QString qstr(env->GetStringUTFChars(n, 0));
    FMHandler::responseString = qstr;
}
//-----------------------------------------------------------------------------------------------
void FMHandler::getLookup(JNIEnv *env, jobject /*obj*/, jstring n)
{
    QString qstr(env->GetStringUTFChars(n, 0));
    FMHandler::responseString = qstr;
}
//-----------------------------------------------------------------------------------------------
void FMHandler::getVersion(JNIEnv *env, jobject /*obj*/, jstring n)
{
    QString qstr(env->GetStringUTFChars(n, 0));
    FMHandler::responseString = qstr;
}
//-----------------------------------------------------------------------------------------------
void FMHandler::service(HobrasoftHttpd::HttpRequest *request, HobrasoftHttpd::HttpResponse *response)
{
    QString path = request->path();
    QString metodo = request->method();
    QString conteudo = request->body();

    FMHandler::responseString = "";

    QAndroidJniObject serviceInstante = QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative", "activity", "()Landroid/app/Activity;");   //service is valid

    if(!serviceInstante.isValid())
    {
        sendResponse(response, "Algum problema ocorreu com a aplicação");
        return;
    }

    LogFile::writeToLog("Requisição recebida");

    if(path.endsWith("/versao"))
    {
        LogFile::writeToLog("Serviço de Versão");

        QAndroidJniObject::callStaticMethod<void>("org/qtproject/example/Commands","getVersion",
                                                  "(Landroid/content/Context;)V", QtAndroid::androidContext().object());

        while(FMHandler::responseString == ""){}
        sendResponse(response, FMHandler::responseString.toUtf8());

        response->write(FMHandler::responseString.toUtf8());
        response->flush();

        LogFile::writeToLog("Versão enviada para o Cliente");
    }

    else if(path.endsWith("/invoicelookup"))
    {
        LogFile::writeToLog("Serviço de Lookup");

        QStringList campos;
        campos << "id";

        QMap<QString, QString> jsonValues = JsonParser::parseBody(conteudo, campos);
        QString id = jsonValues["id"];

        QAndroidJniObject::callStaticMethod<void>("org/qtproject/example/Commands","invoiceLookup",
                                                  "(Landroid/content/Context;Ljava/lang/String;)V", QtAndroid::androidContext().object(),
                                                  QAndroidJniObject::fromString(id).object<jstring>());

        while(FMHandler::responseString == ""){}
        sendResponse(response, FMHandler::responseString.toUtf8());

        LogFile::writeToLog("Lookup enviado para o Cliente");
    }

    else if(path.endsWith("/checkout"))
    {
        LogFile::writeToLog("Serviço de Checkout");

        QString json = conteudo;
        QStringList campos;
        campos << "serie_B07" << "nNF_B08";

        QMap<QString, QString> jsonValues = JsonParser::parseBody(conteudo, campos);
        QString serieModelo = jsonValues["serie_B07"] + "|" + jsonValues["nNF_B08"];

        QAndroidJniObject::callStaticMethod<void>("org/qtproject/example/Commands","checkoutOperation",
                                                  "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V",
                                                  QtAndroid::androidContext().object(), QAndroidJniObject::fromString(json).object<jstring>(),
                                                  QAndroidJniObject::fromString(serieModelo).object<jstring>());

        while(FMHandler::responseString == ""){}
        sendResponse(response, FMHandler::responseString.toUtf8());

        LogFile::writeToLog("Checkout enviado para o Cliente");
    }

    // Nenhum serviço
    else
    {
        sendResponse(response, "Serviço inexistente");
        return;
    }

    return;
}
#endif
