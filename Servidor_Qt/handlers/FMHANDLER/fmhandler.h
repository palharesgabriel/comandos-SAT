#ifndef FMHANDLER_H
#define FMHANDLER_H

#include <QObject>
#include <QDebug>
#include "httpserver/httprequesthandler.h"
#include "httpserver/httprequest.h"
#include "httpserver/httpresponse.h"
#include "httpserver/httpconnection.h"
#include "httpserver/staticfilecontroller.h"
#include "server_core/logfile.h"

// Handler
#include "handler_templates/generichandler.h"

#if defined(Q_OS_ANDROID)
#include <QAndroidJniObject>
#include <QtAndroid>
#include <jni.h>

class FMHandler : public HobrasoftHttpd::HttpRequestHandler, public GenericHandler
{
    Q_OBJECT
public:
   ~FMHandler();

    /**
     * @brief Construktor
     */
    FMHandler(HobrasoftHttpd::HttpConnection *parent);

    /**
     * @brief Processes the request
     *
     * Own specialized classes are created and called for dynamic content.
     * General request handler is created for other static content requests.
     */
    void service(HobrasoftHttpd::HttpRequest *request, HobrasoftHttpd::HttpResponse *response);
    static QString responseString;

    // Return functions that are mapped to Android on Server.cpp
    static void getVersion(JNIEnv *env, jobject /*obj*/, jstring n);
    static void getLookup(JNIEnv *env, jobject /*obj*/, jstring n);
    static void getCheckout(JNIEnv *env, jobject /*obj*/, jstring n);

private:
    static void writeResponse(HobrasoftHttpd::HttpResponse*);
};

#endif // ANDROID
#endif // FMHANDLER_H
