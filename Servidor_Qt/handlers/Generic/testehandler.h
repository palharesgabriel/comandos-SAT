#ifndef TESTEHANDLER_H
#define TESTEHANDLER_H

#include "httpserver/httprequesthandler.h"
#include "httpserver/httpconnection.h"
#include "httpserver/httprequest.h"
#include "httpserver/httpresponse.h"

#include "handler_templates/generichandler.h"

class TesteHandler : public HobrasoftHttpd::HttpRequestHandler, public GenericHandler
{
    Q_OBJECT
public:
    ~TesteHandler();

    TesteHandler(HobrasoftHttpd::HttpConnection *parent);
    void service(HobrasoftHttpd::HttpRequest *request, HobrasoftHttpd::HttpResponse *response);
};

#endif // TESTEHANDLER_H
