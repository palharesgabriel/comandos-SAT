#include "testehandler.h"

TesteHandler::TesteHandler(HobrasoftHttpd::HttpConnection *parent) : HobrasoftHttpd::HttpRequestHandler(parent){}
//---------------------------------------------------------------------------------------------------------------
TesteHandler::~TesteHandler(){}
//---------------------------------------------------------------------------------------------------------------
void TesteHandler::service(HobrasoftHttpd::HttpRequest *request, HobrasoftHttpd::HttpResponse *response)
{
    QString path = request->path();
    QString metodo = request->method();
    QString conteudo = request->body();

    if(path.endsWith("teste"))
    {
        sendResponse(response, "AMENO");
    }
}
