#ifndef JSONPARSER_H
#define JSONPARSER_H

#include <QString>
#include <QJsonDocument>
#include <QStringList>
#include <QJsonObject>
#include <QJsonArray>
#include <QMap>
#include <QDebug>
#include <QVector>
#include <QVariantMap>

class JsonParser
{
public:
    static QMap<QString, QString> parseBody(const QString, const QStringList);
    static bool parseBody(const QString conteudo, const QStringList valores, QMap<QString, QVariant>* vetor);
    static QVector<QMap<QString, QVariant>> parseArray(const QString, const QString, const QStringList);
    static bool bemFormado(const QString);
    static bool fieldExists(const QString, const QString);
    static QString jsonArrayResponse(QString arrayname, QVector<QMap<QString,QVariant>> valores);


private:
    JsonParser();
};

#endif // JSONPARSER_H
