#include "jsonparser.h"
#include <QString>

JsonParser::JsonParser(){}
//-----------------------------------------------------------------------------------------------
bool JsonParser::bemFormado(const QString json)
{
    QJsonDocument doc = QJsonDocument::fromJson(json.toUtf8());
    return !doc.isNull();
}
//-----------------------------------------------------------------------------------------------
QMap<QString, QString> JsonParser::parseBody(const QString conteudo, const QStringList valores)
{
    QJsonDocument doc = QJsonDocument::fromJson(conteudo.toUtf8());
    QJsonObject objectJason = doc.object();
    QMap<QString,QString> res;

    if(doc.isNull())
    {
        return res;
    }

    foreach(QString val, valores)
    {
        QJsonValue valor = objectJason.value(val);
        if(valor.isNull() || valor.isUndefined() || valor.toString() == "" )
        {
            qWarning() << "campo não encontrado";
        }

        res.insert(val, valor.toString().toUtf8());
    }

    return res;
}
//-----------------------------------------------------------------------------------------------
bool JsonParser::parseBody(const QString conteudo, const QStringList valores, QMap<QString, QVariant>* vetor)
{
    QJsonDocument doc = QJsonDocument::fromJson(conteudo.toUtf8());
    QJsonObject objectJason = doc.object();

    if(doc.isNull())
    {
        return false;
    }

    foreach(QString val, valores)
    {
        QJsonValue valor = objectJason.value(val);

        if(valor.isNull() || valor.isUndefined() || (valor.isString() && valor.toString() == "") || (!valor.isString() && !valor.isDouble()))
        {
            qDebug() << "!";
            return false;
        }

        vetor->insert(val, valor.toVariant());
    }

    return true;
}
//-----------------------------------------------------------------------------------------------
QVector<QMap<QString, QVariant>> JsonParser::parseArray(const QString conteudo, const QString nome, const QStringList fields)
{
    QJsonDocument doc = QJsonDocument::fromJson(conteudo.toUtf8());
    QJsonObject objectJson = doc.object();
    QVector<QMap<QString,QVariant>> res;

    if(doc.isNull())
    {
        res.clear();
        return res;
    }

    QJsonValue valor = objectJson.value(nome);

    if(valor.isNull() || valor.isUndefined() || !valor.isArray())
    {
        res.clear();
        return res;
    }

    QJsonArray bitmap = valor.toArray();

    foreach(QJsonValue obj, bitmap)
    {
        if(obj.toObject().isEmpty())
        {
            res.clear();
            return res;
        }

        res.append(obj.toObject().toVariantMap());
    }

    return res;
}
//-----------------------------------------------------------------------------------------------
bool JsonParser::fieldExists(const QString campo, const QString conteudo)
{
    QJsonDocument doc = QJsonDocument::fromJson(conteudo.toUtf8());
    QJsonObject objectJson = doc.object();

    return !objectJson.value(campo).isUndefined();
}
//-----------------------------------------------------------------------------------------------
// Constroi um json array de resposta
// Utiliza os nomes em campos e pareia com os valores
// O valor passado em arrayname é o nome do objeto json
// Retorna uma string vazia se arrayname ou valores estiverem vazios.
QString JsonParser::jsonArrayResponse(QString arrayname, QVector<QMap<QString,QVariant>> valores)
{
    if(arrayname == "" || valores.size() == 0)
    {
        return "";
    }

    QJsonObject mainObj;
    QJsonArray fieldArray;

    QMap<QString,QVariant> entry;

    for(int i=0; i < valores.length(); i++)
    {
        entry = valores[i];
        QList<QString> keys = entry.keys();
        QList<QVariant> values = entry.values();

        QJsonObject fieldObj;
        QMapIterator<QString,QVariant> it(entry);

        while(it.hasNext())
        {
            it.next();
            fieldObj.insert(it.key(), it.value().toString());
        }


        fieldArray.push_back(fieldObj);
    }

    mainObj.insert(arrayname, fieldArray);

    QJsonDocument doc(mainObj);
    return doc.toJson();
}

