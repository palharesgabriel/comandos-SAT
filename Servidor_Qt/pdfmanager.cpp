#include "pdfmanager.h"
#include <QDebug>


PdfManager::PdfManager()
{

}

QImage PdfManager::getPdfImageFromAndroid()
{
    QImage pdfImage;
    #if defined(Q_OS_ANDROID)
        QString folder = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation)[0];
        qDebug() << "String do folder" << folder;
        if(QDir(folder+"/QtRest/pdf-images").exists())
        {
            bool imageIsOpened = pdfImage.load(folder+"/QtRest/pdf-images"+"/temp.png", "PNG");
            QDir myDir(folder+"/QtRest/pdf-images");
            qDebug() << "Listando todos os arquivos do diretório" << myDir.absolutePath();
            foreach (QFileInfo afile, myDir.entryInfoList(QDir::AllEntries)) {
                qDebug() << afile.completeBaseName();
            }
            qDebug() << imageIsOpened;
        }
        return pdfImage;
    #endif

}

QString PdfManager::createPdfFromBase64(QString conteudoPDF)
{
    QByteArray temp;
    temp.append(conteudoPDF);
    QByteArray pdfData = QByteArray::fromBase64(temp);
    QString pdfCaminho = QCoreApplication::applicationDirPath() + "/util/ghostscript/temp.pdf";
    QFile pdfArquivo(pdfCaminho);
    if(pdfArquivo.open(QIODevice::WriteOnly)){
        pdfArquivo.write(pdfData);
        pdfArquivo.close();
        return pdfCaminho;
    }

    return "";
}

QString PdfManager::callGhostScriptToConvertPDFtoJpeg(QString ghostscriptPath, QString pdfPath)
{
    QProcess proccess;
    QStringList arguments;
    QString imageDestinationDir = QCoreApplication::applicationDirPath() + "/util/ghostscript/temp.jpeg";
    arguments << "-dBATCH" << "-dNOPAUSE" << "-dSAFER" << "-sDEVICE=jpeg" << "-dJPEGQ=99" << "-r200" << "-sOutputFile="+imageDestinationDir << pdfPath;
    proccess.execute(ghostscriptPath, arguments);
    proccess.waitForFinished(-1);
    proccess.close();
    return imageDestinationDir;
}
