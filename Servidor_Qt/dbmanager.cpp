#include "dbmanager.h"

#define SERIALTABLE    "serialdevices"
#define NETWORKTABLE   "networkdevices"

DbManager::DbManager()
{
    DbManager("./devices.db");
}
DbManager::DbManager(const QString& path)
{
    m_db = QSqlDatabase::addDatabase("QSQLITE");
    m_db.setDatabaseName(path);

    if(!m_db.open())
    {
        qDebug() << "Error: connection with database fail";
        qDebug() << m_db.isOpenError();
    }
    else
    {
        qDebug() << "Database: connection ok";
    }
}
//-----------------------------------------------------------------------------------------------
bool DbManager::init()
{
    qDebug() << "DbManager init";
    QFile dfile("./devices.db");
    if (!dfile.exists())
    {
        qDebug() << "Arquivo não existe - copiando arquivo do qrc";
        QFile::copy(":/db/devices.db", "./devices.db");
        QFile::setPermissions("./devices.db", QFile::WriteOwner | QFile::ReadOwner);
    }

    QFile dfile2("./devices.db");
    if (dfile2.exists())
    {
        qDebug() << "arquivo sqlite copiado;";
        QSqlDatabase::addDatabase("QSQLITE");
        return true;
    }

    return false;
}
//-----------------------------------------------------------------------------------------------
bool DbManager::openConnection(QSqlDatabase *dbm)
{
    *dbm = QSqlDatabase::database();
    dbm->setDatabaseName("./devices.db");

    if(dbm->open())
    {
        qDebug() << "retornou dbm aberto";
        return true;
    }
    else{
        qDebug() << "erro na abertura do banco de dados";
        return false;
    }
}
//-----------------------------------------------------------------------------------------------
UDPBroadcastDeviceInfo::NetworkDevice DbManager::getNetworkDeviceByMac(const QString& mac)
{
    QSqlDatabase dbm;
    UDPBroadcastDeviceInfo::NetworkDevice dev;

    if(!DbManager::openConnection(&dbm))
    {
        return dev;
    }

    QString ip;
    QSqlQuery query;
    query.prepare("SELECT ip,tag,mac FROM devices WHERE mac = :mac");
    query.bindValue(":mac",mac);
    if(query.exec())
    {
        while(query.next())
        {
            dev = UDPBroadcastDeviceInfo::NetworkDevice(query.value("ip").toString(), query.value("tag").toString(), mac);
        }
    }

    else
    {
        qDebug() << query.lastError();
    }

    dbm.close();

    return dev;
}
//-----------------------------------------------------------------------------------------------
bool DbManager::removeNetworkDevices(const QString& tipo)
{
    return DbManager::removeDevice(NETWORKTABLE,tipo);
}
//-----------------------------------------------------------------------------------------------
bool DbManager::removeSerialDevices(const QString& tipo)
{
    return DbManager::removeDevice(SERIALTABLE,tipo);
}
//-----------------------------------------------------------------------------------------------
bool DbManager::removeDevice(const QString& tablename, const QString& tipo)
{
    QSqlDatabase dbm;

    if(!DbManager::openConnection(&dbm))
    {
        return false;
    }

    bool status;

    QSqlQuery query;
    query.prepare("DELETE from "+ tablename +" WHERE tag = :tag");
    query.bindValue(":tag",tipo);

    if(query.exec())
    {
        status = true;
    }

    else
    {
       status = false;
    }

    dbm.close();

    return status;
}
//-----------------------------------------------------------------------------------------------
QVector<UDPBroadcastDeviceInfo::NetworkDevice> DbManager::getNetworkDevices(const QString& tipo)
{

    QSqlDatabase dbm;
    QVector<UDPBroadcastDeviceInfo::NetworkDevice> dispositivos;

    if(!DbManager::openConnection(&dbm))
    {
        return dispositivos;
    }

    QString ip;
    QSqlQuery query;
    QString queryText = "SELECT ip,tag,mac FROM " NETWORKTABLE " WHERE tag = :tag";
    query.prepare(queryText);
    query.bindValue(":tag",tipo);
    if(query.exec())
    {
        while(query.next())
        {
            qDebug() << "bucho";
            dispositivos.append(UDPBroadcastDeviceInfo::NetworkDevice(query.value("ip").toString(), tipo, query.value("mac").toString()));
        }
    }

    else
    {
        qDebug() << query.lastError();
    }

    dbm.close();

    return dispositivos;
}
//-----------------------------------------------------------------------------------------------
bool DbManager::addNetworkDevice(const QString& tipo, const QString& ip, const QString& mac)
{
    QSqlDatabase dbm;

    if(!DbManager::openConnection(&dbm))
    {
        return false;
    }

    bool success = false;
    QSqlQuery query;
    QString queryText = "INSERT INTO " NETWORKTABLE " (tag,ip,mac) VALUES (:tag,:ip,:mac)";
    query.prepare(queryText);
    query.bindValue(":tag", tipo);
    query.bindValue(":ip", ip);
    query.bindValue(":mac", mac);

    if(query.exec())
    {
        success = true;
        qDebug() << "dispositivo adicionado. TIPO: " << tipo << ip << mac;
    }
    else
    {
        qDebug() << "addDevice error: " << query.lastError();
    }

    dbm.close();

    return success;
}
//-----------------------------------------------------------------------------------------------
bool DbManager::addSerialDevice(const QString& tipo, const QString& porta, const QString& serial)
{
    QSqlDatabase dbm;

    if(!DbManager::openConnection(&dbm))
    {
        return false;
    }

    bool success = false;
    QSqlQuery query;
    QString queryText = "INSERT INTO " SERIALTABLE " (tag,port,nserie) VALUES (:tag,:porta,:serial)";
    query.prepare(queryText);
    query.bindValue(":tag", tipo);
    query.bindValue(":porta", porta);
    query.bindValue(":serial", serial);

    if(query.exec())
    {
        success = true;
        qDebug() << "dispositivo adicionado. TIPO: " << tipo << porta << serial;
    }
    else
    {
        qDebug() << "addDevice error: " << query.lastError();
    }

    dbm.close();

    return success;
}
//-----------------------------------------------------------------------------------------------
void DbManager::printAllNetworkDevices(void)
{
    QSqlDatabase dbm;

    if(!DbManager::openConnection(&dbm))
    {
        return;
    }

    QString queryText = "SELECT * FROM " NETWORKTABLE;
    QSqlQuery query(queryText);

    int idTipo = query.record().indexOf("tag");
    int idIP   = query.record().indexOf("ip");
    int idMac  = query.record().indexOf("mac");
    while(query.next())
    {
        QString name = query.value(idTipo).toString();
        QString ip = query.value(idIP).toString();
        QString mac = query.value(idMac).toString();
        qDebug() << "TAG: " << name << " | IP: " << ip << " | MAC:" << mac;
    }

    dbm.close();
}
//-----------------------------------------------------------------------------------------------
void DbManager::printAllSerialDevices(void)
{
    QSqlDatabase dbm;

    if(!DbManager::openConnection(&dbm))
    {
        return;
    }

    QString queryText = "SELECT * FROM " SERIALTABLE;
    QSqlQuery query(queryText);

    int idTipo = query.record().indexOf("tag");
    int idPort   = query.record().indexOf("port");
    int idSerie  = query.record().indexOf("nserie");
    while(query.next())
    {
        QString name = query.value(idTipo).toString();
        QString port = query.value(idPort).toString();
        QString serie = query.value(idSerie).toString();
        qDebug() << "TAG: " << name << " | PORTA: " << port << " | NSERIE:" << serie;
    }

    dbm.close();
}
//-----------------------------------------------------------------------------------------------
QVector<UDPBroadcastDeviceInfo::NetworkDevice> DbManager::getSerialDevices(const QString& tipo)
{
    QSqlDatabase dbm;
    QVector<UDPBroadcastDeviceInfo::NetworkDevice> dispositivos;

    if(!DbManager::openConnection(&dbm))
    {
        return dispositivos;
    }

    QString ip;
    QSqlQuery query;
    QString queryText = "SELECT ip,tag,mac FROM " SERIALTABLE " WHERE tag = :tag";
    query.prepare(queryText);
    query.bindValue(":tipo",tipo);
    if(query.exec())
    {
        while(query.next())
        {
            qDebug() << "bucho";
            //dispositivos.append(NetworkDevice(query.value("ip").toString(), tipo, query.value("mac").toString()));
        }
    }

    else
    {
        qDebug() << query.lastError();
    }

    dbm.close();

    return dispositivos;
}
//-----------------------------------------------------------------------------------------------
bool DbManager::removePerson(const QString& name)
{
    bool success = false;
    if(personExists(name))
    {
        QSqlQuery query;
        query.prepare("DELETE FROM people WHERE name = (:name)");
        query.bindValue(":name", name);
        success = query.exec();

        if(!success)
        {
            qDebug() << "remove person error: " << query.lastError();
        }
    }

    return success;
}

bool DbManager::personExists(const QString& name)
{
    bool success = false;
    QSqlQuery query;
    query.prepare("SELECT name FROM people WHERE name = (:name)");
    query.bindValue(":name", name);

    if(query.exec())
    {
        if(query.next())
        {
            qDebug() << "teste";
            success = true;
            return success;
        }
    }

    return success;
}
