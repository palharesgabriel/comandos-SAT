#include "serialmanager.h"

SerialManager::SerialManager(QObject *parent) : QObject(parent){}

QString SerialManager::sendToSerialPort(const QString port, const QString data)
{
    //connect(&m_port, &QSerialPort::readyRead, this, &SerialManager::dataFromSerial);

    QString sendData = data + "\r\n";
    m_port.setPortName(port);

    m_port.setBaudRate(QSerialPort::Baud115200);        //seta o baudrate
    m_port.setFlowControl(QSerialPort::NoFlowControl);  //sem controle de fluxo de dados
    m_port.setDataBits(QSerialPort::Data8);             //seta tamanho da msg em 8 bits
    m_port.setParity(QSerialPort::NoParity);            //sem envio de bit de paridade
    m_port.setStopBits(QSerialPort::OneStop);           //seta bit de stop da serial em apenas 1 bit

    if(m_port.open(QIODevice::ReadWrite))
    {
        qDebug() << "Porta" << m_port.portName() << "aberta com sucesso";
        m_port.write(sendData.toUtf8());
        if(m_port.waitForReadyRead(30000))
        {
            qDebug() << "Recebi dados da serial";
            QByteArray returnString = m_port.readAll();
            m_port.close();
            return QString::fromUtf8(returnString);
        }
    }
    else
    {
        qDebug() << "Não consegui abrir a porta";
        m_port.close();
        return "";
    }

    if(m_port.isOpen())
    {
        m_port.close();
    }

    return "";
}

void SerialManager::dataFromSerial()
{
    qDebug() << "entrei no slot";
}


