#ifndef SERIALDEVICEINFO_H
#define SERIALDEVICEINFO_H

#include <QString>
#include <QDebug>

// Template para dispositivos que fazem descoberta via serial
class SerialDeviceInfo
{
public:
    struct SerialDevice
    {
        SerialDevice()
        {
            this->modelo = "";
            this->serial = "";
            this->port   = "";
        }

        SerialDevice(QString modelo, QString serial, QString port)
        {
            this->modelo = modelo;
            this->serial = serial;
            this->port   = port;
        }

        QString modelo;
        QString serial;
        QString port;
    };

    ~SerialDeviceInfo();
    SerialDeviceInfo();
    SerialDeviceInfo(QString tag, QString hexPid, QString hexVid);

    // Discovery
    SerialDeviceInfo getSerialDeviceInfo(const QString filter = "");

    QString tag = "";  // Identificação do dispositivo serial
    QString vid = "";       // Identificador do vendedor do equipamento
    QString pid = "";       // Identificador do produto
};

#endif // SERIALDEVICEINFO_H
