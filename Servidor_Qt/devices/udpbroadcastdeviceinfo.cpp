#include "udpbroadcastdeviceinfo.h"

// Destructor
UDPBroadcastDeviceInfo::~UDPBroadcastDeviceInfo(){}
//---------------------------------------------------------------------------------------------------------------
UDPBroadcastDeviceInfo::UDPBroadcastDeviceInfo(){}
//---------------------------------------------------------------------------------------------------------------
// Constructor
// Inicia os valores padrão de busca do dispositivo
UDPBroadcastDeviceInfo::UDPBroadcastDeviceInfo(QString tag, int origin, int dest, int ipbegin,
                                                         int macbegin, QString discovery, QString result)
{
    qDebug() << "UDPBroadcastDeviceInfo";

    this->tag = tag;
    this->origin = origin;
    this->dest = dest;
    this->discovery = discovery;
    this->ipBegin = ipbegin;
    this->macBegin = macbegin;
    this->result = result;
}
//--------------------------------------------------------------------------------------------------------------
// Retorna uma estrutura de discoveryinfo a partir da classe
// Essa estrutura é utilizada para buscar o dispositivo na rede.
// O filtro é utilizado para a busca de um endereço mac específico na rede,
// e sua inclusão ignora quaisquer dispositivos a mais que tenham sido encontrados.
// O IP deve ser formatado sem leading zeroes, como 1:0:0:25:0:1f
UDPDiscoveryInfo UDPBroadcastDeviceInfo::getUDPBroadcastInfo(QString filter)
{
    UDPDiscoveryInfo discovery(this->tag, this->origin, this->dest, this->discovery, this->result);
    discovery.addMACField(this->macBegin);
    discovery.addIPField(this->ipBegin);
    discovery.setMACFilter(filter);
    return discovery;
}
