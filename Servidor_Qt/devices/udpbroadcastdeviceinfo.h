#ifndef UDPBROADCASTDEVICEINFO_H
#define UDPBROADCASTDEVICEINFO_H

#include "QString"
#include "discovery/udpdiscoveryinfo.h"

// Template para dispositivos que fazem descoberta via broadcast UDP
class UDPBroadcastDeviceInfo
{
public:

    struct NetworkDevice
    {
        NetworkDevice()
        {
            this->IP = "";
            this->modelo = "";
            this->mac = "";
        }

        NetworkDevice(QString ip, QString modelo, QString mac)
        {
            this->IP = ip;
            this->modelo = modelo;
            this->mac = mac;
        }

        QString IP;
        QString modelo;
        QString mac;
    };

    ~UDPBroadcastDeviceInfo();

    UDPBroadcastDeviceInfo();

    UDPBroadcastDeviceInfo(QString tag, int origin, int dest, int ipbegin, int macbegin,
                               QString discovery, QString result);

    // Discovery
    UDPDiscoveryInfo getUDPBroadcastInfo(QString filter);

    QString tag = "";        // Identificação da impressora
    int origin = 0;          // Porta de origem da requisição
    int dest = 0;            // Porta de destino da requisição
    QString discovery = "";  // String de descoberta
    int ipBegin = 0;         // Indice no array de bytes onde o ip começoa
    int macBegin = 0;        // Indice no array de bytes onde o mac começa
    QString result = "";     // String de retorno
};

#endif // UDPBroadcastDeviceInfo_H
