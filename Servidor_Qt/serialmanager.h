#ifndef SERIALMANAGER_H
#define SERIALMANAGER_H

#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDebug>

class SerialManager : public QObject
{
    Q_OBJECT
public:
    explicit SerialManager(QObject *parent = 0);
    QString sendToSerialPort(const QString porta, const QString dados);
private:
    QSerialPort m_port;
private slots:
    void dataFromSerial();

};

#endif // SERIALMANAGER_H
