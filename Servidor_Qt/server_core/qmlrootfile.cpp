#include "qmlrootfile.h"

#if defined(Q_OS_ANDROID)
#include <QtAndroidExtras/QAndroidJniObject>
#endif

QQuickView* QmlRootFile::view = NULL;

QmlRootFile::QmlRootFile(QObject *parent)
    : QObject(parent)
{
    QmlRootFile::view = qobject_cast<QQuickView *>(parent);
}
//-----------------------------------------------------------------------------------------------
void QmlRootFile::updateLog(QString msg)
{
    QObject *object = QmlRootFile::view->rootObject();
    QObject *rect = object->findChild<QObject*>("logText");
    if (rect){}
        rect->setProperty("text", msg);

    QmlRootFile::view->update();
}

