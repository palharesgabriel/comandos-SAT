#include "server.h"
#define FINDSTRING "LAPUENTEFIND"
#define SERVICEPORT 33333

Server::Server(QObject *parent) : HobrasoftHttpd::HttpServer(parent) {}

Server::Server(const HobrasoftHttpd::HttpSettings* settings, QObject *parent) : HobrasoftHttpd::HttpServer(settings, parent)
{
    QVector<UDPDiscoveryInfo> deviceInfo = ServicesHandler::retornarInformacoesDescobrimento();
}
//-----------------------------------------------------------------------------------------------
HobrasoftHttpd::HttpRequestHandler *Server::requestHandler(HobrasoftHttpd::HttpConnection *connection)
{
    return new ServicesHandler(connection);
}
//-----------------------------------------------------------------------------------------------
// Escuta por uma conexão
// Utilizado para responder as requisições das aplicações cliente na rede local
// que precisam descobrir onde o servidor se encontra via broadcast UDP
void Server::escutandoConexao(QObject* parent)
{
    socket = new QUdpSocket(parent);
    socket->bind(SERVICEPORT, QUdpSocket::ShareAddress);

    connect(socket, SIGNAL(readyRead()),
               this, SLOT(responderBroadcast()));
}
//-----------------------------------------------------------------------------------------------
// Responde a broadcasts UDP
// Caso a mensagem recebida seja a esperada, retorna o IP para quem realizou
// a requisição ao servidor
void Server::responderBroadcast()
{
    while(socket->hasPendingDatagrams())
    {
        QHostAddress receiveIP;
        quint16 receivePort;
        QByteArray datagram;

        datagram.resize(socket->pendingDatagramSize());
        socket->readDatagram(datagram.data(), datagram.size(), &receiveIP, &receivePort);

        if(QString::compare(FINDSTRING, datagram.data(), Qt::CaseInsensitive) == 0)
        {
            QList<QHostAddress> list = QNetworkInterface::allAddresses();

            for(int nIter=0; nIter<list.count(); nIter++)
            {
              if(!list[nIter].isLoopback())
              {
                  if (list[nIter].protocol() == QAbstractSocket::IPv4Protocol)
                  {
                      QUdpSocket *res = new QUdpSocket(this);
                      QString content = list[nIter].toString();;
                      res->writeDatagram(content.toUtf8(), receiveIP, receivePort);
                      break;
                  }
              }
            }
        }//datagrama enviado
    }
}
//-----------------------------------------------------------------------------------------------
// Mapeamento das funções de retorno do Android
#if defined(Q_OS_ANDROID)
static JNINativeMethod methods[] = {
    {"getCheckoutFromAndroid", "(Ljava/lang/String;)V", (void *)FMHandler::getCheckout},
    {"getLookupFromAndroid",   "(Ljava/lang/String;)V", (void *)FMHandler::getLookup},
    {"getVersionFromAndroid",  "(Ljava/lang/String;)V", (void *)FMHandler::getVersion},
    {"descobrimentoConcluido", "(Ljava/lang/String;)V", (void *)MP4200Handler::descobrimentoConcluido}
};
//-----------------------------------------------------------------------------------------------
// Inicia o carregamento via JNI
JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void* /*reserved*/)
{
    JNIEnv* env;

    // get the JNIEnv pointer.
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK)
        return JNI_ERR;

    // step 3
    // search for Java class which declares the native methods
    jclass javaClass = env->FindClass("org/qtproject/example/QtBridge");
    if (!javaClass)
    {
        return JNI_ERR;
    }

    // step 4
    // register our native methods
    if (env->RegisterNatives(javaClass, methods, sizeof(methods) / sizeof(methods[0])) < 0)
    {
        return JNI_ERR;
    }

    return JNI_VERSION_1_6;
}
#endif
