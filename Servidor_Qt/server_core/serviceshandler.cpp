#include "serviceshandler.h"

HobrasoftHttpd::HttpRequest *myRequest;
HobrasoftHttpd::HttpResponse *myResponse;
HobrasoftHttpd::HttpResponse *staticResponse;

ServicesHandler::~ServicesHandler() {}

ServicesHandler::ServicesHandler(HobrasoftHttpd::HttpConnection *parent) : HobrasoftHttpd::HttpRequestHandler(parent) {}
//-----------------------------------------------------------------------------------------------
QVector<UDPDiscoveryInfo> ServicesHandler::retornarInformacoesDescobrimento(void)
{
    QVector<UDPDiscoveryInfo> deviceInfo = QVector<UDPDiscoveryInfo>();
    //deviceInfo.append(MP4200Handler::retornarInformacoesDescobrimento(""));
    return deviceInfo;
}
void ServicesHandler::registerUDPBroadcastInfo(HobrasoftHttpd::HttpConnection* parent)
{
    //qDebug() << "registerUDPBInfo - I";
    //QVector<UDPBroadcastDeviceInfo> udpDevices;

    //MP4200Handler *controller = new MP4200Handler(parent);
    //udpDevices.append(controller->udpInfo);

    //qDebug() << "registerUDPBInfo - F";
    //delete controller;
}
//-----------------------------------------------------------------------------------------------
// Trata as requisições recebidas pelo servidor e
// redireciona para o handler correto caso necessário
void ServicesHandler::service(HobrasoftHttpd::HttpRequest *request, HobrasoftHttpd::HttpResponse *response)
{
    LogFile::createLog();

    if(true)
    {
        registerUDPBroadcastInfo(connection());
    }

    QString path = request->path();

    // Impressora MP4200TH
    if (path.startsWith("/mp4200"))
    {
        HobrasoftHttpd::HttpRequestHandler * controller = new MP4200Handler(connection());
        controller->service(request,response);
        return;
    }

    else if(path.startsWith("/sat"))
    {
        HobrasoftHttpd::HttpRequestHandler *controller = new SATHandler(connection());
        controller->service(request,response);
        return;
    }

    // Teste
    else if(path.startsWith("/generic"))
    {
        HobrasoftHttpd::HttpRequestHandler * controller = new TesteHandler(connection());
        controller->service(request,response);
        return;
    }

#if defined(Q_OS_ANDROID)
    // FISCAL MANAGER
    else if(path.startsWith("/fm"))
    {
        HobrasoftHttpd::HttpRequestHandler *controller = new FMHandler(connection());
        controller->service(request, response);
        return;
    }
#endif

    // Tratamento de erros padrão: 404, 503, etc.
    // call default handler - static html pages, shtml pages, images, javascript, styles...
    HobrasoftHttpd::HttpRequestHandler::service(request, response);
}
