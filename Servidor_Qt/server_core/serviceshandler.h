#ifndef SERVICESHANDLER_H
#define SERVICESHANDLER_H

#include <QObject>
#include <QDebug>
#include <QUdpSocket>
#include <QVector>

#include "httpserver/httprequesthandler.h"
#include "httpserver/httprequest.h"
#include "httpserver/httpresponse.h"
#include "httpserver/httpconnection.h"
#include "httpserver/staticfilecontroller.h"
#include "jsonparser.h"
#include "discovery/udpdiscoveryinfo.h"
#include "logfile.h"

//HANDLERS
#include "handler_templates/generichandler.h"
#include "handlers/MP4200/mp4200handler.h"
#include "handlers/Generic/testehandler.h"
#include "handlers/SAT/sathandler.h"

#if defined(Q_OS_ANDROID)
#include "handlers/FMHANDLER/fmhandler.h"
#include <QAndroidJniObject>
#include <QtAndroid>
#include <jni.h>
#endif

// Responsável por direcionar as conexões recebidas pelos clientes
// para os handlers corretos. Também gerencia os handlers, caso necessário.
class ServicesHandler : public HobrasoftHttpd::HttpRequestHandler
{
    Q_OBJECT
  public:

   ~ServicesHandler();

    /**
     * @brief Construktor
     */
    ServicesHandler(HobrasoftHttpd::HttpConnection *parent);

    /**
     * @brief Processes the request
     *
     * Own specialized classes are created and called for dynamic content.
     * General request handler is created for other static content requests.
     */
    void service(HobrasoftHttpd::HttpRequest *request, HobrasoftHttpd::HttpResponse *response);

    static QVector<UDPDiscoveryInfo> retornarInformacoesDescobrimento(void);
  protected:
    void registerUDPBroadcastInfo(HobrasoftHttpd::HttpConnection *parent);
};

#endif // SERVICESHANDLER_H
