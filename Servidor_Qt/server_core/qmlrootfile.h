#ifndef QMLROOTFILE_H
#define QMLROOTFILE_H

#include <QObject>

class QmlRootFile : public QObject
{
    Q_OBJECT
public:
    explicit QmlRootFile(QObject *parent = 0);

    static QQuickView* view;
    static void updateLog(QString);

private:
    QString m_notification;
};

#endif // QMLROOTFILE_H
