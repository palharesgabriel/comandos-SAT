//#include <QCoreApplication>
#include <QDir>

#include "httpserver/httpserver.h"
#include "httpserver/httpsettings.h"
#include "server.h"
#include "logfile.h"
#include "dbmanager.h"

// Teste Serial
#include "discovery/serialdiscoveryagent.h"
#include "devices/serialdeviceinfo.h"

#if defined(Q_OS_ANDROID)
#include <QAndroidJniObject>
#include <QtAndroid>
#include <jni.h>
#endif

QString buscarArquivoConfig(QString pasta)
{
    QFile arquivoConfig;
    arquivoConfig.setFileName(pasta + "/web.ini");

    if (arquivoConfig.exists())
    {
        QString nomeArquivoConfig = QDir(arquivoConfig.fileName()).canonicalPath();
        qDebug("using config file %s", qPrintable(nomeArquivoConfig));

        arquivoConfig.close();
        return nomeArquivoConfig;
    }

    else
    {
        qDebug("Arquivo de Config nao encontrado");
        return "";
    }
}
//-----------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QSettings qsettings(":/web/web.ini", QSettings::IniFormat);
    qsettings.beginGroup("listener");
    HobrasoftHttpd::HttpSettings settings(&qsettings, 0);

    // Inicilização do banco de dados
    if(!DbManager::init())
    {
       return 1;
    }

    new Server(&settings, 0);

    return a.exec();
}
