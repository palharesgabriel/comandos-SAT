#ifndef LOGFILE_H
#define LOGFILE_H

#include <QString>
#include <QFile>
#include <QIODevice>
#include <QTextStream>
#include <QStandardPaths>
#include <QtGlobal>
#include <QDir>
#include <QDebug>
#include <QDateTime>

#if defined(Q_OS_ANDROID)
#include <QAndroidJniObject>
#include <QAndroidJniEnvironment>
#include <QtAndroid>
#include <jni.h>
#endif

// Classe responsável pela criação e escrita dos arquivos de Log usados pelo Android [c++]
class LogFile
{
public:
    static QString path;
    static QString logname;
    static void clear(void);
    static void createLog();
    static void writeToLog(QString);
    static QString readFromLog();
    static QString createTimestamp();

private:
    LogFile();
    static void createAndroidLog();
};

#endif // LOGFILE_H
