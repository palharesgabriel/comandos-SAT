#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include "httpserver/httpserver.h"
#include <QDebug>
#include <QUdpSocket>
#include <QNetworkInterface>
#include <QVector>
#include <QUdpSocket>
#include <QTimer>
#include <QSignalMapper>

#include "discovery/udpdiscoveryinfo.h"
#include "discovery/udpdiscoveryagent.h"

//HANDLER
#include "serviceshandler.h"

/**
 * @brief Own implementation of HTTP server.
 *
 * It extends the general class HobrasoftHttpd::HttpServer.
 */
class Server : public HobrasoftHttpd::HttpServer {
    Q_OBJECT
  public:
    /**
     * @brief Constructor
     */
    Server(QObject *parent);

    Server(const HobrasoftHttpd::HttpSettings*, QObject *);

    /**
     * @brief Returns pointer to new instance of my own request handler HobrasoftHttpd::HttpRequestHandler
     */
    virtual HobrasoftHttpd::HttpRequestHandler *requestHandler(HobrasoftHttpd::HttpConnection *);

  private:
    QUdpSocket *socket;
    QUdpSocket *udpSocketGet;
    void escutandoConexao(QObject*);

  private slots:
    void responderBroadcast(void);
};

#endif // SERVER_H
