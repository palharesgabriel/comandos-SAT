#include "logfile.h"

LogFile::LogFile(){}

QString LogFile::path = "";
QString LogFile::logname = "";
//-----------------------------------------------------------------------------------------------
void LogFile::createLog()
{
    #if defined(Q_OS_ANDROID)
    LogFile::createAndroidLog();
    #endif
}
//-----------------------------------------------------------------------------------------------
// Limpa o conteúdo do log
void LogFile::clear()
{
    QFile* file = new QFile(LogFile::path);
    file->resize(0);
}
//-----------------------------------------------------------------------------------------------
// Cria um log de arquivo em uma pasta QtRest dentro dentro da pasta
// de documentos do dispositivo
void LogFile::createAndroidLog()
{
    QString folder = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation)[0];
    QString timestamp = QDateTime::currentDateTime().toString("dd-MM-yyyy");

    if(!QDir(folder+"/QtRest").exists())
    {
        QDir().mkdir(folder+"/QtRest");
    }

    QFileInfo check_file(folder+"/QtRest/Debug-"+timestamp+".log");
    LogFile::path = folder+"/QtRest/";

    if("Debug-" + timestamp != LogFile::logname)
    {
        LogFile::logname = "Debug-" + timestamp;
    }

    // Garante que só tem um log por dia
    if (!(check_file.exists() && check_file.isFile()))
    {
        QFile* file = new QFile(folder+"/QtRest/Debug-" + timestamp + ".log");

        file->open(QIODevice::WriteOnly);
        file->write("--- Iniciando o Log ---\n");
        file->close();
    }
}
//-----------------------------------------------------------------------------------------------
// Retorna um timestamp
QString LogFile::createTimestamp()
{
    QString timestamp;
    timestamp = QDateTime::currentDateTime().toString("hh:mm:ss:zzz");
    return timestamp;
}
//-----------------------------------------------------------------------------------------------
// Escreve o conteúdo no log
void LogFile::writeToLog(QString content)
{
    QString time = LogFile::createTimestamp();
    QFile file(LogFile::path + logname + ".log");

    if (file.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        QTextStream stream(&file);
        stream << time << " " << content << " [C++]" << endl;
    }

    file.close();
}
//-----------------------------------------------------------------------------------------------
// Retorna o conteúdo do log como uma string
QString LogFile::readFromLog()
{
    QFile file(LogFile::path + LogFile::logname + ".log");
    QString content = "";

    if (file.open(QIODevice::ReadOnly))
    {
        QTextStream stream( &file );

        while(!stream.atEnd())
        {
            QString line = stream.readLine();
            content += line + "\n";
        }

        file.close();
    }else{
        return "ERRO AO ABRIR ARQUIVO DE LOG";
    }

    return content;
}
